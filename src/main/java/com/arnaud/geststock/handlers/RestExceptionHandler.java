package com.arnaud.geststock.handlers;

import com.arnaud.geststock.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<ErrorDto> handlerException(final EntityNotFoundException exception, final WebRequest webRequest){

        final HttpStatus notFound = HttpStatus.NOT_FOUND;
        final var errorDto =  ErrorDto.builder()
                .errorCodes(exception.getErrorCodes())
                .httpCode(notFound.value())
                .message(exception.getMessage())
                .build();

        return new ResponseEntity<>(errorDto, notFound);
    }

    @ExceptionHandler(value = InvalidJWTException.class)
    public ResponseEntity<ErrorDto> handlerException(final InvalidJWTException exception, final WebRequest webRequest){

        final HttpStatus notFound = HttpStatus.FORBIDDEN;
        final var errorDto =  ErrorDto.builder()
                .errorCodes(exception.getErrorCodes())
                .httpCode(notFound.value())
                .message(exception.getMessage())
                .build();

        return new ResponseEntity<>(errorDto, notFound);
    }

    @ExceptionHandler(value = InvalidEntityException.class)
    public ResponseEntity<ErrorDto> handlerException(final InvalidEntityException exception, final WebRequest webRequest){

        final HttpStatus batRequest = HttpStatus.BAD_REQUEST;
        final var errorDto = ErrorDto.builder()
                .errorCodes(exception.getErrorCodes())
                .errors(exception.getErrors())
                .httpCode(batRequest.value())
                .message(exception.getMessage())
                .build();

        return new ResponseEntity<>(errorDto, batRequest);
    }

    @ExceptionHandler(value = InvalidOperationException.class)
    public ResponseEntity<ErrorDto> handlerException(final InvalidOperationException exception, final WebRequest webRequest){

        final HttpStatus notFound = HttpStatus.BAD_REQUEST;
        final var errorDto =  ErrorDto.builder()
                .errorCodes(exception.getErrorCodes())
                .httpCode(notFound.value())
                .message(exception.getMessage())
                .build();

        return new ResponseEntity<>(errorDto, notFound);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ErrorDto> handlerException(final BadCredentialsException exception, final WebRequest webRequest){

        final HttpStatus batRequest = HttpStatus.BAD_REQUEST;
        final ErrorDto errorDto = ErrorDto.builder()
                .errorCodes(ErrorCodes.BAD_CREDENTIAL)
                .httpCode(batRequest.value())
                .message(exception.getMessage())
                .errors(Collections.singletonList("Login et / ou mot de passe incorrecte"))
                .build();

        return new ResponseEntity<>(errorDto, batRequest);
    }

    @ExceptionHandler(value = HttpClientErrorException.Unauthorized.class)
    public ResponseEntity<ErrorDto> handlerException(final HttpClientErrorException.Unauthorized exception, final WebRequest webRequest){

        final HttpStatus unauthorized = HttpStatus.UNAUTHORIZED;
        final ErrorDto errorDto = ErrorDto.builder()
                .errorCodes(ErrorCodes.UNAUTHORIZED)
                .httpCode(unauthorized.value())
                .message(exception.getMessage())
                .errors(Collections.singletonList("Non Authorizer"))
                .build();
        return new ResponseEntity<>(errorDto, unauthorized);
    }

    @ExceptionHandler(InternalAuthenticationServiceException.class)
    public ResponseEntity<ErrorDto> handleInternalAuthenticationServiceException(final InternalAuthenticationServiceException e, final WebRequest webRequest) {
        final HttpStatus unauthorized = HttpStatus.UNAUTHORIZED;
        final ErrorDto errorDto = ErrorDto.builder()
                .errorCodes(ErrorCodes.UNAUTHORIZED)
                .httpCode(unauthorized.value())
                .message(e.getMessage() + "  --  " + e.getCause().toString())
                .errors(Collections.singletonList("Non Authorizer"))
                .build();

        return new ResponseEntity<>(errorDto, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = EntityAlreadyExistException.class)
    public ResponseEntity<ErrorDto> handlerException(final EntityAlreadyExistException e, final WebRequest webRequest){
        final HttpStatus conflict = HttpStatus.CONFLICT;
        final ErrorDto errorDto = ErrorDto.builder()
                .errorCodes(e.getErrorCodes())
                .httpCode(conflict.value())
                .message(e.getMessage())
                .build();

        return new ResponseEntity<>(errorDto, HttpStatus.CONFLICT);
    }
}
