package com.arnaud.geststock.handlers;

import com.arnaud.geststock.exception.ErrorCodes;
import lombok.*;

import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorDto {

    private Integer httpCode;

    private ErrorCodes errorCodes;

    private String message;

    private Collection<String > errors;
}
