package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.CommandeClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface CommandeClientRepository extends JpaRepository<CommandeClient ,Integer > {

    Optional<CommandeClient> findCommandeClientByCode(final String code);

    Collection<CommandeClient> findAllByClientId(final Integer idClient);
}
