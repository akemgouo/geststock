package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ManufacturerRepository extends JpaRepository<Manufacturer, Integer> {

    Optional<Manufacturer> findManufacturerByCodeManufacturer(final String code);

    Boolean existsManufacturerByCodeManufacturer(final String code);
}
