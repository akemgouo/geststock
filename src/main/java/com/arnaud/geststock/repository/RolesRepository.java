package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesRepository extends JpaRepository<Roles , Integer> {
}
