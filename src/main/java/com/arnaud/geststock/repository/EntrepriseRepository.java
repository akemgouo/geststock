package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Entreprise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EntrepriseRepository extends JpaRepository<Entreprise , Integer> {

    Optional<Entreprise> findEntrepriseByCode(final String code);
}
