package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer > {

    Optional<Category> findCategoryByCode(final String codeCategory);
    Boolean existsByCode(final String codeCategory);
}
