package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client , Integer> {

    Optional<Client> findClientByCodeClient(final String codeClient);
    Optional<Client> findClientByEmail(final String email);
    Boolean existsClientByCodeClient(final String codeClient);
    Boolean existsClientByEmail(final String email);
}
