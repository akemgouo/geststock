package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Ville;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VilleRepository extends JpaRepository<Ville, Integer> {

    Optional<Ville> findVilleByCode(final String code);

    Boolean existsByCode(final String code);
}
