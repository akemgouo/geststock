package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.CommandeFournisseur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface CommandeFournisseurRepository extends JpaRepository<CommandeFournisseur , Integer> {

    Optional<CommandeFournisseur> findCommandeFournisseurByCode(final String code);

    Collection<CommandeFournisseur> findAllByFournisseurId(final Integer idFourinisseur);
}
