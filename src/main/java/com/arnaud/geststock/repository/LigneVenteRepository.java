package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.LigneVente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface LigneVenteRepository extends JpaRepository<LigneVente , Integer> {

    Collection<LigneVente> findAllByArticleId(final Integer idArticle);
    Collection<LigneVente> findAllByVenteId(final Integer idVente);
}
