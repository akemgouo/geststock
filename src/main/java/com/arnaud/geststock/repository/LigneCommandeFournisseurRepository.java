package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.LigneCommandeFournisseur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface LigneCommandeFournisseurRepository extends JpaRepository<LigneCommandeFournisseur ,Integer > {

    Collection<LigneCommandeFournisseur> findAllByCommandeFournisseurId(final Integer idCommande);
    Collection<LigneCommandeFournisseur> findAllByArticleId(final Integer idArticle);
}
