package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Fournisseur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FournisseurRepository extends JpaRepository<Fournisseur , Integer> {

    Optional<Fournisseur> findFournisseurByCodeFournisseur(final String code);
}
