package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer> {

    Optional<Article> findArticleByCodeArticle(final String codeArticle);

    Collection<Article> findAllByCategoryId(final Integer idCategory);

    Collection<Article> findAllByManufacturerId(final Integer idManufacturer);

    Boolean existsByCodeArticle(final String codeArticle);
}
