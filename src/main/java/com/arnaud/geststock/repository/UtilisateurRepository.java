package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur ,Integer> {

    @Query(value = "select u from Utilisateur u where u.username = :username")
    Optional<Utilisateur> findUtilisateurByUsername(@Param(value = "username") final String username);

    Optional<Utilisateur> findUtilisateurByEmail(final String email);
}
