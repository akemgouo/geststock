package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.LigneCommandeClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface LigneCommandeClientRepository extends JpaRepository<LigneCommandeClient ,Integer > {

    Collection<LigneCommandeClient> findAllByCommandeClientId(final Integer id);

    Collection<LigneCommandeClient> findAllByArticleId(final Integer idArticle);
}
