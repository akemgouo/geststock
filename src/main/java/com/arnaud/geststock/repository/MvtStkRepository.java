package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.MvtStk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Collection;

@Repository
public interface MvtStkRepository extends JpaRepository<MvtStk, Integer> {

    BigDecimal stockReelArticle(final Integer idArticle);
    Collection<MvtStk> findAllByArticleId(final Integer idArticle);
}
