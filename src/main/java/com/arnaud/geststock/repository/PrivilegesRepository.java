package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Privileges;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PrivilegesRepository extends JpaRepository<Privileges , Integer> {

    Optional<Privileges> findPrivilegesByNom(final String nom);
}
