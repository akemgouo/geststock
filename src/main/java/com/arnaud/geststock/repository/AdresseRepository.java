package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Adresse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdresseRepository extends JpaRepository<Adresse, Integer> {
    Optional<Adresse> findAdresseByAdresse1(final String adresse1);
}
