package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Pays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PaysRepository extends JpaRepository<Pays , Integer> {

    Boolean existsByCode(final String code);
    Optional<Pays> findPaysByCode(final String code);
}
