package com.arnaud.geststock.interceptor;

import org.hibernate.EmptyInterceptor;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;

public class Interceptor extends EmptyInterceptor {

    @Override
    public String onPrepareStatement(String sql) {
        if(StringUtils.hasLength(sql) && sql.toLowerCase().startsWith("select")){

            final String entityName = sql.substring(7, sql.indexOf("."));
            final String idEntrepise = MDC.get("idEntreprise");
            if (StringUtils.hasLength(entityName)
                    && !entityName.toLowerCase().contains("entreprise")
                    && !entityName.toLowerCase().contains("roles")
                    && StringUtils.hasLength(idEntrepise)){
                if (sql.contains("where")){
                    sql = sql + " and " + entityName + ".identreprise = " + idEntrepise;
                } else {
                    sql = sql + " where " + entityName + ".identreprise = " + idEntrepise;
                }
            }
        }
        return super.onPrepareStatement(sql);
    }
}
