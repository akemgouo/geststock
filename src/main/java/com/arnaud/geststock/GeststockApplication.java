package com.arnaud.geststock;

import com.arnaud.geststock.dto.*;
import com.arnaud.geststock.model.Status;
import com.arnaud.geststock.repository.*;
import de.codecentric.boot.admin.config.EnableAdminServer;
import lombok.SneakyThrows;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Set;


//@ComponentScan(basePackages = "com.arnaud.geststock.repository")
@SpringBootApplication//(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@EnableJpaAuditing
@EnableAdminServer
public class GeststockApplication {

    @SneakyThrows
    public static void main(String[] args) {

        SpringApplication.run(GeststockApplication.class, args);

    }

    @Bean
    @Transactional
    public CommandLineRunner loadData(RolesRepository rolesRepository, UtilisateurRepository userRepository,
                                      PasswordEncoder passwordEncoder, PaysRepository paysRepository,
                                      VilleRepository villeRepository, PrivilegesRepository privilegesRepository,
                                      AdresseRepository adresseRepository, EntrepriseRepository entrepriseRepository) {
        return (args) -> {

            /** Pays **
             *
             */
            PaysDto pays = PaysDto.builder()
                    .code("CMR")
                    .description("Cameroun")
                    .icon("")
                    .build();

            PaysDto savedPays = PaysDto.convertEntityToDto(paysRepository.save(PaysDto.convertDtoToEntity(pays)));

            /** Ville **
             *
             */
            VilleDto villeDto = VilleDto.builder()
                    .code("DLA")
                    .description("Douala")
                    .pays(savedPays)
                    .build();
            VilleDto savedVille = VilleDto.convertEntityToDto(villeRepository.save(VilleDto.convertDtoToEntity(villeDto)));

            /** Adresse **
             *
             */
            AdresseDto adresseDto = AdresseDto.builder()
                    .adresse1("31 rue de la liberté")
                    .adresse2("56100 Lorient")
                    .codePostale("56100")
                    .ville(savedVille)
                    .build();
            AdresseDto savedAdresse = AdresseDto.convertEntityToDto(adresseRepository.save(AdresseDto.convertDtoToEntity(adresseDto)));

            /** Entrepise **
             *
             */
            EntrepriseDto entrepriseDto = EntrepriseDto.builder()
                    .code("CFAO")
                    .description("Confedration Française d'Afrique d'Outre-Mer")
                    .codeFiscal("")
                    .email("cfao.com")
                    .nom("CFAO TECHNOLOGIES")
                    .numTel("+237 225 254 265")
                    .raisonSocial("SA")
                    .siteWeb("www.cfao.com")
                    .status(Status.ACTIVE)
                    .adresse(savedAdresse)
                    .photo("")
                    .build();
            EntrepriseDto saveEntreprise = EntrepriseDto.convertEntityToDto(entrepriseRepository.save(EntrepriseDto.convertDtoToEntity(entrepriseDto)));

            RolesDto rolesDto = RolesDto.builder()
                    .roleName("ADMIN")
                    //.utilisateurs(Arrays.asList(savedUser))
                    .build();
            RolesDto savedRoles = RolesDto.convertEntityToDto(rolesRepository.save(RolesDto.convertDtoToEntity(rolesDto)));

            UtilisateurDto user = UtilisateurDto.builder()
                    .dateNaissance(Instant.now())
                    .enabled(true)
                    .email("tcheukem26@gmail.com")
                    .nom("Arnaud")
                    .username("akemgouo")
                    .password(passwordEncoder.encode("P@ssw0rd"))
                    .entreprise(saveEntreprise)
                    .roles(Set.of(savedRoles))
                    .build();

            UtilisateurDto savedUser = UtilisateurDto.convertEntityToDto(userRepository.save(UtilisateurDto.convertDtoToEntity(user)));

        };
    }
}
