package com.arnaud.geststock.exception;

import lombok.Getter;

public class InvalidOperationException extends RuntimeException {

    @Getter
    private ErrorCodes errorCodes;

    public InvalidOperationException() {
        super();
    }

    public InvalidOperationException(final String message) {
        super(message);
    }

    public InvalidOperationException(final String message, final ErrorCodes errorCodes) {
        super(message);
        this.errorCodes = errorCodes;
    }

    public InvalidOperationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidOperationException(final String message, final Throwable cause, final ErrorCodes errorCodes) {
        super(message, cause);
        this.errorCodes = errorCodes;
    }

    public InvalidOperationException(final Throwable cause) {
        super(cause);
    }

    public InvalidOperationException(final Integer id) {
        super(String.valueOf(id));
    }

    public InvalidOperationException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
