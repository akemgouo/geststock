package com.arnaud.geststock.exception;

import lombok.Getter;

import java.util.List;

public class IllegalArgumentException extends RuntimeException {

    @Getter
    private ErrorCodes errorCodes;

    @Getter
    private List<String> errors;

    public IllegalArgumentException() {
        super();
    }

    public IllegalArgumentException(final String message) {
        super(message);
    }

    public IllegalArgumentException(final String message,final Throwable cause, final ErrorCodes errorCodes) {
        super(message, cause);
        this.errorCodes = errorCodes;
    }

    public IllegalArgumentException(final String message,final ErrorCodes errorCodes) {
        super(message);
        this.errorCodes = errorCodes;
    }

    public IllegalArgumentException(final String message, final ErrorCodes errorCodes, List<String> errors) {
        super(message);
        this.errorCodes = errorCodes;
        this.errors = errors;
    }

    public IllegalArgumentException(final Throwable cause) {
        super(cause);
    }

    public IllegalArgumentException(final Integer id) {
        super(String.valueOf(id));
    }

    public IllegalArgumentException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
