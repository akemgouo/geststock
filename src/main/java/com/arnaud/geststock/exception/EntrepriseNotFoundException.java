package com.arnaud.geststock.exception;

public class EntrepriseNotFoundException extends Exception {

    public EntrepriseNotFoundException() {
        super();
    }

    public EntrepriseNotFoundException(String message) {
        super(message);
    }

    public EntrepriseNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntrepriseNotFoundException(Throwable cause) {
        super(cause);
    }

    public EntrepriseNotFoundException(Integer id) {
        super(String.valueOf(id));
    }

    public EntrepriseNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
