package com.arnaud.geststock.exception;

import lombok.Getter;

public class InvalidJWTException extends RuntimeException {

    @Getter
    private ErrorCodes errorCodes;

    public InvalidJWTException() {
        super();
    }

    public InvalidJWTException(final String message) {
        super(message);
    }

    public InvalidJWTException(final String message, final ErrorCodes errorCodes) {
        super(message);
        this.errorCodes = errorCodes;
    }

    public InvalidJWTException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidJWTException(final String message, final Throwable cause, final ErrorCodes errorCodes) {
        super(message, cause);
        this.errorCodes = errorCodes;
    }

    public InvalidJWTException(final Throwable cause) {
        super(cause);
    }

    public InvalidJWTException(final Integer id) {
        super(String.valueOf(id));
    }

    public InvalidJWTException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
