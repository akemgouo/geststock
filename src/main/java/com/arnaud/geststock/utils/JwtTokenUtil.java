package com.arnaud.geststock.utils;

import com.arnaud.geststock.model.auth.ExtendedUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
@Slf4j
public class JwtTokenUtil {

    public static final long JWT_TOKEN_VALIDATY = 10 * 60 * 60;

    Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    public String getUserNameFromToken(final String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    public String extractIdEntreprise(final String token){
        final Claims claims = getAllClaimsFromToken(token);
        return claims.get("idEntreprise", String.class);
    }

    public Claims getAllClaimsFromToken(final String token) {
        //System.out.println("getAllClaimsFromToken = " + Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getExpiration());
        return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
    }

    public Date getExpirationDateFormToken(final String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public Boolean isTokenExpired(final String token) {
        final Date expirationDate = getExpirationDateFormToken(token);
        return expirationDate.before(new Date());
    }

    public String generateToken(final ExtendedUser userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, userDetails);
    }

    public String createToken(Map<String, Object> claims, final ExtendedUser userDetails){
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDATY * 1000))
                .claim("idEntreprise", userDetails.getIdEntreprise().toString())
                .signWith(key).compact();
    }

    public Boolean validateToken(final String token, final ExtendedUser userDetails) {
        final String username = getUserNameFromToken(token);
        //System.out.println("JwtTokenUtil = " + username);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}