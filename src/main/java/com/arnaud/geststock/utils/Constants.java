package com.arnaud.geststock.utils;

public interface Constants {
    String APP_ROOT = "geststock/v1";

    /**
     * ARTICLE API ENDPOINT
     */

    String ARTICLE_ENDPOINT = APP_ROOT + "/articles";
    String CREATE_ARTICLE_ENDPOINT = ARTICLE_ENDPOINT + "/create";
    String FIND_ARTICLE_BY_ID_ENDPOINT = ARTICLE_ENDPOINT + "/{idArticle}";
    String FIND_ARTICLE_BY_CODE_ENDPOINT = ARTICLE_ENDPOINT + "/filter/{codeArticle}";
    String FIND_ALL_ARTICLE_ENDPOINT = ARTICLE_ENDPOINT + "/all";
    String FIND_ALL_HISTORIQUE_VENTES_ARTICLE_ENDPOINT = ARTICLE_ENDPOINT + "/historique/vente/{idArticle}";
    String FIND_ALL_HISTORIQUE_COMANDE_CLIENT_ARTICLE_ENDPOINT = ARTICLE_ENDPOINT + "/historique/commandeclient/{idArticle}";
    String FIND_ALL_HISTORIQUE_COMANDE_FOURNISSEUR_ARTICLE_ENDPOINT = ARTICLE_ENDPOINT + "/historique/commandefournisseur/{idArticle}";
    String FIND_ALL_ARTICLE_BY_ID_CATEGORY_ENDPOINT = ARTICLE_ENDPOINT + "/filter/category/{idCategory}";
    String DELETE_ARTICLE_ENDPOINT = ARTICLE_ENDPOINT + "/delete/{idArticle}";
    /**
     *  COMMANDECLIENTS API END POINT
     */

    String COMMNANDE_CLIENT_ENDPOINT = APP_ROOT + "/commandesclients";
    String CREATE_COMMANDE_CLIENT_ENDPOINT = COMMNANDE_CLIENT_ENDPOINT + "/create";
    String FIND_COMMANDE_CLIENT_BY_ID_ENDPOINT = COMMNANDE_CLIENT_ENDPOINT + "/{idCommandeClient}";
    String FIND_COMMANDE_CLIENT_BY_CODE_ENDPOINT = COMMNANDE_CLIENT_ENDPOINT + "/{codeCommandeClient}";
    String FIND_ALL_COMMANDE_CLIENT_ENDPOINT = COMMNANDE_CLIENT_ENDPOINT + "/all";
    String FIND_LIGNE_COMMANDE_CLIENT_BY_COMMANDECLIENTID_ENDPOINT = COMMNANDE_CLIENT_ENDPOINT + "/lignesCommande/{idCommandeClient}";
    String UPDATE_COMMANDE_CLIENT_BY_CLIENT_ENDMOINT = COMMNANDE_CLIENT_ENDPOINT + "/update/client/{idCommandeClient}/{idClient}";
    String UPDATE_COMMANDE_CLIENT_BY_STATUS_ENDMOINT = COMMNANDE_CLIENT_ENDPOINT + "/update/status/{idCommandeClient}/{stsCommande}";
    String UPDATE_COMMANDE_CLIENT_BY_ARTICLE_ENDMOINT = COMMNANDE_CLIENT_ENDPOINT + "/update/article/{idCommandeClient}/{idLigneCommande}/{idArticle}";
    String UPDATE_LIGNE_COMMANDE_CLIENT_BY_QUANTITE_ENDMOINT = COMMNANDE_CLIENT_ENDPOINT + "/update/quantite/{idCommandeClient}/{idLigneCommande}/{quantite}";
    String DELETE_COMMANDE_CLIENT_ENDPOINT = COMMNANDE_CLIENT_ENDPOINT + "/delete/{idCommandeClient}";
    String DELETE_LIGNE_COMMANDE_CLIENT_ENDPOINT = COMMNANDE_CLIENT_ENDPOINT + "/delete/lignesCommande/{idCommandeClient}/{idLigneCommande}";


    /**
     *  COMMANDEFOUNISSEURS API END POINT
     */

    String COMMNANDE_FOURNISSEUR_ENDPOIT = APP_ROOT + "/commandefounisseurs";
    String CREATE_COMMANDE_FOURNISSEUR_ENDPOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/create";
    String FIND_COMMANDE_FOURNISSEUR_BY_ID_ENDPOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/{idCommandeFournisseur}";
    String FIND_COMMANDE_FOURNISSEUR_BY_CODE_ENDPOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/{codeCommandeFournisseur}";
    String FIND_ALL_COMMANDE_FOURNISSEUR_ENDPOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/all";
    String DELETE_COMMANDE_FOURNISSEUR_ENDPOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/delete/{idCommandeFournisseur}";

    String FIND_LIGNE_COMMANDE_FOUNISSEURS_BY_COMMANDEFOUNISSEURID_ENDPOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/lignesCommande/{idCommandeFournisseur}";
    String UPDATE_COMMANDE_FOUNISSEURS_BY_FOUNISSEUR_ENDMOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/update/fournisseur/{idCommandeFournisseur}/{idFournisseur}";
    String UPDATE_COMMANDE_FOUNISSEURS_BY_STATUS_ENDMOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/update/status/{idCommandeFournisseur}/{stsCommande}";
    String UPDATE_COMMANDE_FOUNISSEURS_BY_ARTICLE_ENDMOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/update/article/{idCommandeFournisseur}/{idLigneCommande}/{idArticle}";
    String UPDATE_LIGNE_COMMANDE_FOUNISSEURS_BY_QUANTITE_ENDMOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/update/quantite/{idCommandeFournisseur}/{idLigneCommande}/{quantite}";
    String DELETE_LIGNE_COMMANDE_FOUNISSEURS_ENDPOINT = COMMNANDE_FOURNISSEUR_ENDPOIT + "/delete/lignesCommande/{idCommandeFournisseur}/{idLigneCommande}";

    /**
     * Entreprise API Constante
     */
    String ENTREPRISE_ENDPOINT = APP_ROOT + "/entreprises";
    String CREATE_ENTREPRISE_ENDPOINT = ENTREPRISE_ENDPOINT + "/create";
    String FIND_ENTREPRISE_BY_ID_ENDPOINT = ENTREPRISE_ENDPOINT + "/{idEntreprise}";
    String FIND_ENTREPRISE_BY_CODE_ENDPOINT = ENTREPRISE_ENDPOINT + "/{codeEntreprise}";
    String FIND_ALL_ENTREPRISE_ENDPOINT = ENTREPRISE_ENDPOINT + "/all";
    String DELETE_ENTREPRISE_ENDPOINT = ENTREPRISE_ENDPOINT + "/delete/{idEntreprise}";

    /**
     *  Fournisseur API ENDPOINT
     */

    String FOURNISSEUR_ENDPOINT = APP_ROOT + "/fournisseurs";
    String CREATE_FOURNISSEUR_ENDPOINT = FOURNISSEUR_ENDPOINT + "/create";
    String FIND_FOURNISSEUR_BY_ID_ENDPOINT = FOURNISSEUR_ENDPOINT + "/{idFournisseur}";
    String FIND_FOURNISSEUR_BY_CODE_ENDPOINT = FOURNISSEUR_ENDPOINT + "/{codeFournisseur}";
    String FIND_ALL_FOURNISSEUR_ENDPOINT = FOURNISSEUR_ENDPOINT + "/all";
    String DELETE_FOURNISSEUR_ENDPOINT = FOURNISSEUR_ENDPOINT + "/delete/{idFournisseur}";

    /**
     *  Manufacturer API ENDPOINT
     */

    String MANUFACTURER_ENDPOINT = APP_ROOT + "/manufacturerS";
    String CREATE_MANUFACTURER_ENDPOINT = MANUFACTURER_ENDPOINT + "/create";
    String FIND_MANUFACTURER_BY_ID_ENDPOINT = MANUFACTURER_ENDPOINT + "/{idManufacturer}";
    String FIND_MANUFACTURER_BY_CODE_ENDPOINT = MANUFACTURER_ENDPOINT + "/{codeManufacturer}";
    String FIND_ALL_MANUFACTURER_ENDPOINT = MANUFACTURER_ENDPOINT + "/all";
    String DELETE_MANUFACTURER_ENDPOINT = MANUFACTURER_ENDPOINT + "/delete/{idManufacturer}";

    /**
     *  Utilisateur API ENDPOINT
     */

    String UTILISATEUR_ENDPOINT = APP_ROOT + "/utilisateurs";
    String CREATE_UTILISATEUR_ENDPOINT = UTILISATEUR_ENDPOINT + "/create";
    String CHANGE_PASSWORD_UTILISATEUR_ENDPOINT = UTILISATEUR_ENDPOINT + "/change/password";
    String FIND_UTILISATEUR_BY_ID_ENDPOINT = UTILISATEUR_ENDPOINT + "/{idUtilisateur}";
    String FIND_UTILISATEUR_BY_CODE_ENDPOINT = UTILISATEUR_ENDPOINT + "/{UsernameUtilisateur}";
    String FIND_ALL_UTILISATEUR_ENDPOINT = UTILISATEUR_ENDPOINT + "/all";
    String DELETE_UTILISATEUR_ENDPOINT = UTILISATEUR_ENDPOINT + "/delete/{idUtilisateur}";

    /**
     *  Ventes API ENDPOINT
     */

    String VENTES_ENDPOINT = APP_ROOT + "/ventes";
    String CREATE_VENTES_ENDPOINT = VENTES_ENDPOINT + "/create";
    String FIND_VENTES_BY_ID_ENDPOINT = VENTES_ENDPOINT + "/{idVente}";
    String FIND_VENTES_BY_CODE_ENDPOINT = VENTES_ENDPOINT + "/{codeVente}";
    String FIND_ALL_VENTES_ENDPOINT = VENTES_ENDPOINT + "/all";
    String DELETE_VENTES_ENDPOINT = VENTES_ENDPOINT + "/delete/{idVente}";

    /**
     *  VILLE API ENDPOINT
     */

    String VILLE_ENDPOINT = APP_ROOT + "/villes";
    String CREATE_VILLE_ENDPOINT = VILLE_ENDPOINT + "/create";
    String FIND_VILLE_BY_ID_ENDPOINT = VILLE_ENDPOINT + "/{idVille}";
    String FIND_VILLE_BY_CODE_ENDPOINT = VILLE_ENDPOINT + "/{codeVille}";
    String FIND_ALL_VILLE_ENDPOINT = VILLE_ENDPOINT + "/all";
    String DELETE_VILLE_ENDPOINT = VILLE_ENDPOINT + "/delete/{idVille}";

    /**
     *  PAYS API ENDPOINT pays
     */

    String PAYS_ENDPOINT = APP_ROOT + "/pays";
    String CREATE_PAYS_ENDPOINT = PAYS_ENDPOINT + "/create";
    String FIND_PAYS_BY_ID_ENDPOINT = PAYS_ENDPOINT + "/{idPays}";
    String FIND_PAYS_BY_CODE_ENDPOINT = PAYS_ENDPOINT + "/{codePays}";
    String FIND_ALL_PAYS_ENDPOINT = PAYS_ENDPOINT + "/all";
    String DELETE_PAYS_ENDPOINT = PAYS_ENDPOINT + "/delete/{idPays}";

    /**
     *  Authentication API ENDPOINT
     */
    String AUTHENTICATION_ENDPOINT = APP_ROOT + "/auth";
    String USER_AUTHENTICATION_ENDPOINT = "/authenticate";
    String USER_ISCONNECT_ENDPOINT = "/isConnected";
    String AUTHORIZATION_HEADER = "Authorization";
    String JWT = "JWT";
    String HEADER = "header";

    /**
     *  ADRESSE API ENDPOINT pays
     */

    String ADRESSE_ENDPOINT = APP_ROOT + "/adresses";
    String CREATE_ADRESSE_ENDPOINT = ADRESSE_ENDPOINT + "/create";
    String FIND_ADRESSE_BY_ID_ENDPOINT = ADRESSE_ENDPOINT + "/{idAdresse}";
    String FIND_ADRESSE_BY_ADRESSE1_ENDPOINT = ADRESSE_ENDPOINT + "/{addresse1}";
    String FIND_ALL_ADRESSE_ENDPOINT = ADRESSE_ENDPOINT + "/all";
    String DELETE_ADRESSE_ENDPOINT = ADRESSE_ENDPOINT + "/delete/{idAdresse}";

    /**
     *  PRIVILEGES API ENDPOINT
     */

    String PRIVILEGES_ENDPOINT = APP_ROOT + "/privileges";
    String CREATE_PRIVILEGES_ENDPOINT = PRIVILEGES_ENDPOINT + "/create";
    String FIND_PRIVILEGES_BY_ID_ENDPOINT = PRIVILEGES_ENDPOINT + "/{idPriveleges}";
    String FIND_PRIVILEGES_BY_NOM_ENDPOINT = PRIVILEGES_ENDPOINT + "/{nomPriveleges}";
    String FIND_ALL_PRIVILEGES_ENDPOINT = PRIVILEGES_ENDPOINT + "/all";
    String DELETE_PRIVILEGES_ENDPOINT = PRIVILEGES_ENDPOINT + "/delete/{idPriveleges}";


    /**
     *  MVTSKT API ENDPOINT
     */
    String MVTSKT_ENDPOINT = APP_ROOT + "/mvtstk";
    String FIND_MVTSKT_BY_ID_ARTICLE_ENDPOINT = MVTSKT_ENDPOINT + "/stockreel/{idArticle}";
    String FIND_ALL_MVTSKT_BY_ID_ARTICLE_ENDPOINT = MVTSKT_ENDPOINT + "/filter/article/{idArticle}";
    String ENTREE_MVTSTK_ENDPOINT = MVTSKT_ENDPOINT + "/entree";
    String SORTIE_MVTSTK_ENDPOINT = MVTSKT_ENDPOINT + "/sortie";
    String TRANSFERT_MVTSTK_ENDPOINT = MVTSKT_ENDPOINT + "/transfert";
    String CORRECTION_POS_MVTSTK_ENDPOINT = MVTSKT_ENDPOINT + "/correctionpos";
    String CORRECTION_NEG_MVTSTK_ENDPOINT = MVTSKT_ENDPOINT + "/correctionneg";

}
