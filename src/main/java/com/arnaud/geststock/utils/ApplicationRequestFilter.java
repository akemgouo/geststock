package com.arnaud.geststock.utils;

import com.arnaud.geststock.exception.InvalidJWTException;
import com.arnaud.geststock.model.auth.ExtendedUser;
import com.arnaud.geststock.services.auth.JwtUserDetailsService;
import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/***
 * Nous définirons notre filtre de requête que nous avons mentionné dans notre middleware API
 */
@Component
public class ApplicationRequestFilter extends OncePerRequestFilter {

    private final JwtUserDetailsService jwtUserDetailsService;
    private final JwtTokenUtil jwtTokenUtil;

    @Autowired
    public ApplicationRequestFilter(final JwtUserDetailsService jwtUserDetailsService, final JwtTokenUtil jwtTokenUtil) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.jwtUserDetailsService = jwtUserDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        final String requestTokenHeader = request.getHeader("Authorization");
        String idEntreprise = null;
        if (StringUtils.isNotEmpty(requestTokenHeader) && requestTokenHeader.startsWith("Bearer ")) {
            try {
                final String jwtToken = requestTokenHeader.substring(7);
                //System.out.println(jwtToken);
                final String username = jwtTokenUtil.getUserNameFromToken(jwtToken);
                //System.out.println(username);
                idEntreprise = jwtTokenUtil.extractIdEntreprise(jwtToken);
                //System.out.println(idEntreprise);
                //System.out.println(SecurityContextHolder.getContext().getAuthentication());
                if (StringUtils.isNotEmpty(username) && null == SecurityContextHolder.getContext().getAuthentication()) {
                    UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(username);
                    //System.out.println("JWT KTA " + userDetails.getUsername());
                    if (jwtTokenUtil.validateToken(jwtToken, (ExtendedUser) userDetails)) {
                        //System.out.println("JWT " + userDetails.getUsername());
                        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                                new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                        usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                    }
                }

            } catch (IllegalArgumentException e) {
                //logger.error("Unabled to fecht JWT Token");
                throw new InvalidJWTException("Unabled to fecht JWT Token");
            } catch (ExpiredJwtException e) {
                //logger.error("JWT Toke is expired");
                throw new InvalidJWTException("JWT Toke is expired");
            } catch (Exception e) {
                logger.error(e.getMessage());
                //throw new InvalidJWTException(e.getMessage());
            }
        } else {
            logger.info("JWT Token does not begin with Bearer String");
            //throw new InvalidJWTException("JWT Token does not begin with Bearer String");
        }
        MDC.put("idEntriprise", idEntreprise);
        filterChain.doFilter(request, response);
    }
}

