package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.UtilisateurDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class UtilisateurValidator {

    public static Collection<String> validate(final UtilisateurDto utilisateurDto){

        Collection<String> errors = new ArrayList<>();

        if (utilisateurDto == null){
            errors.add("Le username de l'utilisateur est obligatoire");
            errors.add("Le mot de passe de passe de l'utilisateur est obligatoire");
            errors.add("Le nom de l'utilisateur est obligatoire");
            errors.add("Le prenom de l'utilisateur est obligatoire");
            errors.add("Le mot de passe de l'utilisateur est obligatoire");
            errors.addAll(AdresseValidator.validate(null));
            return errors;
        }

        if(!StringUtils.hasLength(utilisateurDto.getUsername())){
            errors.add("Le username de l'utilisateur est obligatoire");
        }
        if(!StringUtils.hasLength(utilisateurDto.getNom())){
            errors.add("Le nom de l'utilisateur est obligatoire");
        }
        if(!StringUtils.hasLength(utilisateurDto.getPrenom())){
            errors.add("Le prenom de l'utilisateur est obligatoire");
        }
        if(!StringUtils.hasLength(utilisateurDto.getPassword())){
            errors.add("Le mot de passe de l'utilisateur est obligatoire");
        }
        if(!StringUtils.hasLength(utilisateurDto.getEmail())){
            errors.add("L'adresse email de l'utilisateur est obligatoire");
        }
        if(utilisateurDto.getDateNaissance() == null){
            errors.add("La date de naissance de l'utilisateur est obligatoire");
        }
        if(!StringUtils.hasLength(utilisateurDto.getNom())){
            errors.add("Veuillez renseigner le nom de l'utilisateur");
        }

        errors.addAll(AdresseValidator.validate(utilisateurDto.getAdresse()));

        return errors;
    }
}
