package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.AdresseDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class AdresseValidator {

    public static List<String> validate(final AdresseDto adresseDto){

        List<String> errors = new ArrayList<>();
        if(adresseDto == null){
            errors.add("La ville est obligatoire");
            errors.add("L'adresse 1 est obligatoire");
            errors.add("Le code postal");
            return errors;
        }
        if(adresseDto.getVille().getId() == null ){
            errors.add("La ville est obligatoire");
        }
        if(!StringUtils.hasLength(adresseDto.getAdresse1())){
            errors.add("L'adresse 1 est obligatoire");
        }
        if(!StringUtils.hasLength(adresseDto.getCodePostale())){
            errors.add("Le code postal");
        }
        return errors;
    }
}
