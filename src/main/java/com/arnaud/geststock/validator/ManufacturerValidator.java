package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.ManufacturerDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ManufacturerValidator {

    public static List<String> validate(final ManufacturerDto dto){

        List<String> errors = new ArrayList<>();

        if(dto == null){
            errors.add("Le code du fabrican est obligatoire");
            errors.add("Le nom du fabrican");
            return errors;
        }

        if(!StringUtils.hasLength(dto.getCodeManufactorer())){
            errors.add("Le code du fabrican est obligatoire");
        }

        if(!StringUtils.hasLength(dto.getDesignation())){
            errors.add("Le nom du fabrican est obligatoire");
        }

        return errors;
    }
}
