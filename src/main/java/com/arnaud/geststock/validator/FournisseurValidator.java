package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.FournisseurDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class FournisseurValidator {

    public static Collection<String> validate(final FournisseurDto fournisseurDto){

        Collection<String> errors = new ArrayList<>();

        if(fournisseurDto == null){
            errors.add("Le code fournisseur est ogligatoire");
            errors.add("Le nom du fournisseur est ogligatoire");
            errors.add("L'adresse email du fournisseur est ogligatoire");
            errors.add("Le numéro du fournisseur est ogligatoire");
            errors.addAll(AdresseValidator.validate(null));
            return errors;
        }

        if(!StringUtils.hasLength(fournisseurDto.getCodeFournisseur())){
            errors.add("Le code fournisseur est ogligatoire");
        }
        if(!StringUtils.hasLength(fournisseurDto.getDescription())){
            errors.add("Le nom du fournisseur est ogligatoire");
        }
        if(!StringUtils.hasLength(fournisseurDto.getEmail())){
            errors.add("L'adresse email du cleint est ogligatoire");
        }
        if(!StringUtils.hasLength(fournisseurDto.getNumTel())){
            errors.add("Le numéro du cleint est ogligatoire");
        }

        errors.addAll(AdresseValidator.validate(fournisseurDto.getAdresse()));
        return errors;
    }
}
