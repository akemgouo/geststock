package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.EntrepriseDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class EntrepriseValidator {

    public static Collection<String> validate(final EntrepriseDto entrepriseDto){

        Collection<String> errors = new ArrayList<>();

        if(entrepriseDto == null){
            errors.add("Le code de l'entreprise est obligatoire");
            errors.add("Le nom de l'entreprise est obligatoire");
            errors.add("La raison social de l'entreprise est obligatoire");
            errors.add("Email de l'entreprise est obligatoire");
            errors.addAll(AdresseValidator.validate(null));
        }

        if(!StringUtils.hasLength(entrepriseDto.getCode())){
            errors.add("Le code de l'entreprise est obligatoire");
        }

        if(entrepriseDto.getAdresse().getId() == null){
            errors.add("L'adresse de l'entrepise est obligatoire");
        }

        /*errors.addAll(AdresseValidator.validate(entrepriseDto.getAdresse()));*/

        return errors;
    }
}
