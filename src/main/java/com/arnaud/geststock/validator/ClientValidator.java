package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.ClientDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class ClientValidator {

    public static Collection<String> validate(final ClientDto clientDto){

        Collection<String> errors = new ArrayList<>();

        if(clientDto == null){
            errors.add("Le code cleint est ogligatoire");
            errors.add("Le nom du cleint est ogligatoire");
            errors.add("Le prenom cleint est ogligatoire");
            errors.add("L'adresse email du cleint est ogligatoire");
            errors.add("Le numéro du cleint est ogligatoire");
            errors.addAll(AdresseValidator.validate(null));
            return errors;
        }

        if(!StringUtils.hasLength(clientDto.getCodeClient())){
            errors.add("Le code cleint est ogligatoire");
        }
        if(!StringUtils.hasLength(clientDto.getNom())){
            errors.add("Le nom du cleint est ogligatoire");
        }
        if(!StringUtils.hasLength(clientDto.getPrenom())){
            errors.add("Le prenom cleint est ogligatoire");
        }
        if(!StringUtils.hasLength(clientDto.getEmail())){
            errors.add("L'adresse email du cleint est ogligatoire");
        }
        if(!StringUtils.hasLength(clientDto.getNumTel())){
            errors.add("Le numéro du cleint est ogligatoire");
        }
        errors.addAll(AdresseValidator.validate(clientDto.getAdresse()));
        return errors;
    }
}
