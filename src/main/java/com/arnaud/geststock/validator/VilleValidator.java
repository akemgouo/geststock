package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.VilleDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class VilleValidator {

    public static Collection<String> validate(final VilleDto dto){
        List<String> errors = new ArrayList<>();

        if (dto == null){
            errors.add("le code de la ville est obligatoire");
            errors.add("le champs pays est obligatoire");
        }

        if (!StringUtils.hasLength(dto.getCode())){
            errors.add("le code de la ville est obligatoire");
        }

        if(dto.getPays() == null){
            errors.add("le champs pays est obligatoire");
        }else {
            if (dto.getPays().getId()== null){
                errors.add("le pays est obligatoire");
            }
        }

        return errors;
    }
}
