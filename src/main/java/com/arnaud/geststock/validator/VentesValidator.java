package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.VentesDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class VentesValidator {

    public static Collection<String> validate(final VentesDto dto){
        List<String> errors = new ArrayList<>();

        if(dto == null){
            errors.add("Veuillez reneigner le code de la commande");
            errors.add("Veuillez reneigner la date de la commande");
            errors.add("Veuillez reneigner le client de la commande");
            return errors;
        }

        if(!StringUtils.hasLength(dto.getCode())){
            errors.add("Veuillez reneigner le code de la commande");
        }
        if(dto.getDateVente() ==null){
            errors.add("Veuillez reneigner la date de la commande");
        }

        return errors;
    }

}
