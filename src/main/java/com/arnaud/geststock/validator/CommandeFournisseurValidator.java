package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.CommandeFournisseurDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CommandeFournisseurValidator {

    public static Collection<String> validate(final CommandeFournisseurDto dto){

        List<String> errors = new ArrayList<>();

        if(dto == null){
            errors.add("Veuillez reneigner le code de la commande");
            errors.add("Veuillez reneigner la date de la commande");
            errors.add("Veuillez reneigner l'état de la commande");
            errors.add("Veuillez reneigner le client de la commande");
            return errors;
        }

        if(!StringUtils.hasLength(dto.getCode())){
            errors.add("Veuillez reneigner le code de la commande");
        }
        if(dto.getDateCommande() ==null){
            errors.add("Veuillez reneigner la date de la commande");
        }
        if(!StringUtils.hasLength(dto.getStatus().toString())){
            errors.add("Veuillez reneigner l'état de la commande");
        }
        if(dto.getFournisseur() == null || dto.getFournisseur().getId() == null){
            errors.add("Veuillez reneigner le client de la commande");
        }

        return errors;

    }
}
