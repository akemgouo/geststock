package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.PrivilegesDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PrivilegesValidator {

    public static List<String> validate(final PrivilegesDto dto){

        List<String> errors = new ArrayList<>();

        if( dto == null){
            errors.add("Le nom du privilege est obligatoire");
        }

        if(!StringUtils.hasLength(dto.getNom())){
            errors.add("Le nom du privilege est obligatoire");
        }
        return errors;
    }
}
