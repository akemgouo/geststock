package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.RolesDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class RolesValidator {

    public static Collection<String> validate(final RolesDto rolesDto){

        Collection<String> errors = new ArrayList<>();

        if(!StringUtils.hasLength(rolesDto.getRoleName()) || rolesDto ==null)
            errors.add("Le roles est obligatoire");
        return errors;
    }
}
