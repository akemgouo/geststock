package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.ArticleDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class ArticleValidator {

    public static Collection<String> validate(final ArticleDto articleDto){

        Collection<String> errors = new ArrayList<>();

        if (articleDto == null){
            errors.add("Le code de l'article est obligatoire");
            errors.add("La désigation de l'article est obligatoire");
            errors.add("Le prix unitaire HT de l'article est obligatoire");
            errors.add("Le taux TVA de l'article est obligatoire");
            errors.add("L'unité de commande est obligatoire");
            errors.add("L'unité de sortie est obligatoire");
            errors.add("Le prix unitaire TTC de l'article est obligatoire");
            errors.add("Veuillez selectionner une category");
            return errors;
        }
        if(!StringUtils.hasLength(articleDto.getCodeArticle())){
            errors.add("Le code de l'article est obligatoire");
        }
        if(!StringUtils.hasLength(articleDto.getDesignation())){
            errors.add("La désigation de l'article est obligatoire");
        }
        if(articleDto.getPrixUnitaire() == null){
            errors.add("Le prix unitaire HT de l'article est obligatoire");
        }
        if(articleDto.getTauxTva() == null){
            errors.add("Le taux TVA de l'article est obligatoire");
        }
        if(articleDto.getPrixUnitaireTtc() == null){
            errors.add("Le prix unitaire TTC de l'article est obligatoire");
        }
        if(articleDto.getCategory() == null){
            errors.add("Veuillez selectionner une category");
        }
        if(!StringUtils.hasLength(articleDto.getIssueunit().name())){
            errors.add("L'unité de sortie est obligatoire");
        }
        if(!StringUtils.hasLength(articleDto.getOrderunit().name())){
            errors.add("L'unité de commande est obligatoire");
        }

        return errors;
    }
}
