package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.PaysDto;
import com.arnaud.geststock.dto.RolesDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class PaysValidator {

    public static Collection<String> validate(final PaysDto paysDto){

        Collection<String> errors = new ArrayList<>();

        if(paysDto == null){
            errors.add("Le code du pays est obligatoire");
            errors.add("La description du pays est obligatoire");
            return errors;
        }

        if(!StringUtils.hasLength(paysDto.getCode()))
            errors.add("Le code du pays est obligatoire");
        if(!StringUtils.hasLength(paysDto.getDescription()))
            errors.add("La description du pays est obligatoire");
        return errors;
    }
}
