package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.CategoryDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class CategoryValidator {

    public static Collection<String> validate(final CategoryDto categoryDto){

        Collection<String> errors = new ArrayList<>();

        if(categoryDto == null || !StringUtils.hasLength(categoryDto.getCode())){
            errors.add("Veuillez renseigner le code de la categorie");
        }

        return errors;
    }
}
