package com.arnaud.geststock.validator;

import com.arnaud.geststock.dto.MvtStkDto;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

public class MvtStkValidator {

    public static Collection<String> validate(final MvtStkDto dto){

        Collection<String> errors = new ArrayList<>();

        if(dto == null){
            errors.add("La date du mouvement de stock est obligatoire");
            errors.add("Le type de mouvent est obligatoire");
            errors.add("La quantité est obligatoire ou supérieur à zero");
            errors.add("L'article est obligatoire");
            errors.add("Le type de mouvement de stock est obligatoire");
            return errors;
        }

        if(dto.getDateMvtStk() == null)
            errors.add("La date du mouvement de stock est obligatoire");
        if(!StringUtils.hasLength(dto.getTypeMvtStk().toString()))
            errors.add("Le type de mouvent est obligatoire");
        if(dto.getQuantite()== null || dto.getQuantite().compareTo(BigDecimal.ZERO) == 0)
            errors.add("La quantité est obligatoire ou supérieur à zero");
        if(dto.getArticle() == null || dto.getArticle().getId() == null)
            errors.add("L'article est obligatoire");
        if(!StringUtils.hasLength(dto.getTypeMvtStk().name()))
            errors.add("Le type de mouvement de stock est obligatoire");

        return errors;
    }
}
