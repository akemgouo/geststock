package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.FournisseurDto;

import java.util.Collection;

public interface FournisseurService {

    FournisseurDto save(final FournisseurDto dto);

    FournisseurDto findById(final Integer id);

    Collection<FournisseurDto> findAll();

    FournisseurDto findFournisseurByCodeFournisseur (final String code);

    void delete(final Integer id);
}
