package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.PrivilegesDto;

import java.util.Collection;

public interface PrivilegesService {

    PrivilegesDto save(final PrivilegesDto dto);

    PrivilegesDto findById(final Integer id);

    PrivilegesDto findPrivilegesByNom(final String nom);

    Collection<PrivilegesDto> findAll();

    void delete(final Integer id);
}
