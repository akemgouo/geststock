package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.ClientDto;

import java.util.Collection;

public interface ClientService {

    ClientDto save(final ClientDto dto);

    ClientDto findById(final Integer id);

    Collection<ClientDto> findAll();

    void delete(final Integer id);

    ClientDto findClientByCodeClient(final String clientCode);

    ClientDto findClientByEmail(final String email);

    Boolean existsClientByCodeClient(final String clientCode);

    Boolean existsClientByEmail(final String email);
}
