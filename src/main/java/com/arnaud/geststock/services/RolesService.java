package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.RolesDto;

import java.util.Collection;

public interface RolesService {

    RolesDto save(final RolesDto dto);

    RolesDto findById(final Integer id);

    Collection<RolesDto> findAll();

    void delete(final Integer id);
}
