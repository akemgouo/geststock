package com.arnaud.geststock.services.strategy;

import java.io.InputStream;

public interface Strategy<T> {
    T savePhote(InputStream photo, String titre );
}
