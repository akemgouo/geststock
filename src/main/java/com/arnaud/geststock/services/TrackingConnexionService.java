package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.TrackingConnexionDto;

public interface TrackingConnexionService {

    void save(TrackingConnexionDto dto);
}
