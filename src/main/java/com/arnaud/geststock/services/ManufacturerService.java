package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.ManufacturerDto;

import java.util.Collection;

public interface ManufacturerService {

    ManufacturerDto save(final ManufacturerDto dto);

    ManufacturerDto findManufacturerByCodeManufacturer(final String code);

    ManufacturerDto findManufacturerById(final Integer id);

    Collection<ManufacturerDto> findAll();

    void delete (final Integer id);
}
