package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.EntrepriseDto;

import java.util.Collection;

public interface EnterpriseService {

    EntrepriseDto save(final EntrepriseDto dto);

    EntrepriseDto findById(final Integer id);

    Collection<EntrepriseDto> findAll();

    EntrepriseDto findEntrepriseByCode(final String code);

    void delete(final Integer id);
}
