package com.arnaud.geststock.services.auth;

import com.arnaud.geststock.dto.UtilisateurDto;
import com.arnaud.geststock.model.auth.ExtendedUser;
import com.arnaud.geststock.repository.TrackingConnexionRepository;
import com.arnaud.geststock.services.UtilisateurService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Slf4j
public class JwtUserDetailsService implements UserDetailsService {

    private final UtilisateurService utilisateurService;
    private final TrackingConnexionRepository trackingConnexionRepository;

    @Autowired
    public JwtUserDetailsService(final UtilisateurService utilisateurService, final TrackingConnexionRepository trackingConnexionRepository) {
        this.utilisateurService = utilisateurService;
        this.trackingConnexionRepository = trackingConnexionRepository;
    }

    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {

        UtilisateurDto utilisateur = utilisateurService.findUtilisateurByEmail(email);
        System.out.println(utilisateur.getUsername());
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        if (!utilisateur.getRoles().isEmpty()){
            utilisateur.getRoles().forEach(rolesDto -> authorities.add(new SimpleGrantedAuthority(rolesDto.getRoleName())));
        }
        return new ExtendedUser(
                utilisateur.getUsername(),
                utilisateur.getPassword(),
                utilisateur.getEntreprise().getId(),
                utilisateur.getDateNaissance(),
                utilisateur.getNom(),
                utilisateur.getPrenom(),
                utilisateur.getEmail(),
                utilisateur.getPhoto(),
                utilisateur.isEnabled(),
                utilisateur.isTokenExpired(),
                utilisateur.getStatus(),
                utilisateur.getIsspamdin()
                ,authorities);
    }
}
