package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.VilleDto;

import java.util.Collection;

public interface VilleService {

    VilleDto save(final VilleDto dto);

    VilleDto findById(final Integer id);

    VilleDto findVilleByCode(final String code);

    Boolean existsByCode(final String code);

    Collection<VilleDto> findAll();

    void delete(final Integer id);
}
