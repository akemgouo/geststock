package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.AdresseDto;

import java.util.Collection;

public interface AdresseService {

    AdresseDto save(final AdresseDto dto);

    AdresseDto findAdresseByAdresse1(final String addresse1);

    AdresseDto findAdressebyId(final Integer id);

    Collection<AdresseDto> findAll();

    void delete (final Integer id);
}
