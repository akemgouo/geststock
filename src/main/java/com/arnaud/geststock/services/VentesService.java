package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.VentesDto;

import java.util.Collection;

public interface VentesService {

    VentesDto save(final VentesDto dto);

    VentesDto findById(final Integer id);

    VentesDto findVentesByCode(final String code);

    Collection<VentesDto> findAll();

    void delete(final Integer id);
}
