package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.PaysDto;

import java.util.Collection;

public interface PaysService {

    PaysDto save(final PaysDto dto);

    PaysDto findById(final Integer id);

    Boolean existsByCode (final String code);

    PaysDto findPaysByCode(final String code);

    Collection<PaysDto> findAll();

    void delete(final Integer id);
}
