package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.ChangePasswordUserDto;
import com.arnaud.geststock.dto.UtilisateurDto;

import java.util.Collection;

public interface UtilisateurService {

    UtilisateurDto save(final UtilisateurDto dto);

    UtilisateurDto findById(final Integer id);

    Collection<UtilisateurDto> findAll();

    UtilisateurDto findUtilisateurByUsername(final String username);

    UtilisateurDto findUtilisateurByEmail(final String email);

    UtilisateurDto changePasswordUtilisateur(final ChangePasswordUserDto dto);

    void delete(final Integer id);
}
