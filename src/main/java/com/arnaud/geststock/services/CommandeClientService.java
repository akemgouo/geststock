package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.CommandeClientDto;
import com.arnaud.geststock.dto.LigneCommandeClientDto;
import com.arnaud.geststock.model.CommandeSts;

import java.math.BigDecimal;
import java.util.Collection;

public interface CommandeClientService {

    CommandeClientDto save(final CommandeClientDto dto);

    CommandeClientDto updateStatusCommande(final Integer idCommande, final CommandeSts commandeSts);

    CommandeClientDto updateQuantiteCommande(final Integer idCommande, final Integer idLigneCommande, final BigDecimal quantite);

    CommandeClientDto updateClientCommande(final Integer idCommande, final Integer idClient);

    CommandeClientDto updateArticleCommande(final Integer idCommande, final Integer idLigngeCommande, final Integer newIdArticle);

    CommandeClientDto findById(final Integer id);

    CommandeClientDto findCommandeClientByCode(final String code);

    Collection<CommandeClientDto> findAll();

    Collection<LigneCommandeClientDto> findAllByCommandeClientId(final Integer id);

    // Delete article c'est supprimer une ligne de commande client
    CommandeClientDto deleteLigneCommande(final Integer idCommande, final Integer idLigngeCommande);

    void delete(final Integer id);
}
