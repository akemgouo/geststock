package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.AdresseDto;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.repository.AdresseRepository;
import com.arnaud.geststock.services.AdresseService;
import com.arnaud.geststock.validator.AdresseValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class AdresseServiceImpl implements AdresseService {

    private final AdresseRepository adresseRepository;

    public AdresseServiceImpl(final AdresseRepository adresseRepository) {
        this.adresseRepository = adresseRepository;
    }

    @Override
    public AdresseDto save(final AdresseDto dto) {
        List<String> errors = AdresseValidator.validate(dto);
        if(!errors.isEmpty()){
            throw new InvalidEntityException("L'adresse n'est pas valide", ErrorCodes.ADRESSE_NOT_VALID, errors);
        }
        return AdresseDto.convertEntityToDto(adresseRepository.save(AdresseDto.convertDtoToEntity(dto)));
    }

    @Override
    public AdresseDto findAdresseByAdresse1(final String addresse1) {
        if(!StringUtils.hasLength(addresse1)){
            log.error("Adresse 1 is null");
            return null;
        }
        return adresseRepository.findAdresseByAdresse1(addresse1)
                .map(AdresseDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException("L'adresse 1 fourni par adresse1 = "+ addresse1 + " n'existe pas en BD",
                                ErrorCodes.ADRESSE_NOT_FOUND)
                );
    }

    @Override
    public AdresseDto findAdressebyId(final Integer id) {
        if (id == null){
            log.error("L'identifiant est null");
            return null;
        }
        return adresseRepository.findById(id)
                .map(AdresseDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("L'adresse fournit avec l'ID = " + id + " n'existe pas en BD",
                                ErrorCodes.ADRESSE_NOT_FOUND)
                );
    }

    @Override
    public Collection<AdresseDto> findAll() {
        return adresseRepository.findAll()
                .stream()
                .map(AdresseDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(final Integer id) {
        if (id == null){
            log.error("L'identifiant est null");
            return ;
        }
        adresseRepository.deleteById(id);
    }
}
