package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.ClientDto;
import com.arnaud.geststock.exception.*;
import com.arnaud.geststock.model.CommandeClient;
import com.arnaud.geststock.repository.ClientRepository;
import com.arnaud.geststock.repository.CommandeClientRepository;
import com.arnaud.geststock.services.ClientService;
import com.arnaud.geststock.validator.ClientValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class ClientServiceImpl implements ClientService {

    //@Autowired
    private final ClientRepository clientRepository;
    private final CommandeClientRepository commandeClientRepository;

    @Autowired
    public ClientServiceImpl(final ClientRepository clientRepository, final CommandeClientRepository commandeClientRepository){
        this.clientRepository = clientRepository;
        this.commandeClientRepository = commandeClientRepository;
    }

    @Override
    public ClientDto save(ClientDto dto) {

        List<String> errors = (List<String>) ClientValidator.validate(dto);
        if (!errors.isEmpty()){
            log.error("Client is not valid {}", dto);
            throw new InvalidEntityException("Le client n'est pas valide", ErrorCodes.CLIENT_NOT_VALID, errors);
        }
        if (dto.getId() == null ){
            if (!existsClientByCodeClient(dto.getCodeClient())){
                log.error("Client exist avec le code = ", dto.getCodeClient());
                throw new EntityAlreadyExistException("Le client existe deja en BD avec ce code", ErrorCodes.CLIENT_ALREADY_EXIST);
            }else if(!existsClientByEmail(dto.getEmail())){
                log.error("Client exist avec email = ", dto.getEmail());
                throw new EntityAlreadyExistException("Le client existe deja en BD avec l'adresse email", ErrorCodes.CLIENT_ALREADY_EXIST);
            }
        }
        return ClientDto.convertEntityToDto(clientRepository.save(ClientDto.convertDtoToEntity(dto)));
    }

    @Override
    public ClientDto findById(Integer id) {
        if(id == null){
            log.error("Client ID is null");
            return null;
        }
        return clientRepository.findById(id)
                .map(ClientDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException("Aucune Client de l'ID = " + id + " n'est trouvé dans la BDD",
                                ErrorCodes.CLIENT_NOT_FOUND)
                );
    }

    @Override
    public Collection<ClientDto> findAll() {
        return clientRepository.findAll()
                .stream()
                .map(ClientDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(Integer id) {
        if (id == null){
            log.error("Client ID is null");
            return;
        }
        Collection<CommandeClient> commandeClients = commandeClientRepository.findAllByClientId(id);
        if(!commandeClients.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer un client qui à déjà des commandes",
                    ErrorCodes.CLIENT_ALREADY_IN_USE);
        }
        clientRepository.deleteById(id);
    }

    @Override
    public ClientDto findClientByCodeClient(String clientCode) {
        if(!StringUtils.hasLength(clientCode)){
            log.error("Code client inexistant");
            return null;
        }
        return clientRepository.findClientByCodeClient(clientCode)
                .map(ClientDto::convertEntityToDto)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Le client avec le Code = " + clientCode + " n'existe pas dans la bd", ErrorCodes.CLIENT_NOT_FOUND)
                );
    }

    @Override
    public ClientDto findClientByEmail(String email) {
        return clientRepository.findClientByEmail(email)
                .map(ClientDto::convertEntityToDto)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Le " +
                                "client avec le Code = " + email + " n'existe pas dans la bd", ErrorCodes.CLIENT_NOT_FOUND)
                );
    }

    @Override
    public Boolean existsClientByCodeClient(final String clientCode) {
        if (!StringUtils.hasLength(clientCode)) {
            log.error("Client Code is null");
            return null;
        }
        return clientRepository.existsClientByCodeClient(clientCode).booleanValue();
    }

    @Override
    public Boolean existsClientByEmail(final String email) {
        if (!StringUtils.hasLength(email)) {
            log.error("Client email is null");
            return null;
        }
        return clientRepository.existsClientByEmail(email).booleanValue();
    }
}
