package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.EntrepriseDto;
import com.arnaud.geststock.dto.UtilisateurDto;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.model.Status;
import com.arnaud.geststock.repository.EntrepriseRepository;
import com.arnaud.geststock.services.EnterpriseService;
import com.arnaud.geststock.services.RolesService;
import com.arnaud.geststock.services.UtilisateurService;
import com.arnaud.geststock.validator.EntrepriseValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
@Slf4j
public class EntrepriseServiceImpl implements EnterpriseService {

    private final EntrepriseRepository entrepriseRepository;
    private final UtilisateurService utilisateurService;
    private final RolesService rolesService;

    @Autowired
    public EntrepriseServiceImpl(final EntrepriseRepository entrepriseRepository, final UtilisateurService utilisateurService, final RolesService rolesService){
        this.entrepriseRepository = entrepriseRepository;
        this.utilisateurService = utilisateurService;
        this.rolesService = rolesService;
    }

    @Override
    public EntrepriseDto save(EntrepriseDto dto) {
        List<String> errors = (List<String>) EntrepriseValidator.validate(dto);
        if(!errors.isEmpty()){
            log.error("Entreprise is not valid {}", dto);
            throw  new InvalidEntityException("Entreprise is not valid", ErrorCodes.ENTREPRISE_NOT_VALID, errors);
        }
        /*EntrepriseDto savedEntreprise = EntrepriseDto.convertEntityToDto(entrepriseRepository.save(
                EntrepriseDto.convertDtoToEntity(dto)
        ));*/

        return EntrepriseDto.convertEntityToDto(entrepriseRepository.save(
                EntrepriseDto.convertDtoToEntity(dto)));

        /*UtilisateurDto utilisateur = fromEntreprise(savedEntreprise);

        UtilisateurDto savedUser = utilisateurService.save(utilisateur);

        RolesDto rolesDto = RolesDto.builder()
                .roleName("ADMIN")
                .build();
        rolesService.save(rolesDto);*/
        //return savedEntreprise;
    }

    @Override
    public EntrepriseDto findById(Integer id) {
        if(id == null){
            log.error("Entrise ID not found");
            //TODO Exception
        }
        return entrepriseRepository.findById(id)
                .map(EntrepriseDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException("Aucune Entreprise avec l'ID = " + id + " n'est trouvé dans la BDD",
                                ErrorCodes.ENTREPRISE_NOT_FOUND)
                );
    }

    @Override
    public Collection<EntrepriseDto> findAll() {
        return entrepriseRepository.findAll()
                .stream()
                .map(EntrepriseDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    private UtilisateurDto fromEntreprise(EntrepriseDto dto){
        return UtilisateurDto.builder()
                .nom(dto.getNom())
                .email(dto.getEmail())
                .enabled(true)
                .status(Status.ACTIVE)
                .tokenExpired(true)
                .password(generateRandomPassword())
                .username(dto.getCode())
                .prenom(dto.getRaisonSocial())
                .entreprise(dto)
                .dateNaissance(Instant.now())
                .photo(dto.getPhoto())
                .build();
    }

    private String generateRandomPassword(){ return "P@ssw0rd";}

    @Override
    public EntrepriseDto findEntrepriseByCode(String code) {
        if(!StringUtils.hasLength(code)){
            log.error("Entreprise code not found");
            //TODO Exception
            return null;
        }
        return entrepriseRepository.findEntrepriseByCode(code)
                .map(EntrepriseDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException("Aucune Entreprise avec le code = " + code + " n'est trouvé dans la BDD",
                                ErrorCodes.ENTREPRISE_NOT_FOUND)
                );
    }

    @Override
    public void delete(Integer id) {
        if(id == null){
            log.error("Entreprise ID not found");
            return;
        }
        entrepriseRepository.deleteById(id);
    }
}
