package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.RolesDto;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.repository.RolesRepository;
import com.arnaud.geststock.services.RolesService;
import com.arnaud.geststock.validator.RolesValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
@Slf4j
@Transactional
public class RolesServiceImpl implements RolesService {

    private final RolesRepository rolesRepository;

    @Autowired
    public RolesServiceImpl(final RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }

    @Override
    public RolesDto save(RolesDto dto) {
        List<String> errors = (List<String>) RolesValidator.validate(dto);
        if(!errors.isEmpty()){
            log.error("Roles is not valid {}", dto);
            throw  new InvalidEntityException("Roles is not valid", ErrorCodes.ENTREPRISE_NOT_VALID, errors);
        }
       return RolesDto.convertEntityToDto(rolesRepository.save(RolesDto.convertDtoToEntity(dto)));
    }

    @Override
    public RolesDto findById(Integer id) {
        return null;
    }

    @Override
    public Collection<RolesDto> findAll() {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }
}
