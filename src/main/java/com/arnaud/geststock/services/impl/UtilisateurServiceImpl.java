package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.ChangePasswordUserDto;
import com.arnaud.geststock.dto.UtilisateurDto;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.exception.InvalidOperationException;
import com.arnaud.geststock.model.Utilisateur;
import com.arnaud.geststock.repository.UtilisateurRepository;
import com.arnaud.geststock.services.UtilisateurService;
import com.arnaud.geststock.validator.UtilisateurValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class UtilisateurServiceImpl implements UtilisateurService {

    private final UtilisateurRepository utilisateurRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UtilisateurServiceImpl(final UtilisateurRepository utilisateurRepository){
        this.utilisateurRepository = utilisateurRepository;
        //this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UtilisateurDto save(final UtilisateurDto dto) {

        List<String> errors = (List<String>) UtilisateurValidator.validate(dto);
        if(!errors.isEmpty()){
            log.error("Utilisateur is not valid {}", dto);
            throw new InvalidEntityException("L'utilisateur n'est pas valide", ErrorCodes.UTILISATEUR_NOT_VALID, errors);
        }

        dto.setPassword(passwordEncoder.encode(dto.getPassword()));

        return UtilisateurDto.convertEntityToDto(utilisateurRepository.save(UtilisateurDto.convertDtoToEntity(dto)));
    }

    @Override
    public UtilisateurDto findById(final Integer id) {
        if(id == null){
            log.error("Utilisateur ID is null");
            return null;
        }
        return utilisateurRepository.findById(id)
                .map(UtilisateurDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException("Auncun utilisateur avec l'ID = " + id + " n'a été trouvé dans la BDD",
                                ErrorCodes.UTILISATEUR_NOT_FOUND)
                );
    }

    @Override
    public Collection<UtilisateurDto> findAll() {
        return utilisateurRepository.findAll()
                .stream()
                .map(UtilisateurDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public UtilisateurDto findUtilisateurByUsername(final String username) {
        if (!StringUtils.hasLength(username)){
            log.error("Utilisateur avec le username is null");
            return null;
        }
        return utilisateurRepository.findUtilisateurByUsername(username)
                .map(UtilisateurDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException("Auncun utilisateur avec ce username = " + username + " n'a été trouvé dans la BDD",
                                    ErrorCodes.UTILISATEUR_NOT_FOUND)
                );
    }

    @Override
    public UtilisateurDto findUtilisateurByEmail(final String email) {
        if (!StringUtils.hasLength(email)){
            log.error("Utilisateur avec le username is null");
            return null;
        }
        return  utilisateurRepository.findUtilisateurByEmail(email)
                .map(UtilisateurDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException("Auncun utilisateur avec cet email = " + email + " n'a été trouvé dans la BDD",
                                ErrorCodes.UTILISATEUR_NOT_FOUND)
                );
    }

    @Override
    public UtilisateurDto changePasswordUtilisateur(final ChangePasswordUserDto dto) {
        validateDtoUser(dto);
        Optional<Utilisateur> utilisateurOptional = utilisateurRepository.findById(dto.getId());
        if(utilisateurOptional.isEmpty()){
            log.warn("Aucun utilisateur n'a été trouvé avec l' ID = "+ dto.getId());
            throw new EntityNotFoundException("Aucun utilisateur n'a été trouvé avec l' ID = "+ dto.getId(),
                    ErrorCodes.UTILISATEUR_NOT_FOUND);
        }
        Utilisateur utilisateur = utilisateurOptional.get();
        utilisateur.setPassword(passwordEncoder.encode(dto.getPassword()));
        return UtilisateurDto.convertEntityToDto(utilisateurRepository.save(utilisateur));
    }

    @Override
    public void delete(Integer id) {
        if(id == null){
            log.error("Utilisateur ID is null");
            return;
        }
        utilisateurRepository.deleteById(id);
    }

    private void validateDtoUser(final ChangePasswordUserDto dto){
        if (dto == null){
            log.warn("Impossible de modifier le mot de passe avec un object NULL");
            throw new InvalidOperationException("Aucun information n'a été fourni pour changer le mot de passe",
                    ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
        }
        if(dto.getId() == null){
            log.warn("Impossible de modifier le mot de passe avec un ID NULL");
            throw new InvalidOperationException("ID utilisateur NULL impossible de modifier le mot de passe",
                    ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
        }

        if(!StringUtils.hasLength(dto.getPassword()) || !StringUtils.hasLength(dto.getConfirmPassword())){
            log.warn("Impossible de modifier le mot de passe avec un mot de passe NULL");
            throw new InvalidOperationException("Mot de passe utilisateur NULL impossible de modifier le mot de passe",
                    ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
        }

        if(!dto.getPassword().equals(dto.getConfirmPassword())){
            log.warn("Impossible de modifier le mot de passe utilisateur avec deux mot de passe differents");
            throw new InvalidOperationException("Mot de passe utilisateur non conforme :: Impossible de modifier le mot de passe",
                    ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
        }
    }
}
