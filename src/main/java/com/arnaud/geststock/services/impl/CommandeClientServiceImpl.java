package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.*;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.exception.InvalidOperationException;
import com.arnaud.geststock.model.*;
import com.arnaud.geststock.repository.ArticleRepository;
import com.arnaud.geststock.repository.ClientRepository;
import com.arnaud.geststock.repository.CommandeClientRepository;
import com.arnaud.geststock.repository.LigneCommandeClientRepository;
import com.arnaud.geststock.services.CommandeClientService;
import com.arnaud.geststock.services.MvtStkService;
import com.arnaud.geststock.validator.ArticleValidator;
import com.arnaud.geststock.validator.CommandeClientValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Service
@Slf4j
public class CommandeClientServiceImpl implements CommandeClientService {

    private final CommandeClientRepository commandeClientRepository;

    private final LigneCommandeClientRepository ligneCommandeClientRepository;

    private final ClientRepository clientRepository;

    private final ArticleRepository articleRepository;

    private final MvtStkService mvtStkService;

    @Autowired
    public CommandeClientServiceImpl(final CommandeClientRepository commandeClientRepository,
                                     final ClientRepository clientRepository,
                                     final ArticleRepository articleRepository,
                                     final LigneCommandeClientRepository ligneCommandeClientRepository,
                                     final MvtStkService mvtStkService){
        this.articleRepository = articleRepository;
        this.clientRepository = clientRepository;
        this.commandeClientRepository = commandeClientRepository;
        this.ligneCommandeClientRepository = ligneCommandeClientRepository;
        this.mvtStkService = mvtStkService;
    }

    @Override
    public CommandeClientDto save(CommandeClientDto dto) {

        // Validation global sur la commande client
        List<String> errors = (List<String>) CommandeClientValidator.validate(dto);

        // On verifi si on a des erreur de validation et
        if(!errors.isEmpty()){
            log.error("Commande client n'est pas valide ");
            throw new InvalidEntityException("La commande client n'est pas valide ", ErrorCodes.COMMANDE_CLIENT_NOT_VALID, errors);
        }

        if(dto.getId() != null && dto.isCommandeLivree()){
            throw new InvalidOperationException("Impossible de modifier la commande lorsqu'elle est livrée", ErrorCodes.COMMANDE_CLIENT_CANNOT_MODIFIED);
        }

        //Verifie si le client existe dans la base de données
        Optional<Client> client = clientRepository.findById(dto.getClient().getId());
        if(client.isEmpty()){
            log.warn("Client with ID {} was not found in the DB", dto.getClient().getId());
            throw new EntityNotFoundException("Aucun client avec l'ID" + dto.getClient().getId() + " n'a étét trouvé dans BDD",
                    ErrorCodes.CLIENT_NOT_FOUND);
        }

        // Verifier les ligne clients cad verifier que les articles existe dans ma base donnees
        List<String> articleErrors = new ArrayList<>();
        // Si nous avons des commande client on doit valider que chaque article existe bien dans la base de donnees
        if (dto.getLigneCommandeClients() != null){
            dto.getLigneCommandeClients().forEach(
                    ligCmdClt -> {
                        if(ligCmdClt.getArticle() != null){
                            Optional<Article> article = articleRepository.findById(ligCmdClt.getArticle().getId());
                            if (article.isEmpty()){
                                articleErrors.add("L'article avec l'ID = " + ligCmdClt.getArticle().getId() + " n'existe pas");
                            }
                        } else{
                            articleErrors.add("Impossible d'enregistrer une commande avec un article null");
                        }
                    });
        }

        // S'il y a des article qui n'existe pas on leve une exception
        if(!articleErrors.isEmpty()){
            log.warn("Article n'existe pas dans la BDD");
            throw new InvalidEntityException("Article n'existe pas dans la BDD",
                    ErrorCodes.ARTICLE_NOT_FOUND, articleErrors);
        }

        // On enregister notre commande client sans les lignes
        CommandeClient savedCmdClt =  commandeClientRepository.save(CommandeClientDto.convertDtoToEntity(dto));

        // on verifie que notre commande client contient les lignes, on parcours chaque ligne pour inserer nos lignecommande client
        if ( dto.getLigneCommandeClients() != null){
            dto.getLigneCommandeClients().forEach(ligCmdClt ->{
                LigneCommandeClient ligneCommandeClient = LigneCommandeClientDto.convertDtoToEntity(ligCmdClt);
                ligneCommandeClient.setCommandeClient(savedCmdClt);
                ligneCommandeClientRepository.save(ligneCommandeClient);
            });
        }
        // On renvoi comme reponse notre client Dto
        return CommandeClientDto.convertEntityToDto(savedCmdClt);
    }

    @Override
    public CommandeClientDto updateStatusCommande(final Integer idCommande, final CommandeSts commandeSts) {
        checkIdCommande(idCommande);
        if(!StringUtils.hasLength(String.valueOf(commandeSts))){
            log.warn("L'état de la commande client est NULL");
            throw new InvalidOperationException("Impossible de modifier le statut de la commande avec un statut null",
                    ErrorCodes.COMMANDE_CLIENT_CANNOT_MODIFIED);
        }
        CommandeClientDto  commandeClientDto = checkEtatCommande(idCommande);
        commandeClientDto.setStatus(commandeSts);

        CommandeClient savedCmdClt = commandeClientRepository.save(CommandeClientDto.convertDtoToEntity(commandeClientDto));
        if(commandeClientDto.isCommandeLivree())
            updateMvtStk(idCommande);
        return CommandeClientDto.convertEntityToDto(savedCmdClt);
    }

    @Override
    public CommandeClientDto updateQuantiteCommande(final Integer idCommande, final Integer idLigneCommande, final BigDecimal quantite) {
        checkIdCommande(idCommande);
        checkIdLigneCommande(idLigneCommande);

        if(quantite == null || quantite.compareTo(BigDecimal.ZERO) == 0){
            log.error("La quantité ne doit pas être NULL");
            throw new InvalidOperationException("Impossible de modifier l'état de la commande avec une ligne quantité null ou zero", ErrorCodes.COMMANDE_CLIENT_CANNOT_MODIFIED);
        }

        CommandeClientDto  commandeClientDto = checkEtatCommande(idCommande);

        Optional<LigneCommandeClient> ligneCommandeClientOptional = findLigneCommandeClient(idLigneCommande);
        LigneCommandeClient ligneCommandeClient = ligneCommandeClientOptional.get();
        ligneCommandeClient.setQuantite(quantite);
        ligneCommandeClientRepository.save(ligneCommandeClient);

        return commandeClientDto;
    }

    @Override
    public CommandeClientDto updateClientCommande(final Integer idCommande, final Integer idClient) {

        checkIdCommande(idCommande);
        if(idClient == null){
            log.error("L'ID du client est NULL");
            throw new InvalidOperationException("Impossible de modifier le statut de la commande avec un Id client null", ErrorCodes.COMMANDE_CLIENT_CANNOT_MODIFIED);
        }
        CommandeClientDto  commandeClientDto =  checkEtatCommande(idCommande);
        Optional<Client> clientOptional = clientRepository.findById(idClient);
        if(clientOptional.isEmpty()){
            throw new EntityNotFoundException("Aucun client avec l'ID" + idClient + " n'a étét trouvé dans BDD",
                    ErrorCodes.CLIENT_NOT_FOUND);
        }

        commandeClientDto.setClient(ClientDto.convertEntityToDto(clientOptional.get()));
        return CommandeClientDto.convertEntityToDto(
                commandeClientRepository.save(CommandeClientDto.convertDtoToEntity(commandeClientDto))
        );

    }

    @Override
    public CommandeClientDto updateArticleCommande(final Integer idCommande, final Integer idLigngeCommande, final Integer idArticle) {
        checkIdCommande(idCommande);
        checkIdLigneCommande(idLigngeCommande);
        checkIdArticle(idArticle, "nouvelle");
        CommandeClientDto commandeClientDto = checkEtatCommande(idCommande);

        Optional<LigneCommandeClient> ligneCommandeClientOptional = findLigneCommandeClient(idLigngeCommande);

        Optional<Article> articleOptional = articleRepository.findById(idArticle);
        if(articleOptional.isEmpty()){
            throw new EntityNotFoundException("Aucune article na été trouvée avec l'ID" + idArticle + " n'a étét trouvé dans BDD",
                    ErrorCodes.ARTICLE_NOT_FOUND);
        }

        List<String> errors = (List<String>) ArticleValidator.validate(ArticleDto.convertEntityToDto(articleOptional.get()));

        if(!errors.isEmpty()){
            throw new InvalidEntityException("Article invalid ", ErrorCodes.ARTICLE_NOT_VALID, errors);
        }

        LigneCommandeClient ligneCommandeClient = ligneCommandeClientOptional.get();
        ligneCommandeClient.setArticle(articleOptional.get());
        ligneCommandeClientRepository.save(ligneCommandeClient);

        return commandeClientDto;
    }

    @Override
    public CommandeClientDto deleteLigneCommande(final Integer idCommande, final Integer idLigngeCommande) {
        checkIdCommande(idCommande);
        checkIdLigneCommande(idLigngeCommande);

        CommandeClientDto commandeClientDto = checkEtatCommande(idCommande);

        // Just to check the LigneCommandeClient and inform the client in case it is absent
        findLigneCommandeClient(idLigngeCommande);

        ligneCommandeClientRepository.deleteById(idLigngeCommande);

        return commandeClientDto;
    }

    @Override
    public CommandeClientDto findById(Integer id) {
        if(id == null){
            log.error("Commande client ID is NULL");
            return null;
        }
        return commandeClientRepository.findById(id)
                .map(CommandeClientDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Aucune commande client n'a été trouvé avec l' ID = " + id ,
                                ErrorCodes.COMMANDE_CLIENT_NOT_FOUND)
                );
    }

    @Override
    public CommandeClientDto findCommandeClientByCode(String code) {
        if(!StringUtils.hasLength(code)){
            log.error("Commande client CODE is NULL");
            return null;
        }
        return commandeClientRepository.findCommandeClientByCode(code)
                .map(CommandeClientDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Aucune commande client n'a été trouvé avec le code = " + code ,
                                ErrorCodes.COMMANDE_CLIENT_NOT_FOUND)
                );
    }

    @Override
    public Collection<CommandeClientDto> findAll() {
        return commandeClientRepository.findAll()
                .stream()
                .map(CommandeClientDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<LigneCommandeClientDto> findAllByCommandeClientId(final Integer id) {
        return ligneCommandeClientRepository.findAllByCommandeClientId(id).stream()
                .map(LigneCommandeClientDto::convertEntityToDto)
                .collect(Collectors.toSet());
    }

    @Override
    public void delete(Integer id) {
        if(id == null){
            log.error("Commande client ID is NULL");
            return;
        }
        Collection<LigneCommandeClient> ligneCommandeClients = ligneCommandeClientRepository.findAllByCommandeClientId(id);
        if(!ligneCommandeClients.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer une commande client deja utilisé",
                    ErrorCodes.COMMANDE_CLIENT_ALREADY_IN_USE);
        }
        commandeClientRepository.deleteById(id);
    }

    private void checkIdCommande(final Integer idCommande){
        if(idCommande == null){
            log.error("Commande client ID is NULL");
            throw new InvalidOperationException("Impossible de modifier le statut de la commande avec un id null", ErrorCodes.COMMANDE_CLIENT_CANNOT_MODIFIED);
        }
    }

    private void checkIdLigneCommande(final Integer idLigneCommande) {
        if(idLigneCommande == null){
            log.error("L'ID de la ligne commande est NULL");
            throw new InvalidOperationException("Impossible de modifier le statut de la commande avec une ligne de commande null", ErrorCodes.COMMANDE_CLIENT_CANNOT_MODIFIED);
        }
    }

    private void checkIdArticle(final Integer idArticle, final String msg) {
        if(idArticle == null){
            log.error("L'ID de " + msg + " est NULL");
            throw new InvalidOperationException("Impossible de modifier la commande avec un " + msg + " ID article null", ErrorCodes.COMMANDE_CLIENT_CANNOT_MODIFIED);
        }
    }

    private CommandeClientDto checkEtatCommande(final Integer idCommande) {
        CommandeClientDto  commandeClientDto = findById(idCommande);
        if(commandeClientDto.getId() != null && commandeClientDto.isCommandeLivree()){
            throw new InvalidOperationException("Impossible de modifier la commande lorsqu'elle est livrée",
                    ErrorCodes.COMMANDE_CLIENT_CANNOT_MODIFIED);
        }
        return commandeClientDto;
    }

    private Optional<LigneCommandeClient> findLigneCommandeClient(final Integer idLigneCommande){
        Optional<LigneCommandeClient> ligneCommandeClientOptional = ligneCommandeClientRepository.findById(idLigneCommande);
        if (ligneCommandeClientOptional.isEmpty()){
            throw new EntityNotFoundException("Aucune ligne commande trouvée avec l'ID" + idLigneCommande + " n'a étét trouvé dans BDD",
                    ErrorCodes.LIGNE_COMMANDE_CLEINT_NOT_FOUND);
        }
        return ligneCommandeClientOptional;
    }

    private void updateMvtStk(final Integer idCommande){
        Collection<LigneCommandeClient> ligneCommandeClients = ligneCommandeClientRepository.findAllByCommandeClientId(idCommande);
        ligneCommandeClients.stream()
                .forEach(
                        ligneCommandeClient -> {
                            MvtStkDto mvtStkDto = MvtStkDto.builder()
                                    .article(ArticleDto.convertEntityToDto(ligneCommandeClient.getArticle()))
                                    .dateMvtStk(Instant.now())
                                    .typeMvtStk(TypeMvtStk.SORTIE)
                                    .sourceMvt(SourceMvtStk.COMMANDE_CLIENT)
                                    .quantite(ligneCommandeClient.getQuantite())
                                    .build();

                            mvtStkService.sortieStk(mvtStkDto);
                        }
                );

    }
}
