package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.TrackingConnexionDto;
import com.arnaud.geststock.repository.TrackingConnexionRepository;
import com.arnaud.geststock.services.TrackingConnexionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
public class TrackingConnexionServiceImpl implements TrackingConnexionService {

    private final TrackingConnexionRepository trackingConnexionRepository;

    @Autowired
    public TrackingConnexionServiceImpl(final TrackingConnexionRepository trackingConnexionRepository) {
        this.trackingConnexionRepository = trackingConnexionRepository;
    }

    @Override
    public void save(final TrackingConnexionDto dto) {
        trackingConnexionRepository.save(TrackingConnexionDto.convertDtoToEntity(dto));
    }
}
