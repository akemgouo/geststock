package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.PaysDto;
import com.arnaud.geststock.exception.EntityAlreadyExistException;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.repository.PaysRepository;
import com.arnaud.geststock.services.PaysService;
import com.arnaud.geststock.validator.PaysValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PaysServiceImpl implements PaysService {

    private final PaysRepository paysRepository;

    @Autowired
    public PaysServiceImpl(final PaysRepository paysRepository) {
        this.paysRepository = paysRepository;
    }

    @Override
    @Transactional
    public PaysDto save(final PaysDto dto) {
        List<String> errors = (List<String>) PaysValidator.validate(dto);
        if(!errors.isEmpty()){
            throw new InvalidEntityException("L'objet Pays n'est pas valide", ErrorCodes.PAYS_NOT_VALID, errors);
        }

        if(existsByCode(dto.getCode())){
            throw new EntityAlreadyExistException("Le Pays fourni avec le code = "+ dto.getCode()+ " existe", ErrorCodes.PAYS_ALREADY_EXISTS);
        }
        return PaysDto.convertEntityToDto(paysRepository.save(PaysDto.convertDtoToEntity(dto)));
    }

    @Override
    public PaysDto findById(Integer id) {
        if (id == null){
            log.error("L'object Id est null");
            return null;
        }
        return paysRepository.findById(id)
                .map(PaysDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException("Le pays fourni avec l'identifiant ID = " + id + " est inexistant", ErrorCodes.PAYS_NOT_FOUND)
                );
    }

    @Override
    public Boolean existsByCode(final String code) {
        return paysRepository.existsByCode(code).booleanValue();
    }

    @Override
    public PaysDto findPaysByCode(final String code) {

        if(!StringUtils.hasLength(code)){
            log.error("Code pays est null");
            return null;
        }
        return paysRepository.findPaysByCode(code)
                .map(PaysDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException("Le pays fourni avec l'identifiant Code = " + code + " est inexistant", ErrorCodes.PAYS_NOT_FOUND)
                );
    }

    @Override
    public Collection<PaysDto> findAll() {
        return paysRepository.findAll()
                .stream()
                .map(PaysDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(final Integer id) {

        if (id == null){
            log.error("L'object Id est null");
            return;
        }

        paysRepository.deleteById(id);

    }
}
