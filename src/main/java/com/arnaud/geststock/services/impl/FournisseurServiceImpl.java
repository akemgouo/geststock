package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.FournisseurDto;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.exception.InvalidOperationException;
import com.arnaud.geststock.model.CommandeFournisseur;
import com.arnaud.geststock.repository.CommandeFournisseurRepository;
import com.arnaud.geststock.repository.FournisseurRepository;
import com.arnaud.geststock.services.FournisseurService;
import com.arnaud.geststock.validator.FournisseurValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FournisseurServiceImpl implements FournisseurService {

    private final FournisseurRepository fournisseurRepository;
    private final CommandeFournisseurRepository commandeFournisseurRepository;

    @Autowired
    public FournisseurServiceImpl(final FournisseurRepository fournisseurRepository,
                                  final CommandeFournisseurRepository commandeFournisseurRepository){
        this.fournisseurRepository = fournisseurRepository;
        this.commandeFournisseurRepository = commandeFournisseurRepository;
    }

    @Override
    public FournisseurDto save(final FournisseurDto dto) {
        List<String> errors = (List<String>) FournisseurValidator.validate(dto);

        if(!errors.isEmpty()){
            log.error("Fournisseur is not valid {}", dto);
            throw new InvalidEntityException("Le fournisseur n'est pas valide", ErrorCodes.FOURNISSEUR_NOT_VALID, errors);
        }
        return FournisseurDto.convertEntityToDto(fournisseurRepository.save(FournisseurDto.convertDtoToEntity(dto)));
    }

    @Override
    public FournisseurDto findById(final Integer id) {
        if(id == null){
            log.error("Fournisseur ID is null");
            return null;
        }
        return fournisseurRepository.findById(id)
                .map(FournisseurDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Auncune Fournisseur avec l' ID = " + id + " n'a été trouvé dans la BDD",
                                ErrorCodes.FOURNISSEUR_NOT_FOUND)
                );
    }

    @Override
    public Collection<FournisseurDto> findAll() {
        return fournisseurRepository.findAll()
                .stream()
                .map(FournisseurDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public FournisseurDto findFournisseurByCodeFournisseur(final String code) {

        if(!StringUtils.hasLength(code)){
            log.error("Fournisseur code not found");
            //TODO Exception
            return null;
        }
        return fournisseurRepository.findFournisseurByCodeFournisseur(code)
                .map(FournisseurDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Auncune Fournisseur avec l' ID = " + code + " n'a été trouvé dans la BDD",
                                ErrorCodes.FOURNISSEUR_NOT_FOUND)
                );
    }

    @Override
    public void delete(final Integer id) {
        if(id == null){
            log.error("Fournisseur ID is null");
            return;
        }
        Collection<CommandeFournisseur> commandeFournisseurs = commandeFournisseurRepository.findAllByFournisseurId(id);
        if(!commandeFournisseurs.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer un fournisseur qui à déjà des commandes",
                    ErrorCodes.FOURNISSEUR_ALREADY_IN_USE);
        }
        fournisseurRepository.deleteById(id);
    }
}
