package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.ArticleDto;
import com.arnaud.geststock.dto.LigneCommandeClientDto;
import com.arnaud.geststock.dto.LigneCommandeFournisseurDto;
import com.arnaud.geststock.dto.LigneVenteDto;
import com.arnaud.geststock.exception.*;
import com.arnaud.geststock.model.*;
import com.arnaud.geststock.repository.*;
import com.arnaud.geststock.services.ArticleService;
import com.arnaud.geststock.validator.ArticleValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ArticleServiceImpl implements ArticleService {

    private final ArticleRepository articleRepository;
    private final LigneVenteRepository ligneVenteRepository;
    private final LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository;
    private final LigneCommandeClientRepository ligneCommandeClientRepository;
    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ArticleServiceImpl(final ArticleRepository articleRepository,
                              final LigneVenteRepository ligneVenteRepository,
                              final LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository,
                              final LigneCommandeClientRepository ligneCommandeClientRepository,
                              final ManufacturerRepository manufacturerRepository){

        this.articleRepository = articleRepository;
        this.ligneVenteRepository = ligneVenteRepository;
        this.ligneCommandeFournisseurRepository = ligneCommandeFournisseurRepository;
        this.ligneCommandeClientRepository = ligneCommandeClientRepository;
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public ArticleDto save(final ArticleDto dto) {

        List<String> errors = (List<String>) ArticleValidator.validate(dto);
        if(!errors.isEmpty()){
            log.error("Article is not valid {}", dto);
            throw new InvalidEntityException("L'article n'est pas valide", ErrorCodes.ARTICLE_NOT_VALID, errors);
        }
        if (existsByCodeArticle(dto.getCodeArticle())) {
            throw new EntityAlreadyExistException("L'article avec le code = " + dto.getCodeArticle() + " is already exist ",
                    ErrorCodes.ARTICLE_ALREADY_EXIST);
        }

        Optional<Manufacturer> manufacturerOptional = manufacturerRepository.findById(dto.getManufacturer().getId());
        if (manufacturerOptional.isEmpty()){
            log.warn("Le fabrican with ID {} was not found in the DB", dto.getManufacturer().getId());
            throw new EntityNotFoundException("Aucun fabrican avec l'ID" + dto.getManufacturer().getId() + " n'a étét trouvé dans BDD",
                    ErrorCodes.MANUFACTURER_NOT_FOUND);
        }
        Article articleSaved = articleRepository.save(
                ArticleDto.convertDtoToEntity(dto)
        );

        return ArticleDto.convertEntityToDto(
                articleSaved
        );
    }

    @Override
    public ArticleDto findById(final Integer id) {
        if (id == null){
            log.error("Article ID is not null");
            return null;
        }
        Optional<Article> article = articleRepository.findById(id);
        if (article.isEmpty()){
            throw new EntityNotFoundException("Aucun article avec l' ID = "+ id +"n'a ete trouver dans la BDD",
                    ErrorCodes.ARTICLE_NOT_FOUND);
        }
        ArticleDto dto = ArticleDto.convertEntityToDto(article.get());
        return Optional.of(dto).orElseThrow(() ->
                new EntityNotFoundException("Aucun article avec l'ID = "+ id +"n'a ete trouver dans la BDD",
                        ErrorCodes.ARTICLE_NOT_FOUND)
        );
    }

    @Override
    public ArticleDto findByCodeArticle(final String codeArticle) {

        if (!StringUtils.hasLength(codeArticle)){
            log.error("Article CODE is null");
            return null;
        }

        Optional<Article> article = articleRepository.findArticleByCodeArticle(codeArticle);
        if (article.isEmpty()){
            throw new EntityNotFoundException("Aucun article avec le code = "+ codeArticle +"n'a ete trouver dans la BDD",
                    ErrorCodes.ARTICLE_NOT_FOUND);
        }
        ArticleDto dto = ArticleDto.convertEntityToDto(article.get());
        return Optional.of(dto).orElseThrow(() ->
                new EntityNotFoundException("Aucun article avec le code = "+ codeArticle +"n'a ete trouver dans la BDD",
                        ErrorCodes.ARTICLE_NOT_FOUND)
        );
    }

    @Override
    public Collection<ArticleDto> findAll() {
        return articleRepository.findAll().stream()
                .map(ArticleDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<LigneVenteDto> findHistoriqueVentes(final Integer idArticle) {
        return ligneVenteRepository.findAllByArticleId(idArticle)
                .stream()
                .map(LigneVenteDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<LigneCommandeClientDto> findHistoriqueCommandeClient(final Integer idArticle) {
        return ligneCommandeClientRepository.findAllByArticleId(idArticle)
                .stream()
                .map(LigneCommandeClientDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<LigneCommandeFournisseurDto> findHistoriqueCommandeFournisseur(final Integer idArticle) {
        return ligneCommandeFournisseurRepository.findAllByArticleId(idArticle)
                .stream()
                .map(LigneCommandeFournisseurDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<ArticleDto> findAllArticleByIdCategory(final Integer idCategory) {
        return articleRepository.findAllByCategoryId(idCategory)
                .stream()
                .map(ArticleDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Boolean existsByCodeArticle(final String codeArticle) {
        return articleRepository.existsByCodeArticle(codeArticle);
    }

    @Override
    public void delete(final Integer id) {
        if (id == null){
            log.error("Article ID is not null");
            return;
        }

        Collection<LigneCommandeClient> ligneCommandeClients = ligneCommandeClientRepository.findAllByArticleId(id);
        if(!ligneCommandeClients.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimier un article déjà utiliser dans des commande client",
                    ErrorCodes.ARTICLE_ALREADY_IN_USE);
        }

        Collection<LigneCommandeFournisseur> ligneCommandeFournisseurs = ligneCommandeFournisseurRepository.findAllByArticleId(id);
        if(!ligneCommandeFournisseurs.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimier un article déjà utiliser dans des commande fournisseur",
                    ErrorCodes.ARTICLE_ALREADY_IN_USE);
        }

        Collection<LigneVente> ligneVentes = ligneVenteRepository.findAllByArticleId(id);
        if(!ligneVentes.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimier un article déjà utiliser dans une vente",
                    ErrorCodes.ARTICLE_ALREADY_IN_USE);
        }
        articleRepository.deleteById(id);
    }
}
