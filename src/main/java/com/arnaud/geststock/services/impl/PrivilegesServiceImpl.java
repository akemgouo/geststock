package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.PrivilegesDto;
import com.arnaud.geststock.exception.EntityAlreadyExistException;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.repository.PrivilegesRepository;
import com.arnaud.geststock.services.PrivilegesService;
import com.arnaud.geststock.validator.PrivilegesValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PrivilegesServiceImpl implements PrivilegesService {

    private final PrivilegesRepository privilegesRepository;

    @Autowired
    public PrivilegesServiceImpl(final PrivilegesRepository privilegesRepository) {
        this.privilegesRepository = privilegesRepository;
    }

    @Override
    public PrivilegesDto save(final PrivilegesDto dto) {
        List<String> errors = PrivilegesValidator.validate(dto);
        if(!errors.isEmpty()){
            throw new InvalidEntityException("L'objet privileges est invalid", ErrorCodes.PRIVILEGES_NOT_VALID, errors);
        }

        if(StringUtils.hasLength(findPrivilegesByNom(dto.getNom()).getNom())){
            throw new EntityAlreadyExistException("Le privelege fourni par le nom = "+ dto.getNom() + " existe en BD",
                    ErrorCodes.PRIVILEGES_ALREADY_EXISTS);
        }
        return PrivilegesDto.convertEntityToDto(
                privilegesRepository.save(
                        PrivilegesDto.convertDtoToEntity(dto)
                ));
    }

    @Override
    public PrivilegesDto findById(final Integer id) {
        if(id == null){
            log.error("L'idantifian fourni est null");
            return null ;
        }
        return privilegesRepository.findById(id)
                .map(PrivilegesDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException("Le privilege fourni avec l'ID = " + id + " n'existe pas en BD",
                                ErrorCodes.PRIVILEGES_NOT_FOUND
                        )
                );
    }

    @Override
    public PrivilegesDto findPrivilegesByNom(final String nom) {
        if(!StringUtils.hasLength(nom)){
            log.error("Le nom de l'objet est null");
            return null;
        }
        return privilegesRepository.findPrivilegesByNom(nom)
                .map(PrivilegesDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Le privilege fouri par le nom = "+ nom + " n'existe pas en BD",
                                ErrorCodes.ENTREPRISE_NOT_FOUND)
                );
    }

    @Override
    public Collection<PrivilegesDto> findAll() {
        return privilegesRepository.findAll()
                .stream()
                .map(PrivilegesDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(final Integer id) {
        if(id == null){
            log.error("L'idantifiant fourni est null");
            return;
        }
        privilegesRepository.deleteById(id);

    }
}
