package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.*;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.exception.InvalidOperationException;
import com.arnaud.geststock.model.*;
import com.arnaud.geststock.repository.*;
import com.arnaud.geststock.services.CommandeFournisseurService;
import com.arnaud.geststock.services.MvtStkService;
import com.arnaud.geststock.validator.ArticleValidator;
import com.arnaud.geststock.validator.CommandeFournisseurValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Service
@Slf4j
public class CommandeFournisseurServiceImpl implements CommandeFournisseurService {

    private final CommandeFournisseurRepository commandeFournisseurRepository;

    private final ArticleRepository articleRepository;

    private final LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository;

    private final FournisseurRepository fournisseurRepository;

    private final MvtStkService mvtStkService;

    @Autowired
    public CommandeFournisseurServiceImpl(final CommandeFournisseurRepository commandeFournisseurRepository,
                                          final ArticleRepository articleRepository,
                                          final LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository,
                                          final FournisseurRepository fournisseurRepository,
                                          final MvtStkService mvtStkService){
        this.articleRepository = articleRepository;
        this.commandeFournisseurRepository = commandeFournisseurRepository;
        this.fournisseurRepository = fournisseurRepository;
        this.ligneCommandeFournisseurRepository = ligneCommandeFournisseurRepository;
        this.mvtStkService = mvtStkService;
    }

    @Override
    public CommandeFournisseurDto save(final CommandeFournisseurDto dto) {

        List<String> errors = (List<String>) CommandeFournisseurValidator.validate(dto);
        if(!errors.isEmpty()){
            log.error("La comande fourniseeur est invalid");
            throw new InvalidEntityException("La comande fourniseeur est invalid.", ErrorCodes.COMMANDE_FOURNISSEUR_NOT_VALID, errors);
        }

        if(dto.getId() != null && dto.isCommandeLivree()){
            throw new InvalidOperationException("Impossible de modifier la commande au statut livré",
                    ErrorCodes.COMMANDE_FOURNISSEUR_CANNOT_MODIFIED);
        }

        Optional<Fournisseur> fournisseur = fournisseurRepository.findById(dto.getFournisseur().getId());
        if(!fournisseur.isPresent()){
            log.warn("Fournisseur not found");
            throw new EntityNotFoundException("Fournisseur de ID = " + dto.getFournisseur().getId() + " not found",ErrorCodes.FOURNISSEUR_NOT_FOUND);
        }

        Collection<String> articleErrors = new ArrayList<>();
        if(dto.getLigneCommandeFournisseurs() != null){
            dto.getLigneCommandeFournisseurs().forEach(
                    lgnCmdFournisseur ->{
                        if(lgnCmdFournisseur.getArticle() != null){
                            Optional<Article> article = articleRepository.findById(lgnCmdFournisseur.getArticle().getId());
                            if(article.isEmpty()){
                                articleErrors.add("L'article de ID = "+ lgnCmdFournisseur.getArticle().getId() + " n'existe pas en BDD");
                            }
                        } else {
                            articleErrors.add("Impossible d'enregister une commande avec un article null");
                        }

            });
        }

        if(articleErrors.isEmpty()){
            log.warn("lArticle n'existe pas dans la BDD");
            throw new EntityNotFoundException("Article n'existe pas dans la BDD", ErrorCodes.ARTICLE_NOT_FOUND);
        }

        CommandeFournisseur savedcmdFournisseur = commandeFournisseurRepository.save(CommandeFournisseurDto.convertDtoToEntity(dto));

        if(dto.getLigneCommandeFournisseurs() != null){
            dto.getLigneCommandeFournisseurs().forEach(
                    lngCmdFournisseur ->{
                        LigneCommandeFournisseur ligneCommandeFournisseur = LigneCommandeFournisseurDto.convertDtoToEntity(lngCmdFournisseur);
                        ligneCommandeFournisseur.setCommandeFournisseur(savedcmdFournisseur);
                        ligneCommandeFournisseurRepository.save(ligneCommandeFournisseur);
                    });
        }

        return CommandeFournisseurDto.convertEntityToDto(savedcmdFournisseur);
    }

    @Override
    public CommandeFournisseurDto updateStatusCommandeForunisseur(final Integer idCommande, final CommandeSts commandeSts) {
        checkIdCommand(idCommande);
        if(!StringUtils.hasLength(commandeSts.name())){
            log.warn("Le statut de la commande ne peut étre null");
            throw new InvalidOperationException("Impossible de modifier le statut de la commande avec un statut null",
                   ErrorCodes.COMMANDE_FOURNISSEUR_CANNOT_MODIFIED );
        }

        CommandeFournisseurDto commandeFournisseurDto = checkEtatCommande(idCommande);
        commandeFournisseurDto.setStatus(commandeSts);

        CommandeFournisseur savedCmdFournisseur = commandeFournisseurRepository.save(CommandeFournisseurDto.convertDtoToEntity(commandeFournisseurDto));
        if (commandeFournisseurDto.isCommandeLivree()) {
            updateMvtStk(idCommande);
        }

        return CommandeFournisseurDto.convertEntityToDto(savedCmdFournisseur);
    }
    @Override
    public CommandeFournisseurDto findById(final Integer id) {
        if(id == null){
            log.error("Id not valid");
            return null;
        }
        return commandeFournisseurRepository.findById(id)
                .map(CommandeFournisseurDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new EntityNotFoundException(
                                "Aucune commande fournisseur n'a été trouvé avec l'ID = " + id, ErrorCodes.COMMANDE_FOURNISSEUR_NOT_FOUND
                        )
                );

    }

    @Override
    public CommandeFournisseurDto findCommandeFournisseurByCode(final String code) {
        if(!StringUtils.hasLength(code)){
            log.error("Command fornissur not found with Code = " + code );
            return null ;
        }
        return commandeFournisseurRepository.findCommandeFournisseurByCode(code)
                .map(CommandeFournisseurDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Aucune commade fournisseur n'a été trouvé avec le Code = " + code,
                                ErrorCodes.COMMANDE_FOURNISSEUR_NOT_FOUND)
                );
    }

    @Override
    public Collection<CommandeFournisseurDto> findAll() {
        return commandeFournisseurRepository.findAll()
                .stream()
                .map(CommandeFournisseurDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public CommandeFournisseurDto updateQuantiteCommandeFournisseur(final Integer idCommande, final Integer idLigneCmdFournissseur, final BigDecimal quantite) {
        checkIdCommand(idCommande);
        checkIdLigneCommandeFournisseur(idLigneCmdFournissseur);

        if(quantite == null && quantite.compareTo(BigDecimal.ZERO) == 0){
            log.warn("La quantité ne peut etre negative ou zero");
            throw new InvalidOperationException("Impossible de modifeir l'état de la commande avec une quantité négavite ou égale à zero",
                    ErrorCodes.COMMANDE_FOURNISSEUR_CANNOT_MODIFIED);
        }

        CommandeFournisseurDto commandeFournisseurDto = checkEtatCommande(idCommande);
        Optional<LigneCommandeFournisseur> ligneCommandeFournisseurOptional = findLigneCommandeFourninsseur(idLigneCmdFournissseur);
        LigneCommandeFournisseur ligneCommandeFournisseur  = ligneCommandeFournisseurOptional.get();
        ligneCommandeFournisseur.setQuantite(quantite);
        ligneCommandeFournisseurRepository.save(ligneCommandeFournisseur);

        return commandeFournisseurDto;
    }

    @Override
    public CommandeFournisseurDto updateFournisseurCommande(final Integer idCommande, final Integer idFournisseur) {
        checkIdCommand(idCommande);
        if(idFournisseur == null){
            log.warn("L'ID du fournisseur est null");
            throw new InvalidOperationException("Impossible de modifier la commande avec un Id fournisseur null",
                    ErrorCodes.COMMANDE_FOURNISSEUR_CANNOT_MODIFIED);
        }

        CommandeFournisseurDto commandeFournisseurDto = checkEtatCommande(idCommande);
        Optional<Fournisseur> fournisseurOptional = fournisseurRepository.findById(idFournisseur);
        if(fournisseurOptional.isEmpty()){
            log.warn("Le fournisseur fourni avec l'ID = " + idFournisseur + " n'existe plus en BD");
            throw new EntityNotFoundException("Le fournisseur fourni avec l'ID = " + idFournisseur + " n'existe plus en BD",
                    ErrorCodes.FOURNISSEUR_NOT_FOUND);
        }

        commandeFournisseurDto.setFournisseur(FournisseurDto.convertEntityToDto(fournisseurOptional.get()));

        return CommandeFournisseurDto.convertEntityToDto(
                commandeFournisseurRepository.save(
                        CommandeFournisseurDto.convertDtoToEntity(commandeFournisseurDto)
                ));
    }

    @Override
    public CommandeFournisseurDto updateArticleCommandeFournisseur(final Integer idCommande,final Integer idLigneCmdFournisseur,final Integer idArticle) {
        checkIdCommand(idCommande);
        checkIdLigneCommandeFournisseur(idLigneCmdFournisseur);
        checkIdArticle(idArticle);

        CommandeFournisseurDto commandeFournisseurDto = checkEtatCommande(idCommande);

        Optional<LigneCommandeFournisseur> ligneCommandeFournisseurOptional = findLigneCommandeFourninsseur(idLigneCmdFournisseur);
        Optional<Article> articleOptional = articleRepository.findById(idArticle);

        if(articleOptional.isEmpty()){
            log.warn("Article set is empty");
            throw new EntityNotFoundException("L'article fourni avec ID = " + idArticle + " n'existe pas dans la bd ",
                    ErrorCodes.ARTICLE_NOT_FOUND);
        }

        List<String> errors = (List<String>) ArticleValidator.validate(ArticleDto.convertEntityToDto(articleOptional.get()));

        if(!errors.isEmpty()){
            log.warn("L'article est invalid");
            throw new InvalidEntityException("L'article fourni est invalid",
                    ErrorCodes.ARTICLE_NOT_VALID, errors);
        }

        LigneCommandeFournisseur ligneCommandeFournisseur = ligneCommandeFournisseurOptional.get();
        ligneCommandeFournisseur.setArticle(articleOptional.get());
        ligneCommandeFournisseurRepository.save(ligneCommandeFournisseur);

        return commandeFournisseurDto;
    }

    @Override
    public Collection<LigneCommandeFournisseurDto> findAllByCommandeFournisseurId(final Integer id) {

        return ligneCommandeFournisseurRepository.findAllByCommandeFournisseurId(id)
                .stream()
                .map(LigneCommandeFournisseurDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public CommandeFournisseurDto deleteLigneCommandeFournisseur(final Integer idCommande, final Integer idLingeCommande) {
        checkIdCommand(idLingeCommande);
        checkIdLigneCommandeFournisseur(idLingeCommande);
        CommandeFournisseurDto commandeFournisseurDto = checkEtatCommande(idCommande);
        findLigneCommandeFourninsseur(idLingeCommande);
        ligneCommandeFournisseurRepository.deleteById(idLingeCommande);
        return commandeFournisseurDto;
    }

    @Override
    public void delete(Integer id) {
        if(id == null){
            log.error("Command fornissur ID is null");
            return ;
        }
        Collection<LigneCommandeFournisseur> ligneCommandeFournisseurs = ligneCommandeFournisseurRepository.findAllByCommandeFournisseurId(id);
        if(!ligneCommandeFournisseurs.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer une commande fournisseur deja utilisé",
                    ErrorCodes.COMMANDE_FOURNISSEUR_ALREADY_IN_USE);
        }
        commandeFournisseurRepository.deleteById(id);
    }

    private void checkIdCommand(final Integer idCommande){
        if (idCommande == null){
            log.warn("Commande fournisseur ID is null");
            throw new InvalidOperationException("Impossible de modifier la commane avec un Id null",
                    ErrorCodes.COMMANDE_FOURNISSEUR_CANNOT_MODIFIED);
        }
    }

    private CommandeFournisseurDto checkEtatCommande(final Integer idCommande){
        CommandeFournisseurDto commandeFournisseurDto = findById(idCommande);
        if(commandeFournisseurDto.getId() != null && commandeFournisseurDto.isCommandeLivree()){
            throw new InvalidOperationException("Impossible de modifier la commande lorsqu'elle est livrée",
                    ErrorCodes.COMMANDE_FOURNISSEUR_CANNOT_MODIFIED);
        }
        return commandeFournisseurDto;
    }

    private void checkIdLigneCommandeFournisseur(final Integer idLigneCmdFournisseur){
        if(idLigneCmdFournisseur == null){
            log.error("L'ID de la ligne commande est NULL");
            throw new InvalidOperationException("Impossible de modifier le statut de la commande avec une ligne de commande null", ErrorCodes.COMMANDE_CLIENT_CANNOT_MODIFIED);
        }
    }

    private void checkIdArticle(final Integer idArticle){
        if(idArticle == null){
            log.warn("I' ID de l'article est null");
            throw new InvalidOperationException("Impossible de modifier la commande avec un ID is null",
                    ErrorCodes.COMMANDE_FOURNISSEUR_CANNOT_MODIFIED);
        }
    }

    private Optional<LigneCommandeFournisseur> findLigneCommandeFourninsseur(final Integer idLigneCmd){
        Optional<LigneCommandeFournisseur> ligneCommandeFournisseurOptional = ligneCommandeFournisseurRepository.findById(idLigneCmd);

        if(ligneCommandeFournisseurOptional.isEmpty()){
            log.warn("I' ID de l'article est null");
            throw new EntityNotFoundException("Aucune ligne commande trouver avec l'identifiant fourni ID = "+ idLigneCmd ,
                    ErrorCodes.LIGNE_COMMANDE_FOURNISSEUR_NOT_FOUND);
        }

        return ligneCommandeFournisseurOptional;

    }

    private void updateMvtStk(final Integer idCommande){

        Collection<LigneCommandeFournisseur> ligneCommandeFournisseurs = ligneCommandeFournisseurRepository.findAllByCommandeFournisseurId(idCommande);
        ligneCommandeFournisseurs.stream()
                .forEach(ligneCmdFournisseur -> {
                    MvtStkDto mvtStkDto = MvtStkDto.builder()
                            .dateMvtStk(Instant.now())
                            .typeMvtStk(TypeMvtStk.ENTREE)
                            .sourceMvt(SourceMvtStk.COMMAND_FOURNISSEUR)
                            .article(ArticleDto.convertEntityToDto(ligneCmdFournisseur.getArticle()))
                            .quantite(ligneCmdFournisseur.getQuantite())
                            .build();

                    mvtStkService.entreeStk(mvtStkDto);
                });

    }
}
