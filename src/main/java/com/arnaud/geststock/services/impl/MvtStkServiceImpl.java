package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.MvtStkDto;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.model.TypeMvtStk;
import com.arnaud.geststock.repository.ArticleRepository;
import com.arnaud.geststock.repository.MvtStkRepository;
import com.arnaud.geststock.services.MvtStkService;
import com.arnaud.geststock.validator.MvtStkValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
@Slf4j
public class MvtStkServiceImpl implements MvtStkService {

    private final MvtStkRepository mvtStkRepository;
    private final ArticleRepository articleRepository;

    @Autowired
    public MvtStkServiceImpl(final MvtStkRepository mvtStkRepository, final ArticleRepository articleRepository) {
        this.mvtStkRepository = mvtStkRepository;
        this.articleRepository = articleRepository;
    }

    @Override
    public BigDecimal stockReelArticle(final Integer idArticle) {
        if(idArticle == null){
            log.warn("ID article is Null");
            return BigDecimal.valueOf(-1);
        }
        articleRepository.findById(idArticle);
        return mvtStkRepository.stockReelArticle(idArticle);
    }

    @Override
    public Collection<MvtStkDto> mvtStkArticle(Integer idArticle) {
        if(idArticle == null){
            log.warn("ID article is Null");
            return null;
        }
        return mvtStkRepository.findAllByArticleId(idArticle)
                .stream()
                .map(MvtStkDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public MvtStkDto entreeStk(MvtStkDto dto) {

        return mvtStockPositive(dto, TypeMvtStk.ENTREE);
    }

    @Override
    public MvtStkDto sortieStk(MvtStkDto dto) {
        return mvtStockNegative(dto, TypeMvtStk.SORTIE);
    }

    @Override
    public MvtStkDto transfertStk(MvtStkDto dto) {
        return null;
    }

    @Override
    public MvtStkDto correctionStkPos(MvtStkDto dto) {
        return mvtStockPositive(dto,TypeMvtStk.CORRECTION_POS);
    }

    @Override
    public MvtStkDto correctionStkNeg(MvtStkDto dto) {
        return mvtStockPositive(dto,TypeMvtStk.CORRECTION_NEG);
    }

    private MvtStkDto mvtStockPositive(MvtStkDto dto, TypeMvtStk typeMvtStk){
        List<String> errors = (List<String>) MvtStkValidator.validate(dto);
        if(!errors.isEmpty()){
            log.error("Mouvement de stock is not valid {}", dto);
            throw new InvalidEntityException("Le mouvement de stock n'est pas valide", ErrorCodes.MVTSTK_NOT_VALID, errors);
        }

        dto.setQuantite(
                BigDecimal.valueOf(
                        Math.abs(dto.getQuantite().doubleValue())
                )
        );
        dto.setTypeMvtStk(typeMvtStk);
        return MvtStkDto.convertEntityToDto(mvtStkRepository.save(MvtStkDto.convertDtoToEntity(dto)));
    }

    private MvtStkDto mvtStockNegative(MvtStkDto dto, TypeMvtStk typeMvtStk){
        List<String> errors = (List<String>) MvtStkValidator.validate(dto);
        if(!errors.isEmpty()){
            log.error("Mouvement de stock is not valid {}", dto);
            throw new InvalidEntityException("Le mouvement de stock n'est pas valide", ErrorCodes.MVTSTK_NOT_VALID, errors);
        }

        dto.setQuantite(
                BigDecimal.valueOf(
                        Math.abs(dto.getQuantite().doubleValue())*-1
                )
        );
        dto.setTypeMvtStk(typeMvtStk);
        return MvtStkDto.convertEntityToDto(mvtStkRepository.save(MvtStkDto.convertDtoToEntity(dto)));
    }
}
