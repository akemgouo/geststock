package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.VilleDto;
import com.arnaud.geststock.exception.EntityAlreadyExistException;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.repository.VilleRepository;
import com.arnaud.geststock.services.VilleService;
import com.arnaud.geststock.validator.VilleValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class VilleServiceImpl implements VilleService {

    private final VilleRepository villeRepository;

    @Autowired
    public VilleServiceImpl(final VilleRepository villeRepository) {
        this.villeRepository = villeRepository;
    }

    @Override
    public VilleDto save(final VilleDto dto) {

        List<String> errors = (List<String>) VilleValidator.validate(dto);
        if (!errors.isEmpty()){
            throw new InvalidEntityException("L'objet ville n'est pas valide", ErrorCodes.VILLE_NOT_VALID, errors);
        }

        if (existsByCode(dto.getCode())){
            throw  new EntityAlreadyExistException("La ville de code = " + dto.getCode() + " exite deja", ErrorCodes.VENTES_ALREADY_EXIST);
        }
        return VilleDto.convertEntityToDto(villeRepository.save(VilleDto.convertDtoToEntity(dto)));
    }

    @Override
    public VilleDto findById(final Integer id) {
        if (id == null){
            log.warn("Ville ID is null");
            return null;
        }
        return villeRepository.findById(id)
                .map(VilleDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("La ville avec l' ID = " + id + " fourni n'existe pas", ErrorCodes.VILLE_NOT_FOUND)
                );
    }

    @Override
    public VilleDto findVilleByCode(final String code) {
        if (!StringUtils.hasLength(code)){
            log.warn("Ville code is null");
            return null;
        }

        return villeRepository.findVilleByCode(code)
                .map(VilleDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("La ville avec le code = " + code + " fourni n'existe pas", ErrorCodes.VILLE_NOT_FOUND)
                );
    }

    @Override
    public Boolean existsByCode(final String code) {
        return villeRepository.existsByCode(code).booleanValue();
    }

    @Override
    public Collection<VilleDto> findAll() {
        return villeRepository.findAll()
                .stream()
                .map(VilleDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(final Integer id) {
        if (id == null){
            log.warn("Ville ID is null");
            return ;
        }
        villeRepository.deleteById(id);
    }
}
