package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.CategoryDto;
import com.arnaud.geststock.exception.*;
import com.arnaud.geststock.model.Article;
import com.arnaud.geststock.repository.ArticleRepository;
import com.arnaud.geststock.repository.CategoryRepository;
import com.arnaud.geststock.services.CategoryService;
import com.arnaud.geststock.validator.CategoryValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final ArticleRepository articleRepository;

    @Autowired
    public CategoryServiceImpl(final CategoryRepository categoryRepository, final ArticleRepository articleRepository){
        this.categoryRepository = categoryRepository;
        this.articleRepository = articleRepository;
    }

    @Override
    public CategoryDto save(final CategoryDto dto) {
        List<String> errors = (List<String>) CategoryValidator.validate(dto);
        if(!errors.isEmpty()){
            log.error("Category is not valid {}", dto);
            throw new InvalidEntityException("La category n'est pas valide", ErrorCodes.CATEGORY_NOT_VALID, errors);
        }
        if (this.existsByCode(dto.getCode())) {
            throw new EntityAlreadyExistException("Category avec le code = " + dto.getCode() + " is already exist ",
                    ErrorCodes.CATEGORY_ALREADY_EXIST);
        }
        return CategoryDto.convertEntityToDto(
                categoryRepository.save(
                        CategoryDto.convertDtoToEntity(dto)
                )
        );
    }

    @Override
    public Optional<CategoryDto> findById(final Integer id) {
        if(id == null){
            log.error("Category ID is null");
            return null;
        }
        return Optional.of(categoryRepository.findById(id)
                .map(CategoryDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Aucune category de l'ID = " + id + " n'est trouvé dans la BDD",
                                ErrorCodes.CATEGORY_NOT_FOUND
                        )));
    }

    @Override
    public CategoryDto findCategoryByCode(final String codeCategory) {
        if(!StringUtils.hasLength(codeCategory)){
            log.error("Category Code is null");
            return null;
        }
        return categoryRepository.findCategoryByCode(codeCategory)
                .map(CategoryDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Aucune category avec le Code = " + codeCategory + " n'est trouvé dans la BDD",
                                ErrorCodes.CATEGORY_NOT_FOUND
                        ));
    }

    @Override
    public Collection<CategoryDto> findAll() {
        return categoryRepository.findAll()
                .stream()
                .map(CategoryDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Boolean existsByCode(final String code) {
        return categoryRepository.existsByCode(code);
    }

    @Override
    public void delete(final Integer id) {
        if(id == null){
            log.error("Category ID is null");
            return;
        }
        Collection<Article> articles = articleRepository.findAllByCategoryId(id);
        if(!articles.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer cet category déjà utiliser",
                    ErrorCodes.CATEGORY_ALREADY_IN_USE);
        }
        categoryRepository.deleteById(id);
    }
}
