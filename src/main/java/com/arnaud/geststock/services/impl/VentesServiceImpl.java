package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.ArticleDto;
import com.arnaud.geststock.dto.LigneVenteDto;
import com.arnaud.geststock.dto.MvtStkDto;
import com.arnaud.geststock.dto.VentesDto;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.exception.InvalidOperationException;
import com.arnaud.geststock.model.*;
import com.arnaud.geststock.repository.ArticleRepository;
import com.arnaud.geststock.repository.LigneVenteRepository;
import com.arnaud.geststock.repository.VentesRepository;
import com.arnaud.geststock.services.MvtStkService;
import com.arnaud.geststock.services.VentesService;
import com.arnaud.geststock.validator.VentesValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class VentesServiceImpl implements VentesService
{
    private final VentesRepository ventesRepository;
    private final ArticleRepository articleRepository;
    private final LigneVenteRepository ligneVenteRepository;
    private final MvtStkService mvtStkService;

    @Autowired
    public VentesServiceImpl(final VentesRepository ventesRepository,
                             final ArticleRepository articleRepository,
                             final LigneVenteRepository ligneVenteRepository,
                             final MvtStkService mvtStkService) {
        this.ventesRepository = ventesRepository;
        this.articleRepository = articleRepository;
        this.ligneVenteRepository = ligneVenteRepository;
        this.mvtStkService = mvtStkService;
    }


    @Override
    public VentesDto save(VentesDto dto) {
        List<String> errors = (List<String>) VentesValidator.validate(dto);
        if(!errors.isEmpty()){
            log.error("Vente n'est pas valide");
            throw new InvalidEntityException("L'objet Vente n'est pas valide", ErrorCodes.VENTES_NOT_VALID, errors);
        }

        List<String> errorsArticle = new ArrayList<>();
        dto.getLigneVentes().forEach(ligneVenteDto -> {
            Optional<Article> article = articleRepository.findById(ligneVenteDto.getArticle().getId());

            if(!article.isPresent()){
                errorsArticle.add("Aucun article avec l' ID = " + ligneVenteDto.getArticle().getId() + " n'a été trouvé dans la BD");
            }
        });

        if(!errorsArticle.isEmpty()){
            log.error("One or more aritcle were not found in the DB , {}", errorsArticle);
            throw new InvalidEntityException("One or more aritcle were not found in the DB", ErrorCodes.ARTICLE_NOT_VALID, errorsArticle);
        }

        Ventes savedVentes = ventesRepository.save(VentesDto.convertDtoToEntity(dto));

        if( dto.getLigneVentes() != null){
            dto.getLigneVentes().forEach( ligneVenteDto -> {
                LigneVente ligneVente = LigneVenteDto.convertDtoToEntity(ligneVenteDto);
                ligneVente.setVente(savedVentes);
                ligneVenteRepository.save(ligneVente);
                updateMvtStk(ligneVente);
            });
        }

        return VentesDto.convertEntityToDto(savedVentes);
    }

    @Override
    public VentesDto findById(final Integer id) {
        if(id == null){
            log.warn("Ventes ID is null");
            return null;
        }
        return ventesRepository.findById(id)
                .map(VentesDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Aucune vente n'a été trouvé avec le ID = " + id,
                                ErrorCodes.VENTES_NOT_FOUND)
                );
    }

    @Override
    public VentesDto findVentesByCode(final String code) {
        if(!StringUtils.hasLength(code)){
            log.warn("Ventes Code is null");
            return null;
        }
        return ventesRepository.findVentesByCode(code)
                .map(VentesDto::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Aucune vente n'a été trouvé avec le Code = " + code,
                ErrorCodes.VENTES_NOT_FOUND)
                );
    }

    @Override
    public Collection<VentesDto> findAll() {

        return ventesRepository.findAll()
                .stream()
                .map(VentesDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(final Integer id) {
        if(id == null){
            log.error("Command fornissur ID is null");
            return ;
        }

        Collection<LigneVente> ligneVentes = ligneVenteRepository.findAllByVenteId(id);
        if(!ligneVentes.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer une vente qui à déjà des lignes de ventes",
                    ErrorCodes.VENTES_ALREADY_IN_USE);
        }

        ventesRepository.deleteById(id);
    }

    private void updateMvtStk(LigneVente ligneVente){
        MvtStkDto mvtStkDto = MvtStkDto.builder()
                .article(ArticleDto.convertEntityToDto(ligneVente.getArticle()))
                .dateMvtStk(Instant.now())
                .typeMvtStk(TypeMvtStk.SORTIE)
                .sourceMvt(SourceMvtStk.VENTE)
                .quantite(ligneVente.getQuantite())
                .build();
        mvtStkService.sortieStk(mvtStkDto);
    }
}
