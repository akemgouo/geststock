package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.ManufacturerDto;
import com.arnaud.geststock.exception.*;
import com.arnaud.geststock.model.Article;
import com.arnaud.geststock.model.Manufacturer;
import com.arnaud.geststock.repository.ArticleRepository;
import com.arnaud.geststock.repository.ManufacturerRepository;
import com.arnaud.geststock.services.ManufacturerService;
import com.arnaud.geststock.validator.ManufacturerValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.arnaud.geststock.exception.ErrorCodes.MANUFACTURER_ALREADY_EXIST;
import static com.arnaud.geststock.exception.ErrorCodes.MANUFACTURER_NOT_VALID;

@Service
@Slf4j
public class ManufacturerServiceImpl implements ManufacturerService {

    private final ManufacturerRepository manufacturerRepository;
    private final ArticleRepository articleRepository;

    @Autowired
    public ManufacturerServiceImpl(final ManufacturerRepository manufacturerRepository, final ArticleRepository articleRepository) {
        this.manufacturerRepository = manufacturerRepository;
        this.articleRepository = articleRepository;
    }

    @Override
    public ManufacturerDto save(final ManufacturerDto dto) {

        List<String> errors = ManufacturerValidator.validate(dto);
        if (errors.isEmpty()){
            log.warn("Manufacturer is not valid {}", dto);
            throw new InvalidEntityException("Le fabrican n'est pas valide ", MANUFACTURER_NOT_VALID, errors);
        }

        if(existsManufacturerByCodeManufacturer(dto.getCodeManufactorer())){
            log.warn("Manufacturer existe");
            throw new EntityAlreadyExistException("Le fabrican fourni avec le code = " + dto.getCodeManufactorer() + " existe ", MANUFACTURER_ALREADY_EXIST);
        }
        return ManufacturerDto.convertEntityToDto(
                manufacturerRepository.save(ManufacturerDto.convertDtoToEntity(dto))
        );
    }

    @Override
    public ManufacturerDto findManufacturerByCodeManufacturer(final String code) {
        if(!StringUtils.hasLength(code)){
            log.warn("Manufacturer code is null");
            return null;
        }

        Optional<Manufacturer> manufacturerOptional = manufacturerRepository.findManufacturerByCodeManufacturer(code);
        if(manufacturerOptional.isEmpty()){
            throw new EntityNotFoundException("Aucun fabrican avec le code = "+ code +"n'a ete trouver dans la BDD",
                    ErrorCodes.MANUFACTURER_NOT_FOUND);
        }

        //ManufacturerDto manufacturerDto = ManufacturerDto.convertEntityToDto(manufacturerOptional.get());

        /*return Optional.of(manufacturerDto).orElseThrow(
                ()-> new EntityNotFoundException("Aucun fabrican avec le code = "+ code +"n'a ete trouver dans la BDD",
                ErrorCodes.MANUFACTURER_NOT_FOUND)
        );*/

        return manufacturerRepository.findManufacturerByCodeManufacturer(code)
                .map(ManufacturerDto::convertEntityToDto)
                .orElseThrow(
                ()-> new EntityNotFoundException("Aucun fabrican avec le code = "+ code +"n'a ete trouver dans la BDD",
                        ErrorCodes.MANUFACTURER_NOT_FOUND)
        );

    }

    @Override
    public ManufacturerDto findManufacturerById(final Integer id) {

        if(id == null){
            log.warn("Manufacturer ID is null");
            return null;
        }

        return manufacturerRepository.findById(id)
                .map(ManufacturerDto::convertEntityToDto)
                .orElseThrow(
                        ()-> new  EntityNotFoundException("Aucun fabrican avec le ID = "+ id +"n'a ete trouver dans la BDD",
                ErrorCodes.MANUFACTURER_NOT_FOUND)
                );
    }

    @Override
    public Collection<ManufacturerDto> findAll() {
        return manufacturerRepository.findAll()
                .stream()
                .map(ManufacturerDto::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(final Integer id) {
        if(id == null){
            log.warn("Manufacturer ID is null");
            return ;
        }
        Collection<Article> articles = articleRepository.findAllByManufacturerId(id);
        if(!articles.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer un fabrican qui à déjà des articles",
                    ErrorCodes.MANUFACTURER_ALREADY_IN_USE);
        }
        manufacturerRepository.deleteById(id);
    }

    private Boolean existsManufacturerByCodeManufacturer(final String codeArticle) {
        return manufacturerRepository.existsManufacturerByCodeManufacturer(codeArticle);
    }
}
