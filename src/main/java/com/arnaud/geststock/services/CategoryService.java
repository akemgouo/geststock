package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.CategoryDto;

import java.util.Collection;
import java.util.Optional;

public interface CategoryService {

    CategoryDto save(final CategoryDto dto);

    Optional<CategoryDto> findById(final Integer id);

    CategoryDto findCategoryByCode(final String codeCategory);

    Collection<CategoryDto> findAll();

    Boolean existsByCode(final String code);

    void delete (final Integer id);

}
