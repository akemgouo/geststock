package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.CommandeFournisseurDto;
import com.arnaud.geststock.dto.LigneCommandeFournisseurDto;
import com.arnaud.geststock.model.CommandeSts;

import java.math.BigDecimal;
import java.util.Collection;

public interface CommandeFournisseurService {

    CommandeFournisseurDto save(final CommandeFournisseurDto dto);

    CommandeFournisseurDto updateStatusCommandeForunisseur(final Integer idCommande, final CommandeSts commandeSts);

    CommandeFournisseurDto updateQuantiteCommandeFournisseur(final Integer idCommande, final Integer idLigneCmdFournissseur, final BigDecimal quantite);

    CommandeFournisseurDto updateFournisseurCommande(final Integer idCommande, final Integer idFournisseur);

    CommandeFournisseurDto updateArticleCommandeFournisseur(final Integer idCommande, final  Integer idLigneCmdFournisseur, final Integer idArticle);

    Collection<LigneCommandeFournisseurDto> findAllByCommandeFournisseurId (final Integer id);

    CommandeFournisseurDto deleteLigneCommandeFournisseur(final Integer icCommande, final Integer idLingeCommande);

    CommandeFournisseurDto findById(final Integer id);

    CommandeFournisseurDto findCommandeFournisseurByCode(final String code);

    Collection<CommandeFournisseurDto> findAll();

    void delete(final Integer id);
}
