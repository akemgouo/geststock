package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.ArticleDto;
import com.arnaud.geststock.dto.LigneCommandeClientDto;
import com.arnaud.geststock.dto.LigneCommandeFournisseurDto;
import com.arnaud.geststock.dto.LigneVenteDto;

import java.util.Collection;

public interface ArticleService {

    ArticleDto save(final ArticleDto dto);

    ArticleDto findById(final Integer id);

    ArticleDto findByCodeArticle(final String codeArticle);

    Collection<ArticleDto> findAll();

    Collection<LigneVenteDto> findHistoriqueVentes(final Integer idArticle);

    Collection<LigneCommandeClientDto> findHistoriqueCommandeClient(final Integer idArticle);

    Collection<LigneCommandeFournisseurDto> findHistoriqueCommandeFournisseur(final Integer idArticle);

    Collection<ArticleDto> findAllArticleByIdCategory(final Integer idCategory);

    Boolean existsByCodeArticle(final String codeArticle);

    void delete(final Integer id);
}
