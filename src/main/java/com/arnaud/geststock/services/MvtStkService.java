package com.arnaud.geststock.services;

import com.arnaud.geststock.dto.MvtStkDto;

import java.math.BigDecimal;
import java.util.Collection;

public interface MvtStkService {

    BigDecimal stockReelArticle(final Integer idArticle);
    Collection<MvtStkDto> mvtStkArticle(final Integer idArticle);
    MvtStkDto entreeStk(final MvtStkDto dto);
    MvtStkDto sortieStk(final MvtStkDto dto);
    MvtStkDto transfertStk(final MvtStkDto dto);
    MvtStkDto correctionStkPos(final MvtStkDto dto);
    MvtStkDto correctionStkNeg(final MvtStkDto dto);
}
