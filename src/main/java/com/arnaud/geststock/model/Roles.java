package com.arnaud.geststock.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="roles")
public class Roles extends AbstractEntity{

    @Column(name = "rolename", unique = true)
    private String roleName;

    /*@ManyToMany(mappedBy = "roles")
    private Collection<Utilisateur> utilisateurs;*/

    @ManyToMany(targetEntity = Privileges.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(
            name = "privileges_roles",
            joinColumns = @JoinColumn(
                    name = "idrole", referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "idprivilege", referencedColumnName = "id"
            )
    )
    private Set<Privileges> privileges;
}
