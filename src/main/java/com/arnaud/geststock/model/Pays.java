package com.arnaud.geststock.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "pays")
public class Pays extends AbstractEntity {

    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "icon")
    private String icon;

    @OneToMany(mappedBy = "pays", fetch = FetchType.LAZY)
    private Set<Ville> villes;

}
