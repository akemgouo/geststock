package com.arnaud.geststock.model;

public enum UnitArticle {
    M, CM, MM, Kg, Litre, Lot
}

