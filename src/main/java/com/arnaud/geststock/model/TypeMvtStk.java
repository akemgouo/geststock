package com.arnaud.geststock.model;

public enum TypeMvtStk {
    ENTREE, SORTIE, TRANSFERT, CORRECTION_POS, CORRECTION_NEG
}
