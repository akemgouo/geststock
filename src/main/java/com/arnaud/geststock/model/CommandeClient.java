package com.arnaud.geststock.model;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="commandeclient")
public class CommandeClient extends AbstractEntity{

    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "datecommande")
    private Instant dateCommande;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private CommandeSts status;

    @ManyToOne
    @JoinColumn(name = "idclient")
    private Client client;

    @OneToMany(mappedBy = "commandeClient")
    private Collection<LigneCommandeClient> ligneCommandeClients;

}
