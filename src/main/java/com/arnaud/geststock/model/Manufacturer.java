package com.arnaud.geststock.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="manufacturer")
public class Manufacturer extends AbstractEntity {

    @Column(name = "codemanufacturer", unique = true)
    private String codeManufacturer;

    @Column(name = "designation", unique = true)
    private String designation;

    @OneToMany(mappedBy = "manufacturer")
    private Collection<Article> articles;

}
