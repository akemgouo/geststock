package com.arnaud.geststock.model;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "trackingconnexion")
public class TrackingConnexion extends AbstractEntity {

    @Column(name = "app")
    private String app;

    @Column(name = "attemptresult")
    @Enumerated(EnumType.STRING)
    private AttemptResult attemptResult;

    @Column(name = "clientaddr")
    private String clientAddr;

    @Column(name = "clienthost")
    private String clientHost;

    @Column(name = "currentcount")
    private Integer currentCount;

    @Column(name = "nom")
    private String nom;

    @Column(name = "serverhost")
    private String serverHost;

    @Column(name = "servername")
    private String serverName;

    @Column(name = "attemptdate")
    private Instant attemptDate;

    @ManyToOne
    @JoinColumn(name = "idutilisateur")
    private Utilisateur utilisateur;
}
