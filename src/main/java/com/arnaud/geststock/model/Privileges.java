package com.arnaud.geststock.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="privileges")
public class Privileges extends AbstractEntity {

    @Column(name = "nom", unique = true)
    private String nom;

    /*@ManyToMany(mappedBy = "privileges")
    private Collection<Roles> roles;*/
}
