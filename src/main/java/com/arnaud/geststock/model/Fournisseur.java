package com.arnaud.geststock.model;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="fournisseur")
public class Fournisseur extends AbstractEntity {

    @Column(name = "codefournisseur", unique = true)
    private String codeFournisseur;

    @Column(name = "description")
    private String description;

    @OneToOne
    @JoinColumn(name = "idadresse")// champ composer à utiliser plusieur fois dans les autres classe
    private Adresse adresse;

    @Column(name = "photo")
    private String photo;

    @Column(name = "email")
    private String email;

    @Column(name = "numtel")
    private String numTel;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy = "fournisseur")
    private Collection<CommandeFournisseur> commandeFournisseurs;
}
