package com.arnaud.geststock.model;

public enum CommandeSts {
    NOUV,
    ENCRS,
    VALIDEE,
    LIVREE,
    FERMEE,
}
