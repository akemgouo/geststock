package com.arnaud.geststock.model;

public enum AttemptResult {
    FAILED,
    LOGIN,
    LOGOUT,
    RESTART,
    SUCCESS,
    TIMEOUT
}
