package com.arnaud.geststock.model;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="Commandefournisseur")
public class CommandeFournisseur extends AbstractEntity {

    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "datecommande")
    private Instant dateCommande;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private CommandeSts status;

    @ManyToOne
    @JoinColumn(name = "idfournisseur")
    private Fournisseur fournisseur;

    @OneToMany(mappedBy = "commandeFournisseur")
    private Collection<LigneCommandeFournisseur> ligneCommandeFournisseurs;
}
