package com.arnaud.geststock.model;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="vente")
public class Ventes extends AbstractEntity {

    @Column(name = "code", unique = true)
    private String code;

    @OneToMany(mappedBy = "vente")
    private List<LigneVente> ligneVentes;

    @Column(name = "datevente")
    private Instant dateVente;

    @Column(name = "commentaire")
    private String commentaire;
}
