package com.arnaud.geststock.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="mvtstk")
@NamedQueries({
        @NamedQuery(
                name = "MvtStk.stockReelArticle",
                query = "select sum(m.quantite) from MvtStk m where m.article.id = :article")
})
public class MvtStk extends AbstractEntity {

    @Column(name = "datemvtstk")
    private Instant dateMvtStk;

    @Column(name = "quantite")
    private BigDecimal quantite;

    @Column(name="typemvtstk")
    @Enumerated(EnumType.STRING)
    private TypeMvtStk typeMvtStk;

    @Column(name="sourcemvt")
    @Enumerated(EnumType.STRING)
    private SourceMvtStk sourceMvt;

    @ManyToOne
    @JoinColumn(name = "idarticle")
    private Article article;
}
