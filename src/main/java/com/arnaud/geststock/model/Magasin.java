package com.arnaud.geststock.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "magasin")
public class Magasin extends AbstractEntity {
    @Column(name = "codemagasin", unique = true, nullable = false)
    private String codemagasin;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "magasin",cascade = CascadeType.ALL)
    private Collection<Article> articles;

}
