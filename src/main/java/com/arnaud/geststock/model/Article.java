package com.arnaud.geststock.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="article")
public class Article extends AbstractEntity {

    @Column(name = "codearticle", unique = true)
    private String codeArticle;

    @Column(name = "designation")
    private String designation;

    @Column(name = "prixunitaireht")
    @DecimalMin(message = "Price should be greater than 0", value = "0")
    private BigDecimal prixUnitaire;

    @Column(name = "tauxtva")
    private BigDecimal tauxTva;

    @Column(name = "prixunitairettc")
    @DecimalMin(message = "Cost should be greater than 1", value = "1")
    private BigDecimal prixUnitaireTtc;

    @Column(name="orderunit")
    @Enumerated(EnumType.STRING)
    private UnitArticle orderunit;

    @Column(name="issueunit")
    @Enumerated(EnumType.STRING)
    private UnitArticle issueunit;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "identreprise")
    private Integer idEntreprise;

    @ManyToOne
    @JoinColumn(name = "idcategory", referencedColumnName = "id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "idmanufacturer", referencedColumnName = "id")
    private Manufacturer manufacturer;

    @OneToMany(mappedBy = "article")
    private Collection<LigneVente> ligneVentes;

    @OneToMany(mappedBy = "article")
    private Collection<LigneCommandeClient> ligneCommandeClients;

    @OneToMany(mappedBy = "article")
    private Collection<LigneCommandeFournisseur> ligneCommandeFournisseurs;

    @ManyToOne
    @JoinColumn(name = "idmagasin", referencedColumnName = "id")
    private Magasin magasin;

}
