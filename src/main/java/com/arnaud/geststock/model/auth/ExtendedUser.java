package com.arnaud.geststock.model.auth;

import com.arnaud.geststock.model.Status;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.Instant;
import java.util.Collection;
@Getter
@Setter
public class ExtendedUser extends User {

    private Integer idEntreprise;

    private String nom;

    private String prenom;

    private String email;

    private String username;

    private String photo;

    private Instant dateNaissance;

    private boolean enabled;

    private boolean tokenExpired;

    @Enumerated(EnumType.STRING)
    private Status status;

    private Boolean isspamdin;


    public ExtendedUser(final String username,
                        final String password,
                        final Integer idEntreprise,
                        final Instant dateNaissance,
                        final String nom,
                        final String prenom,
                        final String email,
                        final String photo,
                        final boolean enabled,
                        final boolean tokenExpired,
                        final Status status,
                        final Boolean isspamdin,
                        final Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.idEntreprise = idEntreprise;
        this.username = username;
        this.dateNaissance = dateNaissance;
        this.email = email;
        this.enabled = enabled;
        this.isspamdin = isspamdin;
        this.photo = photo;
        this.status = status;
        this.prenom = prenom;
        this.nom = nom;
        this.tokenExpired = tokenExpired;
    }

    public ExtendedUser(String username, String password, Integer idEntreprise, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.idEntreprise = idEntreprise;
    }
}
