package com.arnaud.geststock.model;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "adresses")
public class Adresse extends AbstractEntity{

    @Column(name = "adresse1")
    private String adresse1;

    @Column(name = "adresse2")
    private String adresse2;

    @Column(name = "codepostale")
    private String codePostale;

    @OneToOne(mappedBy = "adresse")
    private Entreprise entreprise;

    @OneToOne(mappedBy = "adresse")
    private Client client;

    @ManyToOne(cascade = CascadeType.MERGE, optional = false)
    @JoinColumn(name = "idville")
    private Ville ville;

    @OneToOne(mappedBy = "adresse")
    private Fournisseur fournisseur;

    @OneToOne(mappedBy = "adresse")
    private Utilisateur utilisateur;
}
