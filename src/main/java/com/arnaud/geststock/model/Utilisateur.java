package com.arnaud.geststock.model;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="utilisateur")
public class Utilisateur extends AbstractEntity {

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "photo")
    private String photo;

    @Column(name = "datenaissance")
    private Instant dateNaissance;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "tokenexpired")
    private boolean tokenExpired;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "isspamdin")
    private Boolean isspamdin;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] image;

    @ManyToOne
    @JoinColumn(name = "identreprise")
    private Entreprise entreprise;

    @ManyToMany(targetEntity = Roles.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(
            name = "utilisateur_roles",
            joinColumns = {@JoinColumn(
                    name = "idutilisateur", referencedColumnName = "id"
            )},
            inverseJoinColumns = {@JoinColumn(
                    name = "idrole", referencedColumnName = "id"
            )}
    )
    private Set<Roles> roles;

    @OneToMany(mappedBy = "utilisateur")
    private Collection<TrackingConnexion> trackingConnexions;

    @OneToOne
    @JoinColumn(name = "idadresse")
    private Adresse adresse;

}
