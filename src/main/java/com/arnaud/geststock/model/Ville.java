package com.arnaud.geststock.model;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "ville")
public class Ville extends AbstractEntity{

    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "idpays")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Pays pays;

    @OneToMany(mappedBy = "ville")
    private Set<Adresse> adresses;
}
