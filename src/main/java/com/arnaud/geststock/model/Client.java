package com.arnaud.geststock.model;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="client")
public class Client extends AbstractEntity {

    @Column(name = "codeclient", unique = true)
    private String codeClient;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] image;

    @OneToOne
    @JoinColumn(name = "idadresse")
    private Adresse adresse;

    @Column(name = "photo")
    private String photo;

    @Column(name = "email")
    private String email;

    @Column(name = "numtel")
    private String numTel;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy = "client")
    private Collection<CommandeClient> commandeClients;
}
