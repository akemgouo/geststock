package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.ArticleApi;
import com.arnaud.geststock.dto.ArticleDto;
import com.arnaud.geststock.dto.LigneCommandeClientDto;
import com.arnaud.geststock.dto.LigneCommandeFournisseurDto;
import com.arnaud.geststock.dto.LigneVenteDto;
import com.arnaud.geststock.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;


@RestController
@CrossOrigin
public class ArticleRestController implements ArticleApi {

    //@Autowired(required=true)
    private final ArticleService articleService;

    @Autowired
    public ArticleRestController(final ArticleService articleService){
        this.articleService = articleService;
    }

    @Override
    public ResponseEntity<ArticleDto> save(final ArticleDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(articleService.save(dto));
    }

    @Override
    public ResponseEntity<ArticleDto> findById(final Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.findById(id));
    }

    @Override
    public ResponseEntity<ArticleDto> findByCodeArticle(final String codeArticle) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.findByCodeArticle(codeArticle));
    }

    @Override
    public ResponseEntity<Collection<ArticleDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.findAll());
    }

    @Override
    public ResponseEntity<Collection<LigneVenteDto>> findHistoriqueVentes(final Integer idArticle) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.findHistoriqueVentes(idArticle));
    }

    @Override
    public ResponseEntity<Collection<LigneCommandeClientDto>> findHistoriqueCommandeClient(final Integer idArticle) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.findHistoriqueCommandeClient(idArticle));
    }

    @Override
    public ResponseEntity<Collection<LigneCommandeFournisseurDto>> findHistoriqueCommandeFournisseur(final Integer idArticle) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.findHistoriqueCommandeFournisseur(idArticle));
    }

    @Override
    public ResponseEntity<Collection<ArticleDto>> findAllArticleByIdCategory(final Integer idCategory) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.findAllArticleByIdCategory(idCategory));
    }

    @Override
    public ResponseEntity<Void> delete(Integer id) {
        articleService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
