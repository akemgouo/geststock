package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.CommandeFournisseurApi;
import com.arnaud.geststock.dto.CommandeFournisseurDto;
import com.arnaud.geststock.dto.LigneCommandeFournisseurDto;
import com.arnaud.geststock.model.CommandeSts;
import com.arnaud.geststock.services.CommandeFournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Collection;

@RestController
@CrossOrigin
public class CommandeFournisseurRestController implements CommandeFournisseurApi {

    private final CommandeFournisseurService commandeFournisseurService;

    @Autowired
    public CommandeFournisseurRestController(final CommandeFournisseurService commandeFournisseurService) {
        this.commandeFournisseurService = commandeFournisseurService;
    }

    @Override
    public ResponseEntity<CommandeFournisseurDto> save(final CommandeFournisseurDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeFournisseurService.save(dto));
    }

    @Override
    public ResponseEntity<CommandeFournisseurDto> updateStatusCommandeForunisseur(final Integer idCommandeFournisseur, final CommandeSts commandeSts) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeFournisseurService.updateStatusCommandeForunisseur(idCommandeFournisseur,commandeSts));
    }

    @Override
    public ResponseEntity<CommandeFournisseurDto> updateQuantiteCommandeFournisseur(final Integer idCommandeFournisseur, final Integer idLigneCmdFournissseur, final BigDecimal quantite) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeFournisseurService.updateQuantiteCommandeFournisseur(idCommandeFournisseur, idLigneCmdFournissseur,quantite));
    }

    @Override
    public ResponseEntity<CommandeFournisseurDto> updateFournisseurCommande(final Integer idCommandeFournisseur, final Integer idFournisseur) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeFournisseurService.updateFournisseurCommande(idCommandeFournisseur,idFournisseur));
    }

    @Override
    public ResponseEntity<CommandeFournisseurDto> updateArticleCommandeFournisseur(final Integer idCommande, final Integer idLigneCmdFournisseur, final Integer idArticle) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeFournisseurService.updateArticleCommandeFournisseur(idCommande,idLigneCmdFournisseur,idArticle));
    }

    @Override
    public ResponseEntity<Collection<LigneCommandeFournisseurDto>> findAllByCommandeFournisseurId(final Integer idCommandeFournisseur) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeFournisseurService.findAllByCommandeFournisseurId(idCommandeFournisseur));
    }

    @Override
    public ResponseEntity<CommandeFournisseurDto> findById(final Integer id) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(commandeFournisseurService.findById(id));
    }

    @Override
    public ResponseEntity<CommandeFournisseurDto> findCommandeFournisseurByCode(final String code) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeFournisseurService.findCommandeFournisseurByCode(code));
    }

    @Override
    public ResponseEntity<Collection<CommandeFournisseurDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(commandeFournisseurService.findAll());
    }

    @Override
    public ResponseEntity<CommandeFournisseurDto> deleteLigneCommandeFournisseur(Integer icCommande, Integer idLingeCommande) {
        return null;
    }

    @Override
    public ResponseEntity delete(final Integer id) {
        commandeFournisseurService.delete(id);
        return ResponseEntity.ok().build();
    }
}
