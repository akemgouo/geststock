package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.AdresseApi;
import com.arnaud.geststock.dto.AdresseDto;
import com.arnaud.geststock.services.AdresseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class AdresseRestController implements AdresseApi {

    private final AdresseService adresseService;

    @Autowired
    public AdresseRestController(final AdresseService adresseService) {
        this.adresseService = adresseService;
    }

    @Override
    public ResponseEntity<AdresseDto> save(final AdresseDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(adresseService.save(dto));
    }

    @Override
    public ResponseEntity<AdresseDto> findAdresseByAdresse1(final String addresse1) {
        return ResponseEntity.ok(adresseService.findAdresseByAdresse1(addresse1));
    }

    @Override
    public ResponseEntity<AdresseDto> findAdressebyId(final Integer id) {
        return ResponseEntity.ok().body(adresseService.findAdressebyId(id));
    }

    @Override
    public ResponseEntity<Collection<AdresseDto>> findAll() {
        return ResponseEntity.ok(adresseService.findAll());
    }

    @Override
    public ResponseEntity delete(final Integer id) {
        adresseService.delete(id);
        return ResponseEntity.ok().build();
    }
}
