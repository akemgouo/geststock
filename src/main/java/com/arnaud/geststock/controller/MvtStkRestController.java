package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.MvtStkApi;
import com.arnaud.geststock.dto.MvtStkDto;
import com.arnaud.geststock.services.MvtStkService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Collection;

@RestController
@CrossOrigin
public class MvtStkRestController implements MvtStkApi {

    private final MvtStkService mvtStkService;

    public MvtStkRestController(final MvtStkService mvtStkService) {
        this.mvtStkService = mvtStkService;
    }

    @Override
    public ResponseEntity<BigDecimal> stockReelArticle(final Integer idArticle) {
        return ResponseEntity.status(HttpStatus.OK).body(mvtStkService.stockReelArticle(idArticle));
    }

    @Override
    public ResponseEntity<Collection<MvtStkDto>> mvtStkArticle(final Integer idArticle) {
        return ResponseEntity.status(HttpStatus.OK).body(mvtStkService.mvtStkArticle(idArticle));
    }

    @Override
    public ResponseEntity<MvtStkDto> entreeStk(final MvtStkDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(mvtStkService.entreeStk(dto));
    }

    @Override
    public ResponseEntity<MvtStkDto> sortieStk(final MvtStkDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(mvtStkService.sortieStk(dto));
    }

    @Override
    public ResponseEntity<MvtStkDto> transfertStk(final MvtStkDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(mvtStkService.transfertStk(dto));
    }

    @Override
    public ResponseEntity<MvtStkDto> correctionStkPos(final MvtStkDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(mvtStkService.correctionStkPos(dto));
    }

    @Override
    public ResponseEntity<MvtStkDto> correctionStkNeg(final MvtStkDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(mvtStkService.correctionStkNeg(dto));
    }
}
