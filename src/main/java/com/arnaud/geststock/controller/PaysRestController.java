package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.PaysApi;
import com.arnaud.geststock.dto.PaysDto;
import com.arnaud.geststock.services.PaysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class PaysRestController implements PaysApi {

    private final PaysService paysService;

    @Autowired
    public PaysRestController(final PaysService paysService) {
        this.paysService = paysService;
    }

    @Override
    public ResponseEntity<PaysDto> save(final PaysDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(paysService.save(dto));
    }

    @Override
    public ResponseEntity<PaysDto> findById(final Integer id) {
        return ResponseEntity.ok().body(paysService.findById(id));
    }

    @Override
    public ResponseEntity<PaysDto> findPaysByCode(String code) {
        ResponseEntity.ok().body(paysService.findPaysByCode(code));
        return null;
    }

    @Override
    public ResponseEntity<Collection<PaysDto>> findAll() {
        return ResponseEntity.ok().body(paysService.findAll());
    }

    @Override
    public ResponseEntity delete(final Integer id) {
        paysService.delete(id);
        return ResponseEntity.ok().build();

    }
}
