package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.VilleApi;
import com.arnaud.geststock.dto.VilleDto;
import com.arnaud.geststock.services.VilleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class VilleRestController implements VilleApi {

    private final VilleService villeService;

    @Autowired
    public VilleRestController(final VilleService villeService) {
        this.villeService = villeService;
    }

    @Override
    public ResponseEntity<VilleDto> save(final VilleDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(villeService.save(dto));
    }

    @Override
    public ResponseEntity<VilleDto> findById(final Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(villeService.findById(id));
    }

    @Override
    public ResponseEntity<VilleDto> findVilleByCode(final String code) {
        return ResponseEntity.status(HttpStatus.OK).body(villeService.findVilleByCode(code));
    }

    @Override
    public ResponseEntity<Collection<VilleDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(villeService.findAll());
    }

    @Override
    public ResponseEntity delete(final Integer id) {
        villeService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
