package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.VentesApi;
import com.arnaud.geststock.dto.VentesDto;
import com.arnaud.geststock.services.VentesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class VentesRestController implements VentesApi {

    private final VentesService ventesService;

    @Autowired
    public VentesRestController(final VentesService ventesService) {
        this.ventesService = ventesService;
    }

    @Override
    public ResponseEntity<VentesDto> save(final VentesDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(ventesService.save(dto));
    }

    @Override
    public ResponseEntity<VentesDto> findById(final Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(ventesService.findById(id));
    }

    @Override
    public ResponseEntity<VentesDto> findVentesByCode(final String code) {
        return ResponseEntity.status(HttpStatus.OK).body(ventesService.findVentesByCode(code));
    }

    @Override
    public ResponseEntity<Collection<VentesDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(ventesService.findAll());
    }

    @Override
    public ResponseEntity delete(final Integer id) {
        ventesService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
