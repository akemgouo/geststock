package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.CategoryApi;
import com.arnaud.geststock.dto.CategoryDto;
import com.arnaud.geststock.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class CategoryRestController implements CategoryApi {

    private final CategoryService categoryService;

    @Autowired
    public CategoryRestController(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public ResponseEntity<CategoryDto> save(final CategoryDto dto) {
        CategoryDto isre = categoryService.save(dto);
        if (isre == null){
            return (ResponseEntity<CategoryDto>) ResponseEntity.status(HttpStatus.CREATED);
        }else{
            return (ResponseEntity<CategoryDto>) ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<CategoryDto> findById(final Integer id) {
        //return ResponseEntity.status(HttpStatus.OK).body(categoryService.findById(id));
        return categoryService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(
                        () -> ResponseEntity.notFound().build()
                );
    }

    @Override
    public ResponseEntity<CategoryDto> findCategoryByCode(final String codeCategory) {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.findCategoryByCode(codeCategory));
    }

    @Override
    public ResponseEntity<Collection<CategoryDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.findAll());
    }

    @Override
    public ResponseEntity<Void> delete(final Integer id) {
        categoryService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
