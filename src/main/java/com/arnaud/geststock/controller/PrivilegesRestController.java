package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.PrivilegesApi;
import com.arnaud.geststock.dto.PrivilegesDto;
import com.arnaud.geststock.services.PrivilegesService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class PrivilegesRestController implements PrivilegesApi {

    private final PrivilegesService privilegesService;

    public PrivilegesRestController(final PrivilegesService privilegesService) {
        this.privilegesService = privilegesService;
    }

    @Override
    public PrivilegesDto save(final PrivilegesDto dto) {
        return privilegesService.save(dto);
    }

    @Override
    public PrivilegesDto findById(final Integer id) {
        return privilegesService.findById(id);
    }

    @Override
    public PrivilegesDto findPrivilegesByNom(final String nom) {
        return null;
    }

    @Override
    public Collection<PrivilegesDto> findAll() {
        return null;
    }

    @Override
    public void delete(final Integer id) {

    }
}
