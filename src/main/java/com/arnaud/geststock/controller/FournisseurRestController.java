package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.FournisseurApi;
import com.arnaud.geststock.dto.FournisseurDto;
import com.arnaud.geststock.services.FournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class FournisseurRestController implements FournisseurApi {

    private final FournisseurService fournisseurService;

    @Autowired
    public FournisseurRestController(final FournisseurService fournisseurService) {
        this.fournisseurService = fournisseurService;
    }

    @Override
    public ResponseEntity<FournisseurDto> save(final FournisseurDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(fournisseurService.save(dto));
    }

    @Override
    public ResponseEntity<FournisseurDto> findById(final Integer id) {
        return ResponseEntity.ok(fournisseurService.findById(id));
    }

    @Override
    public ResponseEntity<Collection<FournisseurDto>> findAll() {
        return ResponseEntity.ok(fournisseurService.findAll());
    }

    @Override
    public ResponseEntity<FournisseurDto> findFournisseurByCodeFournisseur(final String code) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(fournisseurService.findFournisseurByCodeFournisseur(code));
    }

    @Override
    public ResponseEntity delete(final Integer id) { // prevoir le blocage de la suppression du fournisseur s'il a des commande au stauts != NOUV
        fournisseurService.delete(id);
        return ResponseEntity.ok().build();
    }
}
