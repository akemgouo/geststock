package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.UtilisateurApi;
import com.arnaud.geststock.dto.ChangePasswordUserDto;
import com.arnaud.geststock.dto.UtilisateurDto;
import com.arnaud.geststock.services.UtilisateurService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class UtilisateurRestController implements UtilisateurApi {

    private final UtilisateurService utilisateurService;

    public UtilisateurRestController(final UtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    @Override
    public ResponseEntity<UtilisateurDto> save(final UtilisateurDto dto) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(utilisateurService.save(dto));
    }

    @Override
    public ResponseEntity<UtilisateurDto> findById(final Integer id) {
        return ResponseEntity.ok().body(utilisateurService.findById(id));
    }

    @Override
    public ResponseEntity<Collection<UtilisateurDto>> findAll() {
        return  ResponseEntity.ok().body(utilisateurService.findAll());
    }

    @Override
    public ResponseEntity<UtilisateurDto> findUtilisateurByUsername(final String username) {
        return  ResponseEntity.ok().body(utilisateurService.findUtilisateurByUsername(username));
    }

    @Override
    public ResponseEntity delete(Integer id) {
        utilisateurService.delete(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<UtilisateurDto> changePasswordUtilisateur(final ChangePasswordUserDto dto) {
        return ResponseEntity.ok().body(utilisateurService.changePasswordUtilisateur(dto));
    }
}
