package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.ManufacturerApi;
import com.arnaud.geststock.dto.ManufacturerDto;
import com.arnaud.geststock.services.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class ManufacturerRestController implements ManufacturerApi {

    private final ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerRestController(final ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @Override
    public ResponseEntity<ManufacturerDto> save(final ManufacturerDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(manufacturerService.save(dto));
    }

    @Override
    public ResponseEntity<ManufacturerDto> findManufacturerByCodeManufacturer(final String code) {
        return ResponseEntity.status(HttpStatus.OK).body(manufacturerService.findManufacturerByCodeManufacturer(code));
    }

    @Override
    public ResponseEntity<ManufacturerDto> findManufacturerById(final Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(manufacturerService.findManufacturerById(id));
    }

    @Override
    public ResponseEntity<Collection<ManufacturerDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(manufacturerService.findAll());
    }

    @Override
    public ResponseEntity<Void> delete(final Integer id) {
        manufacturerService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
