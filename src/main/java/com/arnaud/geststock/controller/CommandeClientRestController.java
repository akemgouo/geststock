package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.CommandeClientApi;
import com.arnaud.geststock.dto.CommandeClientDto;
import com.arnaud.geststock.dto.LigneCommandeClientDto;
import com.arnaud.geststock.model.CommandeSts;
import com.arnaud.geststock.services.CommandeClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Collection;

@RestController
@CrossOrigin
public class CommandeClientRestController implements CommandeClientApi {

    private final CommandeClientService commandeClientService;

    @Autowired
    public CommandeClientRestController(final CommandeClientService commandeClientService) {
        this.commandeClientService = commandeClientService;
    }

    @Override
    public ResponseEntity<CommandeClientDto> save(final CommandeClientDto dto) {
        //return ResponseEntity.ok(commandeClientService.save(dto)).;
        return ResponseEntity.status(HttpStatus.CREATED).body(commandeClientService.save(dto));
    }

    @Override
    public ResponseEntity<CommandeClientDto> updateStatusCommande(final Integer idCommandeClient, final CommandeSts stsCommande) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeClientService.updateStatusCommande(idCommandeClient,stsCommande));
    }

    @Override
    public ResponseEntity<CommandeClientDto> updateQuantiteCommande(final Integer idCommande, final Integer idLigneCommande, final BigDecimal quantite) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeClientService.updateQuantiteCommande(idCommande,idLigneCommande,quantite));
    }

    @Override
    public ResponseEntity<CommandeClientDto> updateClientCommande(final Integer idCommande, final Integer idClient) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeClientService.updateClientCommande(idCommande,idClient));
    }

    @Override
    public ResponseEntity<CommandeClientDto> updateArticleCommande(final Integer idCommande, final Integer idLigngeCommande, final Integer idArticle) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeClientService.updateArticleCommande(idCommande,idLigngeCommande,idArticle));
    }

    @Override
    public ResponseEntity<CommandeClientDto> findById(final Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeClientService.findById(id));
    }

    @Override
    public ResponseEntity<CommandeClientDto> findCommandeClientByCode(final String code) {
        return ResponseEntity.ok(commandeClientService.findCommandeClientByCode(code));
    }

    @Override
    public ResponseEntity<Collection<CommandeClientDto>> findAll() {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(commandeClientService.findAll());
    }

    @Override
    public ResponseEntity<Collection<LigneCommandeClientDto>> findAllByCommandeClientId(final Integer idCommandeClient) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeClientService.findAllByCommandeClientId(idCommandeClient));
    }

    @Override
    public ResponseEntity<Void> delete(final Integer id) {
        commandeClientService.delete(id);
        return ResponseEntity.noContent().build(); // il faut se rassurer que la commande n'est pas utilise ailleur et si c'est le cas il faut traiter l'exception
    }

    @Override
    public ResponseEntity<CommandeClientDto> deleteLigneCommande(final Integer id, final Integer  idLigneCommande) {
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(commandeClientService.deleteLigneCommande(id,idLigneCommande));
    }
}
