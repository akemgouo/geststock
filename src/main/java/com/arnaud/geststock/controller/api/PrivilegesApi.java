package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.PrivilegesDto;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = PRIVILEGES_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, tags = "PrivilegesApi")
public interface PrivilegesApi {

    @PostMapping(value = CREATE_PRIVILEGES_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    PrivilegesDto save(@Validated @RequestBody final PrivilegesDto dto);

    @GetMapping(value = FIND_PRIVILEGES_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    PrivilegesDto findById(@PathVariable("idPriveleges") final Integer id);

    @GetMapping(value = FIND_PRIVILEGES_BY_NOM_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    PrivilegesDto findPrivilegesByNom(@PathVariable("nomPriveleges") final String nom);

    @GetMapping(value = FIND_ALL_PRIVILEGES_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    Collection<PrivilegesDto> findAll();

    @DeleteMapping(value = DELETE_PRIVILEGES_ENDPOINT)
    void delete(@PathVariable("idPriveleges")final Integer id);
}
