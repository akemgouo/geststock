package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.ChangePasswordUserDto;
import com.arnaud.geststock.dto.UtilisateurDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = UTILISATEUR_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, protocols = "http, https", tags = "UtilisateurApi")
public interface UtilisateurApi {

    @PostMapping(value = CREATE_UTILISATEUR_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<UtilisateurDto> save(@RequestBody final UtilisateurDto dto);

    @GetMapping(value = FIND_UTILISATEUR_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<UtilisateurDto>  findById(@PathVariable("idUtilisateur") final Integer id);

    @GetMapping(value = FIND_ALL_UTILISATEUR_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<UtilisateurDto>> findAll();

    @GetMapping(value = FIND_UTILISATEUR_BY_CODE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<UtilisateurDto> findUtilisateurByUsername(@PathVariable("usernameUtilisateur") final String username);

    @DeleteMapping(value = DELETE_UTILISATEUR_ENDPOINT)
    @ApiOperation(value = "Supprimer un utilisateur par son ID", notes = "Cette methode permet de supprimer un utilisateur dans la BD",
            responseContainer = "Utilisateur supprimer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'utilisateur à été supprimer")
    })
    ResponseEntity delete(@PathVariable("idUtilisateur") final Integer id);

    @PostMapping(value = CHANGE_PASSWORD_UTILISATEUR_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<UtilisateurDto> changePasswordUtilisateur(@Validated @RequestBody ChangePasswordUserDto dto);
}
