package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.CommandeFournisseurDto;
import com.arnaud.geststock.dto.LigneCommandeFournisseurDto;
import com.arnaud.geststock.model.CommandeSts;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = COMMNANDE_FOURNISSEUR_ENDPOIT, tags = "CommandeFournisseurApi", protocols = "http, https", produces = "application/json")
public interface CommandeFournisseurApi {

    @PostMapping(value = CREATE_COMMANDE_FOURNISSEUR_ENDPOINT)
    ResponseEntity<CommandeFournisseurDto> save(@Validated @RequestBody final CommandeFournisseurDto dto);

    @PatchMapping(value = UPDATE_COMMANDE_FOUNISSEURS_BY_STATUS_ENDMOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CommandeFournisseurDto> updateStatusCommandeForunisseur(@PathVariable("idCommandeFournisseur") final Integer idCommandeFournisseur, @PathVariable("stsCommande") final CommandeSts commandeSts);

    @PatchMapping(value = UPDATE_LIGNE_COMMANDE_FOUNISSEURS_BY_QUANTITE_ENDMOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CommandeFournisseurDto> updateQuantiteCommandeFournisseur(@PathVariable("idCommandeFournisseur") final Integer idCommandeFournisseur, @PathVariable("idLigneCommande") final Integer idLigneCmdFournissseur, @PathVariable("quantite") final BigDecimal quantite);

    @PatchMapping(value = UPDATE_COMMANDE_FOUNISSEURS_BY_FOUNISSEUR_ENDMOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CommandeFournisseurDto> updateFournisseurCommande(@PathVariable("idCommandeFournisseur") final Integer idCommandeFournisseur, @PathVariable("idFournisseur") final Integer idFournisseur);

    @PatchMapping(value = UPDATE_COMMANDE_FOUNISSEURS_BY_ARTICLE_ENDMOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CommandeFournisseurDto> updateArticleCommandeFournisseur(@PathVariable("idCommandeFournisseur") final Integer idCommande, @PathVariable("idLigneCommande") final  Integer idLigneCmdFournisseur, @PathVariable("idArticle") final Integer idArticle);

    @GetMapping(value = FIND_LIGNE_COMMANDE_FOUNISSEURS_BY_COMMANDEFOUNISSEURID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<LigneCommandeFournisseurDto>> findAllByCommandeFournisseurId (@PathVariable("idCommandeFournisseur") final Integer idCommandeFournisseur);

    @GetMapping(value = FIND_COMMANDE_FOURNISSEUR_BY_ID_ENDPOINT)
    ResponseEntity<CommandeFournisseurDto> findById(@PathVariable("idCommandeFournisseur") final Integer id);

    @GetMapping(value = FIND_COMMANDE_FOURNISSEUR_BY_CODE_ENDPOINT)
    ResponseEntity<CommandeFournisseurDto> findCommandeFournisseurByCode(@PathVariable("codeCommandeFournisseur")final String code);

    @GetMapping(value = FIND_ALL_COMMANDE_FOURNISSEUR_ENDPOINT)
    ResponseEntity<Collection<CommandeFournisseurDto>> findAll();

    @DeleteMapping(value = DELETE_LIGNE_COMMANDE_FOUNISSEURS_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CommandeFournisseurDto> deleteLigneCommandeFournisseur(@PathVariable("idCommandeFournisseur")final Integer icCommande, @PathVariable("idLigneCommande") final Integer idLingeCommande);

    @DeleteMapping(value = DELETE_COMMANDE_FOURNISSEUR_ENDPOINT)
    ResponseEntity delete(@PathVariable("idCommandeFournisseur") final Integer id);
}
