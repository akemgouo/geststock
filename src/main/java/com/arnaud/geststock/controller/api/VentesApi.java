package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.VentesDto;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = VENTES_ENDPOINT, protocols = "http,https", produces = "application/json", tags = "VentesApi")
public interface VentesApi {

    @PostMapping(value = CREATE_VENTES_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<VentesDto> save(@RequestBody final VentesDto dto);

    @GetMapping(value = FIND_VENTES_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<VentesDto> findById(@PathVariable("idVente") final Integer id);

    @GetMapping(value = FIND_VENTES_BY_CODE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<VentesDto> findVentesByCode(@PathVariable("codeVente") final String code);

    @GetMapping(value = FIND_ALL_VENTES_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<VentesDto>> findAll();

    @DeleteMapping(value = DELETE_VENTES_ENDPOINT)
    ResponseEntity delete(@PathVariable("idVente")final Integer id);
}
