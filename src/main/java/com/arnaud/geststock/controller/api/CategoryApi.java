package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.CategoryDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.APP_ROOT;

@Api(value = APP_ROOT + "/categories", protocols = "http, https", produces = "application/json", tags = "CategoryApi")
public interface CategoryApi {

    @PostMapping(value = APP_ROOT + "/categories/create",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enregistrer une categorie (Ajouter / Modifier)", notes = "Cette methode permet d'entregister ou de modifier une categorie",
            response = CategoryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'objet Category cree / modifier"),
            @ApiResponse(code = 400, message = "L'objet Category n'est pas valide")
    })
    ResponseEntity<CategoryDto> save(@RequestBody final CategoryDto dto);

    @GetMapping(value = APP_ROOT + "/categories/{idCategory}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Rechercher une category par ID", notes = "Cette methode permet de chercher une category par son ID",
            response = CategoryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'objet Category été trouvé"),
            @ApiResponse(code = 404, message = "Aucune catégorie n'existe dans la BD avec l'ID fourni")
    })
    ResponseEntity<CategoryDto> findById(@PathVariable("idCategory") final Integer id);

    @GetMapping(value = APP_ROOT + "/categories/{codeCategory}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Rechercher une category par son code", notes = "Cette methode permet de chercher une category par son code",
            response = CategoryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'objet Category été trouvé"),
            @ApiResponse(code = 404, message = "Aucune catégorie n'existe dans la BD avec le code fourni")
    })
    ResponseEntity<CategoryDto> findCategoryByCode(@PathVariable("codeCategory") final String codeCategory);

    @GetMapping(value = APP_ROOT + "/categories/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Renvoi la liste des cateogries", notes = "Cette methode permet de chercher et renvoyer la liste des categories qui existe dans la BD",
            responseContainer = "Collection<CateogryDto>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La liste des categories ou une liste vide")
    })
    ResponseEntity<Collection<CategoryDto>> findAll();

    @DeleteMapping(value = APP_ROOT + "/categories/delete/{idCategory}")
    @ApiOperation(value = "Supprimer une categorie par ID", notes = "Cette methode permet de supprimer une categorie dans la BD",
            responseContainer = "Categorie supprimer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La categorie à été supprimer")
    })
    ResponseEntity<Void> delete (@PathVariable("idCategory") final Integer id);
}
