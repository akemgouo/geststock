package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.EntrepriseDto;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = ENTREPRISE_ENDPOINT, produces = "application/json", protocols = "http, https", tags = "EntrepriseApi")
public interface EntrepriseApi {

    @PostMapping(value = CREATE_ENTREPRISE_ENDPOINT)
    ResponseEntity<EntrepriseDto> save(@RequestBody final EntrepriseDto dto);

    @GetMapping(value = FIND_ENTREPRISE_BY_ID_ENDPOINT)
    ResponseEntity<EntrepriseDto> findById(@PathVariable("idEntreprise") final Integer id);

    @GetMapping(value = FIND_ENTREPRISE_BY_CODE_ENDPOINT)
    ResponseEntity<EntrepriseDto> findEntrepriseByCode(@PathVariable("codeEntreprise")final String code);

    @GetMapping(value = FIND_ALL_ENTREPRISE_ENDPOINT)
    ResponseEntity<Collection<EntrepriseDto>> findAll();

    @DeleteMapping(value = DELETE_ENTREPRISE_ENDPOINT)
    ResponseEntity delete(@PathVariable("idEntreprise") final Integer id);
}
