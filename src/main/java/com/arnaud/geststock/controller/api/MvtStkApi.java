package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.MvtStkDto;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = MVTSKT_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, protocols = "https, http", tags = "MvtStkApi")
public interface MvtStkApi {

    @GetMapping(value = FIND_MVTSKT_BY_ID_ARTICLE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<BigDecimal> stockReelArticle(@PathVariable("idArticle") final Integer idArticle);

    @GetMapping(value = FIND_ALL_MVTSKT_BY_ID_ARTICLE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<MvtStkDto>> mvtStkArticle(@PathVariable("idArticle") final Integer idArticle);

    @PostMapping(value = ENTREE_MVTSTK_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<MvtStkDto> entreeStk(@Validated @RequestBody final MvtStkDto dto);

    @PostMapping(value = SORTIE_MVTSTK_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<MvtStkDto> sortieStk(@Validated @RequestBody final MvtStkDto dto);

    @PostMapping(value = TRANSFERT_MVTSTK_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<MvtStkDto> transfertStk(@Validated @RequestBody final MvtStkDto dto);

    @PostMapping(value = CORRECTION_POS_MVTSTK_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<MvtStkDto> correctionStkPos(@Validated @RequestBody final MvtStkDto dto);

    @PostMapping(value = CORRECTION_NEG_MVTSTK_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<MvtStkDto> correctionStkNeg(@Validated @RequestBody final MvtStkDto dto);
}
