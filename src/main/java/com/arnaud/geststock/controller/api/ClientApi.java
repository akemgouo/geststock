package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.ClientDto;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.APP_ROOT;

@Api(value = APP_ROOT + "/cleints", protocols = "http, https", produces = "application/json", tags = "ClientApi")
public interface ClientApi {

    @PostMapping(value = APP_ROOT + "/clients/create", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ClientDto>  save(@RequestBody final ClientDto dto);

    @GetMapping(value = APP_ROOT + "/clients/{idClient}")
    ResponseEntity<ClientDto> findById(@PathVariable("idClient") final Integer id);

    @GetMapping(value = APP_ROOT + "/clients/all", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<ClientDto>> findAll();

    @DeleteMapping(value = APP_ROOT + "/clients/delete/{idCloent}")
    ResponseEntity<Void> delete(@PathVariable("idClient") final Integer id);

    @GetMapping(value = APP_ROOT + "/client/{codeClient}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ClientDto> findClientByCodeClient(@PathVariable("codeClient") final String clientCode);

    @GetMapping(value = APP_ROOT + "/client/{emailClient}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ClientDto> findClientByEmail(@PathVariable("emailClient") final String email);
}
