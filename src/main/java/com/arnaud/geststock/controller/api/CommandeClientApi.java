package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.CommandeClientDto;
import com.arnaud.geststock.dto.LigneCommandeClientDto;
import com.arnaud.geststock.model.CommandeSts;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = COMMNANDE_CLIENT_ENDPOINT, protocols = "http, https", produces = MediaType.APPLICATION_JSON_VALUE, tags = "CommandeClientApi" )
public interface CommandeClientApi {

    @PostMapping(value = CREATE_COMMANDE_CLIENT_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CommandeClientDto> save(@Validated @RequestBody final CommandeClientDto dto);

    @PatchMapping(value = UPDATE_COMMANDE_CLIENT_BY_STATUS_ENDMOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CommandeClientDto> updateStatusCommande(@PathVariable("idCommandeClient") final Integer idCommandeClient, @PathVariable("stsCommande") CommandeSts stsCommande);

    @PatchMapping(value = UPDATE_LIGNE_COMMANDE_CLIENT_BY_QUANTITE_ENDMOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CommandeClientDto> updateQuantiteCommande(@PathVariable("idCommandeClient")final Integer idCommande, @PathVariable("idLigneCommande")final Integer idLigneCommande, @PathVariable("quantite") final BigDecimal quantite);

    @PatchMapping(value = UPDATE_COMMANDE_CLIENT_BY_CLIENT_ENDMOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CommandeClientDto> updateClientCommande(@PathVariable("idCommandeClient")final Integer idCommande, @PathVariable("idClient") final Integer idClient);

    @PatchMapping(value = UPDATE_COMMANDE_CLIENT_BY_ARTICLE_ENDMOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CommandeClientDto> updateArticleCommande(@PathVariable("idCommandeClient") final Integer idCommande, @PathVariable("idLigneCommande")final Integer idLigngeCommande, @PathVariable("idArticle")final Integer idArticle);

    @GetMapping(value = FIND_COMMANDE_CLIENT_BY_ID_ENDPOINT)
    ResponseEntity<CommandeClientDto> findById(@PathVariable final Integer idCommandeClient);

    @GetMapping(value = FIND_COMMANDE_CLIENT_BY_CODE_ENDPOINT)
    ResponseEntity<CommandeClientDto> findCommandeClientByCode(@PathVariable("codeCommandeClient")final String code);

    @GetMapping(value = FIND_ALL_COMMANDE_CLIENT_ENDPOINT)
    ResponseEntity<Collection<CommandeClientDto>> findAll();

    @GetMapping(value = FIND_LIGNE_COMMANDE_CLIENT_BY_COMMANDECLIENTID_ENDPOINT)
    ResponseEntity<Collection<LigneCommandeClientDto>> findAllByCommandeClientId(@PathVariable("idCommandeClient") final Integer idCommandeClient);

    @DeleteMapping(value = DELETE_COMMANDE_CLIENT_ENDPOINT)
    ResponseEntity<Void> delete(@PathVariable("idCommandeClient") final Integer id);

    @DeleteMapping(value = DELETE_LIGNE_COMMANDE_CLIENT_ENDPOINT)
    ResponseEntity<CommandeClientDto> deleteLigneCommande(@PathVariable("idCommandeClient") final Integer id, @PathVariable("idLigneCommande") final  Integer idLigneCommande);

}
