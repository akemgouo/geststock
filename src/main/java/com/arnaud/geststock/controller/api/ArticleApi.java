package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.ArticleDto;
import com.arnaud.geststock.dto.LigneCommandeClientDto;
import com.arnaud.geststock.dto.LigneCommandeFournisseurDto;
import com.arnaud.geststock.dto.LigneVenteDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = ARTICLE_ENDPOINT, protocols = "http,https", produces = MediaType.APPLICATION_JSON_VALUE, tags = "ArticleApi")
public interface ArticleApi {

    @PostMapping(value = CREATE_ARTICLE_ENDPOINT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enregistrer un article (Ajouter / Modifier)", notes = "Cette methode permet d'entregister ou de modifier un article",
            response = ArticleDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'objet Article cree / modifier"),
            @ApiResponse(code = 400, message = "L'objet Article n'est pas valide")
    })
    ResponseEntity<ArticleDto> save(@Validated @RequestBody final ArticleDto dto);

    @GetMapping(value = FIND_ARTICLE_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Rechercher un article par ID", notes = "Cette methode permet de chercher un article par son ID",
            response = ArticleDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'objet Article été trouvé"),
            @ApiResponse(code = 404, message = "Aucun article n'existe dans la BD avec l'ID fourni")
    })
    ResponseEntity<ArticleDto> findById(@PathVariable("idArticle") final Integer id);

    @GetMapping(value = FIND_ARTICLE_BY_CODE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Rechercher un article par Code", notes = "Cette methode permet de chercher un article par son Code",
            response = ArticleDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'objet Article été trouvé"),
            @ApiResponse(code = 404, message = "Aucun article n'existe dans la BD avec le code fourni")
    })
    ResponseEntity<ArticleDto> findByCodeArticle(@PathVariable("codeArticle") final String codeArticle);

    @GetMapping(value = FIND_ALL_ARTICLE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Renvoi la liste des articles", notes = "Cette methode permet de chercher et renvoyer la liste des article qui esiste dans la BD",
            responseContainer = "Collection<ArticleDto>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La liste des articles ou une liste vide")
    })
    ResponseEntity<Collection<ArticleDto>> findAll();

    @GetMapping(value = FIND_ALL_HISTORIQUE_VENTES_ARTICLE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<LigneVenteDto>> findHistoriqueVentes(@PathVariable("idArticle") final Integer idArticle);

    @GetMapping(value = FIND_ALL_HISTORIQUE_COMANDE_CLIENT_ARTICLE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<LigneCommandeClientDto>> findHistoriqueCommandeClient(@PathVariable("idArticle")final Integer idArticle);

    @GetMapping(value = FIND_ALL_HISTORIQUE_COMANDE_FOURNISSEUR_ARTICLE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<LigneCommandeFournisseurDto>> findHistoriqueCommandeFournisseur(@PathVariable("idArticle")final Integer idArticle);

    @GetMapping(value = FIND_ALL_ARTICLE_BY_ID_CATEGORY_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<ArticleDto>> findAllArticleByIdCategory(@PathVariable("idCategory") final Integer idCategory);

    @DeleteMapping(value = DELETE_ARTICLE_ENDPOINT)
    @ApiOperation(value = "Supprimer un article par ID", notes = "Cette methode permet de supprimer un article dans la BD",
            responseContainer = "Article supprimer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'article à été supprimer")
    })
    ResponseEntity<Void> delete(@PathVariable("idArticle") final Integer id);
}
