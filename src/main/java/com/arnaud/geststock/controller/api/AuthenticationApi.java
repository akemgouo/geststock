package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.auth.AuthenticationRequest;
import com.arnaud.geststock.dto.auth.AuthenticationResponse;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.arnaud.geststock.utils.Constants.*;

@Api( value = AUTHENTICATION_ENDPOINT, protocols = "http, https", produces = MediaType.APPLICATION_JSON_VALUE, tags = "AuthenticationApi")
@RequestMapping(value = AUTHENTICATION_ENDPOINT)
public interface AuthenticationApi {

    @PostMapping(value = USER_AUTHENTICATION_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<AuthenticationResponse> authenticate(@RequestBody @Validated AuthenticationRequest request);

    @GetMapping(value = USER_ISCONNECT_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getUserConnected();
}
