package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.ManufacturerDto;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = MANUFACTURER_ENDPOINT, tags = "ManufacturerApi", protocols = "http, https", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ManufacturerApi {

    @PostMapping(value = CREATE_MANUFACTURER_ENDPOINT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ManufacturerDto> save(@RequestBody @Validated final ManufacturerDto dto);

    @GetMapping(value = FIND_MANUFACTURER_BY_CODE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ManufacturerDto> findManufacturerByCodeManufacturer(@PathVariable("codeManufacturer")final String code);

    @GetMapping(value = FIND_MANUFACTURER_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ManufacturerDto> findManufacturerById(@PathVariable("idManufacturer") final Integer id);

    @GetMapping(value = FIND_ALL_MANUFACTURER_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<ManufacturerDto>> findAll();

    @DeleteMapping(value = DELETE_MANUFACTURER_ENDPOINT)
    ResponseEntity delete (@PathVariable("idManufacturer")final Integer id);
}
