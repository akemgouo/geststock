package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.PaysDto;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = PAYS_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, protocols = "http, https", tags = "PaysApi")
public interface PaysApi {

    @PostMapping(value = CREATE_PAYS_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<PaysDto> save(@Validated @RequestBody final PaysDto dto);

    @GetMapping(value = FIND_PAYS_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<PaysDto> findById(@PathVariable("idPays") final Integer id);

    @GetMapping(value = FIND_PAYS_BY_CODE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<PaysDto> findPaysByCode(@PathVariable("codePays")final String code);

    @GetMapping(value = FIND_ALL_PAYS_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<PaysDto>> findAll();

    @DeleteMapping(value = DELETE_PAYS_ENDPOINT)
    ResponseEntity delete(@PathVariable("idPays") final Integer id);
}
