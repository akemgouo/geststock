package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.AdresseDto;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = ADRESSE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, protocols = "http, https", tags = "AdresseApi")
public interface AdresseApi {

    @PostMapping(value = CREATE_ADRESSE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<AdresseDto> save(@Validated @RequestBody final AdresseDto dto);

    @GetMapping(value = FIND_ADRESSE_BY_ADRESSE1_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<AdresseDto> findAdresseByAdresse1(@PathVariable("addresse1") final String addresse1);

    @GetMapping(value = FIND_ADRESSE_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<AdresseDto> findAdressebyId(@PathVariable("idAdresse") final Integer id);

    @GetMapping(value = FIND_ALL_ADRESSE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<AdresseDto>> findAll();

    @DeleteMapping(value = DELETE_ADRESSE_ENDPOINT)
    ResponseEntity delete (@PathVariable("idAdresse")final Integer id);
}
