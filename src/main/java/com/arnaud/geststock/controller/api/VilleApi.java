package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.VilleDto;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = VILLE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, protocols = "http, https", tags = "VilleApi")
public interface VilleApi {

    @PostMapping(value = CREATE_VILLE_ENDPOINT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    ResponseEntity<VilleDto> save(@Validated @RequestBody final VilleDto dto);

    @GetMapping(value = FIND_VILLE_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<VilleDto> findById(@PathVariable("idVille") final Integer id);

    @GetMapping(value = FIND_VILLE_BY_CODE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<VilleDto> findVilleByCode(@PathVariable("codeVille")final String code);

    @GetMapping(value = FIND_ALL_VILLE_ENDPOINT)
    ResponseEntity<Collection<VilleDto>> findAll();

    @DeleteMapping(value = DELETE_VILLE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity delete(@PathVariable("idVille")final Integer id);
}
