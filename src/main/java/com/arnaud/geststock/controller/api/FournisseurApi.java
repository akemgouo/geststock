package com.arnaud.geststock.controller.api;

import com.arnaud.geststock.dto.FournisseurDto;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.arnaud.geststock.utils.Constants.*;

@Api(value = FOURNISSEUR_ENDPOINT, tags = "FournisseurApi", protocols = "http, https", produces = MediaType.APPLICATION_JSON_VALUE)
public interface FournisseurApi {

    @PostMapping(value = CREATE_FOURNISSEUR_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<FournisseurDto> save(@RequestBody final FournisseurDto dto);

    @GetMapping(value = FIND_FOURNISSEUR_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<FournisseurDto> findById(@PathVariable("idFournisseur") final Integer id);

    @GetMapping(value = FIND_ALL_FOURNISSEUR_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Collection<FournisseurDto>> findAll();

    @GetMapping(value = FIND_FOURNISSEUR_BY_CODE_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<FournisseurDto> findFournisseurByCodeFournisseur (@PathVariable("codeFournisseur") final String code);

    @DeleteMapping(value = DELETE_FOURNISSEUR_ENDPOINT)
    ResponseEntity delete(@PathVariable("codeFournisseur")  final Integer id);
}
