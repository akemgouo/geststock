package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.EntrepriseApi;
import com.arnaud.geststock.dto.EntrepriseDto;
import com.arnaud.geststock.services.EnterpriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class EntrepriseRestController implements EntrepriseApi {

    private final EnterpriseService enterpriseService;

    @Autowired
    public EntrepriseRestController(final EnterpriseService enterpriseService) {
        this.enterpriseService = enterpriseService;
    }

    @Override
    public ResponseEntity<EntrepriseDto> save(EntrepriseDto dto) {
        return ResponseEntity.ok(enterpriseService.save(dto));
    }

    @Override
    public ResponseEntity<EntrepriseDto> findById(Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(enterpriseService.findById(id));
    }

    @Override
    public ResponseEntity<EntrepriseDto> findEntrepriseByCode(String code) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(enterpriseService.findEntrepriseByCode(code));
    }

    @Override
    public ResponseEntity<Collection<EntrepriseDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(enterpriseService.findAll());
    }

    @Override
    public ResponseEntity delete(Integer id) {
        enterpriseService.delete(id);
        return ResponseEntity.ok().build();
    }
}
