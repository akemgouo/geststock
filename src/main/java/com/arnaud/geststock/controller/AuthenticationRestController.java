package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.AuthenticationApi;
import com.arnaud.geststock.dto.TrackingConnexionDto;
import com.arnaud.geststock.dto.auth.AuthenticationRequest;
import com.arnaud.geststock.dto.auth.AuthenticationResponse;
import com.arnaud.geststock.model.AttemptResult;
import com.arnaud.geststock.model.auth.ExtendedUser;
import com.arnaud.geststock.services.TrackingConnexionService;
import com.arnaud.geststock.services.auth.JwtUserDetailsService;
import com.arnaud.geststock.utils.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Instant;

@RestController
@CrossOrigin
@Slf4j
public class AuthenticationRestController implements AuthenticationApi {

    private final AuthenticationManager authenticationManager;
    private final JwtUserDetailsService jwtUserDetailsService;
    private final JwtTokenUtil jwtTokenUtil;
    private final TrackingConnexionService trackingConnexionService;

    @Autowired
    public AuthenticationRestController(final AuthenticationManager authenticationManager,
                                        final JwtUserDetailsService jwtUserDetailsService,
                                        final JwtTokenUtil jwtTokenUtil,
                                        final TrackingConnexionService trackingConnexionService) {
        this.authenticationManager = authenticationManager;
        this.jwtUserDetailsService = jwtUserDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.trackingConnexionService = trackingConnexionService;
    }

    @Override
    public ResponseEntity<AuthenticationResponse> authenticate(AuthenticationRequest request)  {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        if (authentication.isAuthenticated()){
            final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(request.getEmail());
            final String jwt = jwtTokenUtil.generateToken((ExtendedUser)userDetails);
            String currentIp = "";
            try {
                InetAddress iAddress = InetAddress.getLocalHost();
                currentIp = iAddress.getHostAddress();
            }catch (UnknownHostException e){
                e.printStackTrace();
            }
            trackingConnexionService.save(TrackingConnexionDto.builder()
                    .app("Gestion de stock")
                    .currentCount(1)
                    .attemptDate(Instant.now())
                    .clientAddr(
                            currentIp
                    )
                    .attemptResult(AttemptResult.LOGIN)
                    .clientHost("")
                    .serverHost("")
                    .serverName("")
                    //.utilisateur()
                    .nom("")
                    .build());
            return ResponseEntity.ok(AuthenticationResponse.builder()
                    .jwtToken(jwt)
                    .username(userDetails.getUsername())
                    .nom(((ExtendedUser) userDetails).getNom())
                    .email(((ExtendedUser) userDetails).getEmail())
                    .enabled(userDetails.isEnabled())
                    .photo(((ExtendedUser) userDetails).getPhoto())
                    .prenom(((ExtendedUser) userDetails).getPrenom())
                    .tokenExpired(((ExtendedUser) userDetails).isTokenExpired())
                    .status(((ExtendedUser) userDetails).getStatus())
                    .build());
        }else {
            log.error("Cannot connected");
            return null;
        }
    }

    @Override
    public ResponseEntity getUserConnected(){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof UserDetails){
            return new ResponseEntity(((UserDetails) principal).getUsername(), HttpStatus.OK);
        }
        return new ResponseEntity("User is not Connected", HttpStatus.FORBIDDEN);
    }
}
