package com.arnaud.geststock.controller;

import com.arnaud.geststock.controller.api.ClientApi;
import com.arnaud.geststock.dto.ClientDto;
import com.arnaud.geststock.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin
public class ClientRestController implements ClientApi {

    private final ClientService clientService;

    @Autowired
    public ClientRestController(ClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    public ResponseEntity<ClientDto> save(final ClientDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(clientService.save(dto));
    }

    @Override
    public ResponseEntity<ClientDto> findById(final Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.findById(id));
    }

    @Override
    public ResponseEntity<Collection<ClientDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.findAll());
    }

    @Override
    public ResponseEntity<Void> delete(final Integer id) {
        clientService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Override
    public ResponseEntity<ClientDto> findClientByCodeClient(final String clientCode) {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.findClientByCodeClient(clientCode));
    }

    @Override
    public ResponseEntity<ClientDto> findClientByEmail(final String email) {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.findClientByEmail(email));
    }
}
