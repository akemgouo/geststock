package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.CommandeFournisseur;
import com.arnaud.geststock.model.CommandeSts;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.Collection;

@Data
@Builder
public class CommandeFournisseurDto {

    private Integer id;

    private String code;

    private String description;

    private Instant dateCommande;

    private CommandeSts status;

    private FournisseurDto fournisseur;

    private Collection<LigneCommandeFournisseurDto> ligneCommandeFournisseurs;

    public static CommandeFournisseurDto convertEntityToDto(CommandeFournisseur commandeFournisseur){

        if (commandeFournisseur == null)
            return null;

        return CommandeFournisseurDto.builder()
                .id(commandeFournisseur.getId())
                .code(commandeFournisseur.getCode())
                .description(commandeFournisseur.getDescription())
                .status(commandeFournisseur.getStatus())
                .dateCommande(commandeFournisseur.getDateCommande())
                .fournisseur(FournisseurDto.convertEntityToDto(commandeFournisseur.getFournisseur()))
                .build();
    }

    public static CommandeFournisseur convertDtoToEntity(CommandeFournisseurDto commandeFournisseurDto){

        if(commandeFournisseurDto == null)
            return null;

        CommandeFournisseur commandeFournisseur = new CommandeFournisseur();

        commandeFournisseur.setId(commandeFournisseurDto.getId());
        commandeFournisseur.setCode(commandeFournisseurDto.getCode());
        commandeFournisseur.setDateCommande(commandeFournisseurDto.getDateCommande());
        commandeFournisseur.setDescription(commandeFournisseurDto.getDescription());
        commandeFournisseur.setStatus(commandeFournisseurDto.getStatus());
        commandeFournisseur.setFournisseur(FournisseurDto.convertDtoToEntity(commandeFournisseurDto.getFournisseur()));

        return commandeFournisseur;
    }

    public boolean isCommandeLivree(){
        return CommandeSts.LIVREE.equals(this.status);
    }
}
