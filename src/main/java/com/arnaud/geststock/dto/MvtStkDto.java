package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.MvtStk;
import com.arnaud.geststock.model.SourceMvtStk;
import com.arnaud.geststock.model.TypeMvtStk;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@Builder
public class MvtStkDto {

    private Integer id;

    private Instant dateMvtStk;

    private BigDecimal quantite;

    private TypeMvtStk typeMvtStk;

    private ArticleDto article;

    private SourceMvtStk sourceMvt;

    public static MvtStkDto convertEntityToDto(MvtStk mvtStk) {

        if (mvtStk == null)
            return null;

        return MvtStkDto.builder()
                .dateMvtStk(mvtStk.getDateMvtStk())
                .quantite(mvtStk.getQuantite())
                .typeMvtStk(mvtStk.getTypeMvtStk())
                .sourceMvt(mvtStk.getSourceMvt())
                .build();
    }

    public static MvtStk convertDtoToEntity(MvtStkDto mvtStkDto){

        if(mvtStkDto == null)
            return null;

        MvtStk mvtStk = new MvtStk();
        mvtStk.setDateMvtStk(mvtStkDto.getDateMvtStk());
        mvtStk.setQuantite(mvtStkDto.getQuantite());
        mvtStk.setTypeMvtStk(mvtStkDto.getTypeMvtStk());
        mvtStk.setSourceMvt(mvtStkDto.getSourceMvt());

        return mvtStk;
    }
}
