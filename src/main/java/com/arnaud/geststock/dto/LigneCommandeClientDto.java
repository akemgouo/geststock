package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.LigneCommandeClient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class LigneCommandeClientDto {

    private Integer id;

    private ArticleDto article;

    @JsonIgnore
    private CommandeClientDto commandeClient;

    private BigDecimal quantite;

    private BigDecimal prixUnitaire;

    public static LigneCommandeClientDto convertEntityToDto(LigneCommandeClient ligneCommandeClient) {

        if(ligneCommandeClient == null)
            //throw new LigneCommandeClientNotFoundException("Customer order line not found");
            return null;

        return LigneCommandeClientDto.builder()
                .quantite(ligneCommandeClient.getQuantite())
                .prixUnitaire(ligneCommandeClient.getPrixUnitaire())
                .build();
    }

    public static LigneCommandeClient convertDtoToEntity(LigneCommandeClientDto ligneCommandeClientDto){

        if(ligneCommandeClientDto == null)
            //throw new LigneCommandeClientNotFoundException("Customer order line not found");
            return null;

        LigneCommandeClient ligneCommandeClient = new LigneCommandeClient();

        ligneCommandeClient.setQuantite(ligneCommandeClientDto.getQuantite());
        ligneCommandeClient.setPrixUnitaire(ligneCommandeClientDto.getPrixUnitaire());

        return ligneCommandeClient;
    }
}
