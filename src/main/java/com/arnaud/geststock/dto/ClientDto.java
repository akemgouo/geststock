package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Client;
import com.arnaud.geststock.model.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class ClientDto {

    private Integer id;

    private String codeClient;

    private String nom;

    private String prenom;

    private AdresseDto adresse;

    private String photo;

    private String email;

    private String numTel;

    private Status status;

    private byte[] image;

    @JsonIgnore
    private Collection<CommandeClientDto> commandeClients;

    public static ClientDto convertEntityToDto(final Client client){
        if (client == null)
            return null;

        return ClientDto.builder()
                .id(client.getId())
                .codeClient(client.getCodeClient())
                .nom(client.getNom())
                .prenom(client.getPrenom())
                .email(client.getEmail())
                .numTel(client.getNumTel())
                .image(client.getImage())
                .photo(client.getPhoto())
                .status(client.getStatus())
                .build();
    }

    public static Client convertDtoToEntity(final ClientDto clientDto){

        if (clientDto == null){
            //throw new ClientNotFoundException("Customer not found");
            return null;
        }


        Client client = new Client();
        client.setId(clientDto.getId());
        client.setNom(clientDto.getNom());
        client.setCodeClient(clientDto.getCodeClient());
        client.setPrenom(clientDto.getPrenom());
        client.setEmail(clientDto.getEmail());
        client.setImage(clientDto.getImage());
        client.setNumTel(clientDto.getNumTel());
        client.setPhoto(clientDto.getPhoto());
        client.setStatus(clientDto.getStatus());
        return client;
    }
}
