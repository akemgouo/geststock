package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Article;
import com.arnaud.geststock.model.Magasin;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class MagasinDto {

    private Integer id;

    private String codemagasin;

    private String description;

    @JsonIgnore
    private Collection<Article> articles;

    public static Magasin convertDtoToEntity(final MagasinDto dto){
        if (dto == null)
            return null;

        Magasin magasin = new Magasin();

        magasin.setId(dto.getId());
        magasin.setCodemagasin(dto.getCodemagasin());
        magasin.setDescription(dto.getDescription());
        //magasin.setArticles(ArticleDto.convertDtoToEntity(dto.getArticles()));

        return magasin;
    }

    public static MagasinDto convertEntityToDto(final Magasin magasin){
        if(magasin == null)
            return null;

        return MagasinDto.builder()
                .id(magasin.getId())
                .codemagasin(magasin.getCodemagasin())
                .description(magasin.getDescription())
                .build();
    }
}
