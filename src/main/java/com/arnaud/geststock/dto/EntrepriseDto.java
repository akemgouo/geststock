package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Entreprise;
import com.arnaud.geststock.model.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class EntrepriseDto {

    private Integer id;

    private String code;

    private String nom;

    private String description;

    private String raisonSocial;

    private String codeFiscal;

    private String photo;

    private String email;

    private String numTel;

    private String siteWeb;

    private Status status;

    private AdresseDto adresse;

    @JsonIgnore
    private Collection<UtilisateurDto> utilisateurs;

    public static EntrepriseDto convertEntityToDto(Entreprise entreprise){

        if (entreprise == null){
            return null;
        }

        return EntrepriseDto.builder()
                .id(entreprise.getId())
                .code(entreprise.getCode())
                .nom(entreprise.getNom())
                .description(entreprise.getDescription())
                .codeFiscal(entreprise.getCodeFiscal())
                .email(entreprise.getEmail())
                .photo(entreprise.getPhoto())
                .numTel(entreprise.getNumTel())
                .siteWeb(entreprise.getSiteWeb())
                .status(entreprise.getStatus())
                .raisonSocial(entreprise.getRaisonSocial())
                .adresse(AdresseDto.convertEntityToDto(entreprise.getAdresse()))
                .build();
    }

    public static Entreprise convertDtoToEntity(EntrepriseDto entrepriseDto){

        if(entrepriseDto == null){
            return null;
        }

        Entreprise entreprise = new Entreprise();
        entreprise.setId(entrepriseDto.getId());
        entreprise.setCode(entrepriseDto.getCode());
        entreprise.setNom(entrepriseDto.getNom());
        entreprise.setCodeFiscal(entrepriseDto.getCodeFiscal());
        entreprise.setDescription(entrepriseDto.getDescription());
        entreprise.setRaisonSocial(entrepriseDto.getRaisonSocial());
        entreprise.setEmail(entrepriseDto.getEmail());
        entreprise.setPhoto(entrepriseDto.getPhoto());
        entreprise.setNumTel(entrepriseDto.getNumTel());
        entreprise.setSiteWeb(entrepriseDto.getSiteWeb());
        entreprise.setStatus(entrepriseDto.getStatus());
        entreprise.setAdresse(AdresseDto.convertDtoToEntity(entrepriseDto.getAdresse()));

        return entreprise;

    }
}
