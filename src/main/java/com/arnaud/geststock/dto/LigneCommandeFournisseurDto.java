package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.LigneCommandeFournisseur;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class LigneCommandeFournisseurDto {

    private Integer id;

    private ArticleDto article;

    private CommandeFournisseurDto commandeFournisseur;

    private BigDecimal quantite;

    private BigDecimal prixUnitaire;

    public static LigneCommandeFournisseurDto convertEntityToDto(LigneCommandeFournisseur ligneCommandeFournisseur) {

        if(ligneCommandeFournisseur == null)
            //throw new LigneCommandeClientNotFoundException("Supplier order line not found");
            return null;

        return LigneCommandeFournisseurDto.builder()
                .quantite(ligneCommandeFournisseur.getQuantite())
                .prixUnitaire(ligneCommandeFournisseur.getPrixUnitaire())
                .build();
    }

    public static LigneCommandeFournisseur convertDtoToEntity(LigneCommandeFournisseurDto ligneCommandeFournisseurDto) {

        if(ligneCommandeFournisseurDto == null)
            //throw new LigneCommandeClientNotFoundException("Supplier order line not found");
            return null;

        LigneCommandeFournisseur ligneCommandeFournisseur = new LigneCommandeFournisseur();

        ligneCommandeFournisseur.setQuantite(ligneCommandeFournisseurDto.getQuantite());
        ligneCommandeFournisseur.setPrixUnitaire(ligneCommandeFournisseurDto.getPrixUnitaire());

        return ligneCommandeFournisseur;
    }
}
