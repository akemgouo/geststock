package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Privileges;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class PrivilegesDto {

    private Integer id;

    private String nom;

    @JsonIgnore
    private Collection<RolesDto> roles;

    public static PrivilegesDto convertEntityToDto(final Privileges privileges){

        if (privileges == null)
            return null;

        return PrivilegesDto.builder()
                .id(privileges.getId())
                .nom(privileges.getNom())
                .build();
    }

    public static Privileges convertDtoToEntity(final PrivilegesDto privilegesDto){

        if(privilegesDto == null)
            return null;

        Privileges privileges = new Privileges();
        privileges.setId(privilegesDto.getId());
        privileges.setNom(privilegesDto.getNom());

        return privileges;
    }
}
