package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Manufacturer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Builder
@Data
public class ManufacturerDto {

    private Integer id;

    private String codeManufactorer;

    private String designation;

    @JsonIgnore
    private Collection<ArticleDto> articles;

    public static ManufacturerDto convertEntityToDto(final Manufacturer manufacturer){
        if (manufacturer == null){
            return null;
        }

        return ManufacturerDto.builder()
                .id(manufacturer.getId())
                .codeManufactorer(manufacturer.getCodeManufacturer())
                .designation(manufacturer.getDesignation())
                .build();
    }


    public static Manufacturer convertDtoToEntity(final ManufacturerDto dto){

        if(dto==null){
            return null;
        }

        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setId(dto.getId());
        manufacturer.setCodeManufacturer(dto.getCodeManufactorer());
        manufacturer.setDesignation(dto.getDesignation());

        return manufacturer;

    }


}
