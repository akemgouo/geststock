package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Adresse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdresseDto {

    private Integer id;

    private String adresse1;

    private String adresse2;

    private String codePostale;

    @JsonIgnore
    private EntrepriseDto entreprise;

    @JsonIgnore
    private ClientDto client;

    private VilleDto ville;

    @JsonIgnore
    private FournisseurDto fournisseur;

    @JsonIgnore
    private UtilisateurDto utilisateur;

    public static AdresseDto convertEntityToDto(final Adresse adresse) {
        if (adresse == null){
            return null;
        }

        return AdresseDto.builder()
                .id(adresse.getId())
                .adresse1(adresse.getAdresse1())
                .adresse2(adresse.getAdresse2())
                .codePostale(adresse.getCodePostale())
                .ville(VilleDto.convertEntityToDto(adresse.getVille()))
                .build();
    }

    public static Adresse convertDtoToEntity(final AdresseDto adresseDto){
        if (adresseDto == null ){
            return null;
        }
        Adresse adresse = new Adresse();
        adresse.setId(adresseDto.getId());
        adresse.setAdresse1(adresseDto.getAdresse1());
        adresse.setAdresse2(adresseDto.getAdresse2());
        adresse.setCodePostale(adresseDto.getCodePostale());
        adresse.setVille(VilleDto.convertDtoToEntity(adresseDto.getVille()));
        return adresse;
    }
}
