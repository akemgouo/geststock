package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Ventes;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.Collection;

@Data
@Builder
public class VentesDto {

    private Integer id;

    private String code;

    @JsonIgnore
    private Collection<LigneVenteDto> ligneVentes;

    private Instant dateVente;

    private String commentaire;

    public static VentesDto convertEntityToDto(final Ventes ventes){

        if (ventes == null)
            //throw new VentesNotFoundException("Sales not found");
            return null;

        return VentesDto.builder()
                .id(ventes.getId())
                .code(ventes.getCode())
                .commentaire(ventes.getCommentaire())
                .dateVente(ventes.getDateVente())
                .build();
    }

    public static Ventes convertDtoToEntity(final VentesDto ventesDto){

        if (ventesDto == null)
            //throw new VentesNotFoundException("Sales not found");
            return null;

        Ventes ventes = new Ventes();
        ventes.setId(ventesDto.getId());
        ventes.setCode(ventesDto.getCode());
        ventes.setCommentaire(ventesDto.getCommentaire());
        ventes.setDateVente(ventesDto.getDateVente());

        return ventes;
    }
}
