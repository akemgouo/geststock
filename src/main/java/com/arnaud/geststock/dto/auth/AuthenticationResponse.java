package com.arnaud.geststock.dto.auth;

import com.arnaud.geststock.model.Status;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthenticationResponse {

    private String nom;

    private String prenom;

    private String email;

    private String username;

    private String photo;

    private boolean enabled;

    private boolean tokenExpired;

    private Status status;

    private String jwtToken;

    private String errorMsg;
}
