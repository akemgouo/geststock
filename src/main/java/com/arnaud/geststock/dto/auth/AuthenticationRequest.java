package com.arnaud.geststock.dto.auth;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthenticationRequest {

    private final String email;
    private final String password;
}
