package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.AttemptResult;
import com.arnaud.geststock.model.TrackingConnexion;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class TrackingConnexionDto {

    private Integer id;

    private String app;

    private AttemptResult attemptResult;

    private String clientAddr;

    private String clientHost;

    private Integer currentCount;

    private String nom;

    private String serverHost;

    private String serverName;

    private Instant attemptDate;

    private UtilisateurDto utilisateur;

    public static TrackingConnexionDto convertEntityToDto(TrackingConnexion trackingConnexion){

        if(trackingConnexion == null)
            return null;

        return TrackingConnexionDto.builder()
                .app(trackingConnexion.getApp())
                .attemptDate(trackingConnexion.getAttemptDate())
                .attemptResult(trackingConnexion.getAttemptResult())
                .clientAddr(trackingConnexion.getClientAddr())
                .clientHost(trackingConnexion.getClientHost())
                .nom(trackingConnexion.getNom())
                .serverHost(trackingConnexion.getServerHost())
                .serverName(trackingConnexion.getServerName())
                .currentCount(trackingConnexion.getCurrentCount())
                .utilisateur(UtilisateurDto.convertEntityToDto(trackingConnexion.getUtilisateur()))
                .build();
    }

    public static TrackingConnexion convertDtoToEntity(TrackingConnexionDto trackingConnexionDto) {

        if(trackingConnexionDto == null)
            return null;

        TrackingConnexion trackingConnexion = new TrackingConnexion();

        trackingConnexion.setApp(trackingConnexionDto.getApp());
        trackingConnexion.setAttemptDate(trackingConnexionDto.getAttemptDate());
        trackingConnexion.setAttemptResult(trackingConnexionDto.getAttemptResult());
        trackingConnexion.setClientAddr(trackingConnexionDto.getClientAddr());
        trackingConnexion.setClientHost(trackingConnexionDto.getClientHost());
        trackingConnexion.setCurrentCount(trackingConnexionDto.getCurrentCount());
        trackingConnexion.setNom(trackingConnexionDto.getNom());
        trackingConnexion.setServerHost(trackingConnexionDto.getServerHost());
        trackingConnexion.setServerName(trackingConnexionDto.getServerName());
        trackingConnexion.setUtilisateur(UtilisateurDto.convertDtoToEntity(trackingConnexionDto.getUtilisateur()));

        return trackingConnexion;
    }
}
