package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Fournisseur;
import com.arnaud.geststock.model.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class FournisseurDto {

    private Integer id;

    private String codeFournisseur;

    private String description;

    private AdresseDto adresse;

    private String photo;

    private String email;

    private String numTel;

    private Status status;

    @JsonIgnore
    private Collection<CommandeFournisseurDto> commandeFournisseurs;

    public static FournisseurDto convertEntityToDto(final Fournisseur fournisseur) {
        if (fournisseur == null)
            return null;

        return FournisseurDto.builder()
                .id(fournisseur.getId())
                .codeFournisseur(fournisseur.getCodeFournisseur())
                .description(fournisseur.getDescription())
                .email(fournisseur.getEmail())
                .numTel(fournisseur.getNumTel())
                .photo(fournisseur.getPhoto())
                .status(fournisseur.getStatus())
                .build();
    }

    public static Fournisseur convertDtoToEntity(final FournisseurDto fournisseurDto){

        if (fournisseurDto == null)
            return null;

        Fournisseur fournisseur = new Fournisseur();
        fournisseur.setId(fournisseurDto.getId());
        fournisseur.setCodeFournisseur(fournisseurDto.getCodeFournisseur());
        fournisseur.setDescription(fournisseurDto.getDescription());
        fournisseur.setEmail(fournisseurDto.getEmail());
        fournisseur.setNumTel(fournisseurDto.getNumTel());
        fournisseur.setPhoto(fournisseurDto.getPhoto());
        fournisseur.setStatus(fournisseurDto.getStatus());
        return fournisseur;
    }
}
