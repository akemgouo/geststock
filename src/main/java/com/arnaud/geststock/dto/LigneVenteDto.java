package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.LigneVente;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class LigneVenteDto {

    private Integer id;

    private VentesDto vente;

    private BigDecimal quantite;

    private BigDecimal prixUnitaire;

    private ArticleDto article;

    public static LigneVenteDto convertEntityToDto(LigneVente ligneVente) {

        if(ligneVente == null)
            //throw new LigneCommandeClientNotFoundException("Buyer order line not found");
            return null;

        return LigneVenteDto.builder()
                .id(ligneVente.getId())
                .quantite(ligneVente.getQuantite())
                .prixUnitaire(ligneVente.getPrixUnitaire())
                .build();
    }

    public static LigneVente convertDtoToEntity(LigneVenteDto ligneVenteDto){

        if(ligneVenteDto == null)
            //throw new LigneCommandeClientNotFoundException("Buyer order line not found");
            return null;

        LigneVente ligneVente = new LigneVente();
        ligneVente.setId(ligneVenteDto.getId());
        ligneVente.setQuantite(ligneVenteDto.getQuantite());
        ligneVente.setPrixUnitaire(ligneVenteDto.getPrixUnitaire());

        return ligneVente;
    }
}
