package com.arnaud.geststock.dto;


import com.arnaud.geststock.model.Status;
import com.arnaud.geststock.model.Utilisateur;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
public class UtilisateurDto {

    private Integer id;

    private String nom;

    private String prenom;

    private String email;

    private String password;

    private String username;

    private String photo;

    private Instant dateNaissance;

    private boolean enabled;

    private boolean tokenExpired;

    private Status status;

    private Boolean isspamdin;

    private EntrepriseDto entreprise;

    private Set<RolesDto> roles;

    private byte[] image;

    @JsonIgnore
    private Collection<TrackingConnexionDto> trackingConnexions;

    private AdresseDto adresse;

    public static UtilisateurDto convertEntityToDto(Utilisateur utilisateur){

        if (utilisateur == null)
            return null;

        return UtilisateurDto.builder()
                .id(utilisateur.getId())
                .username(utilisateur.getUsername())
                .dateNaissance(utilisateur.getDateNaissance())
                .email(utilisateur.getEmail())
                .enabled(utilisateur.isEnabled())
                .tokenExpired(utilisateur.isTokenExpired())
                .isspamdin(utilisateur.getIsspamdin())
                .nom(utilisateur.getNom())
                .password(utilisateur.getPassword())
                .prenom(utilisateur.getPrenom())
                .status(utilisateur.getStatus())
                .photo(utilisateur.getPhoto())
                .image(utilisateur.getImage())
                .entreprise(EntrepriseDto.convertEntityToDto(utilisateur.getEntreprise()))
                .roles(utilisateur.getRoles() != null ?
                        utilisateur.getRoles().stream()
                        .map(RolesDto::convertEntityToDto)
                        .collect(Collectors.toSet()) : null
                        )
                .build();
    }

    public static Utilisateur convertDtoToEntity(UtilisateurDto utilisateurDto){

        if(utilisateurDto == null)
            return null;

        Utilisateur utilisateur = new Utilisateur();

        utilisateur.setId(utilisateurDto.getId());
        utilisateur.setUsername(utilisateurDto.getUsername());
        utilisateur.setPassword(utilisateurDto.getPassword());
        utilisateur.setNom(utilisateurDto.getNom());
        utilisateur.setDateNaissance(utilisateurDto.getDateNaissance());
        utilisateur.setEmail(utilisateurDto.getEmail());
        utilisateur.setEnabled(utilisateurDto.isEnabled());
        utilisateur.setTokenExpired(utilisateurDto.isTokenExpired());
        utilisateur.setPrenom(utilisateurDto.getPrenom());
        utilisateur.setStatus(utilisateurDto.getStatus());
        utilisateur.setPhoto(utilisateurDto.getPhoto());
        utilisateur.setIsspamdin(utilisateurDto.getIsspamdin());
        utilisateur.setPassword(utilisateurDto.getPassword());
        utilisateur.setImage(utilisateurDto.getImage());
        utilisateur.setEntreprise(EntrepriseDto.convertDtoToEntity(utilisateurDto.getEntreprise()));
        utilisateur.setRoles(utilisateurDto.roles != null ?
                utilisateurDto.roles.stream().map(RolesDto::convertDtoToEntity).collect(Collectors.toSet()) : null );

        return utilisateur;
    }
}
