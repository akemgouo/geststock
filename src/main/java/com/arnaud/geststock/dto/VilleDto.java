package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Ville;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class VilleDto {

    private Integer id;

    private String code;

    private String description;

    private PaysDto pays;

    @JsonIgnore
    private Set<AdresseDto> adresses;

    public static VilleDto convertEntityToDto(Ville ville){

        if(ville == null)
            return null;

        return VilleDto.builder()
                .id(ville.getId())
                .code(ville.getCode())
                .description(ville.getDescription())
                .pays(PaysDto.convertEntityToDto(ville.getPays()))
                .build();
    }

    public static Ville convertDtoToEntity(VilleDto villeDto){

        if(villeDto == null)
            return null;

        Ville ville = new Ville();
        ville.setId(villeDto.getId());
        ville.setCode(villeDto.getCode());
        ville.setPays(PaysDto.convertDtoToEntity(villeDto.getPays()));
        ville.setDescription(villeDto.getDescription());

        return ville;

    }
}
