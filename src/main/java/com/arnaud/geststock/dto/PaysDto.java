package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Pays;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class PaysDto {

    private Integer id;

    private String code;

    private String description;

    private String icon;

    private Set<VilleDto> villes;

    public static PaysDto convertEntityToDto(Pays pays){

        if (pays == null)
            return  null;

        return PaysDto.builder()
                .id(pays.getId())
                .code(pays.getCode())
                .description(pays.getDescription())
                .icon(pays.getIcon())
                .build();
    }

    public static Pays convertDtoToEntity(PaysDto paysDto){

        if (paysDto == null)
            //throw  new PaysNotFoundException("Country not found");
            return null;

        Pays pays = new Pays();
        pays.setId(paysDto.getId());
        pays.setCode(paysDto.getCode());
        pays.setDescription(paysDto.getDescription());
        pays.setIcon(pays.getIcon());

        return pays;
    }

}
