package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.CommandeClient;
import com.arnaud.geststock.model.CommandeSts;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.Collection;

@Data
@Builder
public class CommandeClientDto {

    private Integer id;

    private String code;

    private String description;

    private Instant dateCommande;

    private CommandeSts status;

    private ClientDto client;

    private Collection<LigneCommandeClientDto> ligneCommandeClients;

    public static CommandeClientDto convertEntityToDto(CommandeClient commandeClient){

        if (commandeClient == null)
            return null;

        return CommandeClientDto.builder()
                .id(commandeClient.getId())
                .code(commandeClient.getCode())
                .status(commandeClient.getStatus())
                .description(commandeClient.getDescription())
                .dateCommande(commandeClient.getDateCommande())
                .client(ClientDto.convertEntityToDto(commandeClient.getClient()))
                .build();
    }

    public static CommandeClient convertDtoToEntity(CommandeClientDto commandeClientDto){

        if(commandeClientDto == null)
            return null;

        CommandeClient commandeClient = new CommandeClient();
        commandeClient.setId(commandeClientDto.getId());
        commandeClient.setCode(commandeClientDto.getCode());
        commandeClient.setStatus(commandeClientDto.getStatus());
        commandeClient.setDateCommande(commandeClientDto.getDateCommande());
        commandeClient.setDescription(commandeClientDto.getDescription());
        commandeClient.setClient(ClientDto.convertDtoToEntity(commandeClientDto.getClient()));

        return commandeClient;
    }

    public boolean isCommandeLivree(){
        return CommandeSts.LIVREE.equals(this.status);
    }
}
