package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Category;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Builder
@Data
public class CategoryDto {

    private Integer id;

    private String code;

    private String designation;

    private Integer IdEntreprise;

    @JsonIgnore
    private Collection<ArticleDto> articles;

    public static CategoryDto convertEntityToDto(final Category category){
        if (category == null){
            // TODO throw and exception

            return null;
        }

        // Category -> CategoryDto
        return CategoryDto.builder()
                .id(category.getId())
                .code(category.getCode())
                .designation(category.getDesignation())
                .IdEntreprise(category.getIdEntreprise())
                .build();

    }

    public static Category convertDtoToEntity(final CategoryDto categoryDto){
        if (categoryDto == null){
            return null;
        }

        Category category = new Category();
        category.setId(categoryDto.getId());
        category.setCode(categoryDto.getCode());
        category.setDesignation(categoryDto.getDesignation());
        category.setIdEntreprise(categoryDto.getIdEntreprise());
        return category;
    }
}
