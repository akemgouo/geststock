package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Article;
import com.arnaud.geststock.model.UnitArticle;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

import static com.arnaud.geststock.utils.ImageUtility.compressImage;
import static com.arnaud.geststock.utils.ImageUtility.decompressImage;

@Builder
@Data
public class ArticleDto {

    private Integer id;

    private String codeArticle;

    private String designation;

    private BigDecimal prixUnitaire;

    private BigDecimal tauxTva;

    private UnitArticle orderunit;

    private UnitArticle issueunit;

    private BigDecimal prixUnitaireTtc;

    private byte[] photo;

    private Integer idEntreprise;

    private CategoryDto category;

    private ManufacturerDto manufacturer;

    private MagasinDto magasin;

    public static ArticleDto convertEntityToDto(final Article article) {
        if (article == null ){
            return null;
        }

        return ArticleDto.builder()
                .id(article.getId())
                .codeArticle(article.getCodeArticle())
                .designation(article.getDesignation())
                .prixUnitaire(article.getPrixUnitaire())
                .prixUnitaireTtc(article.getPrixUnitaireTtc())
                .tauxTva(article.getTauxTva())
                .issueunit(article.getIssueunit())
                .orderunit(article.getOrderunit())
                .photo(decompressImage(article.getPhoto()))
                .idEntreprise(article.getIdEntreprise())
                .category(CategoryDto.convertEntityToDto(article.getCategory()))
                .manufacturer(ManufacturerDto.convertEntityToDto(article.getManufacturer()))
                .magasin(MagasinDto.convertEntityToDto(article.getMagasin()))
                .build();
    }

    public static Article convertDtoToEntity(final ArticleDto articleDto){

        if (articleDto == null){
            return null;
        }
        Article article = new Article();
        article.setId(articleDto.getId());
        article.setCodeArticle(articleDto.getCodeArticle());
        article.setDesignation(articleDto.getDesignation());
        article.setPrixUnitaire(articleDto.getPrixUnitaire());
        article.setPrixUnitaireTtc(articleDto.getPrixUnitaireTtc());
        article.setPhoto(compressImage(articleDto.getPhoto()));
        article.setTauxTva(articleDto.getTauxTva());
        article.setIssueunit(articleDto.getIssueunit());
        article.setOrderunit(articleDto.getOrderunit());
        article.setMagasin(MagasinDto.convertDtoToEntity(articleDto.getMagasin()));
        article.setIdEntreprise(articleDto.getIdEntreprise());
        article.setCategory(CategoryDto.convertDtoToEntity(articleDto.getCategory()));
        article.setManufacturer(ManufacturerDto.convertDtoToEntity(articleDto.getManufacturer()));

        return article;
    }
}
