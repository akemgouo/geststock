package com.arnaud.geststock.dto;

import com.arnaud.geststock.model.Roles;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class RolesDto {

    private Integer id;

    private String roleName;

    /*@JsonIgnore
    private Collection<UtilisateurDto> utilisateurs;*/

    private Set<PrivilegesDto> privileges;

    public static RolesDto convertEntityToDto(Roles roles){

        if (roles == null)
           return null;

        return RolesDto.builder()
                .id(roles.getId())
                .roleName(roles.getRoleName())
                /*.utilisateurs(roles.getUtilisateurs() != null ?
                        roles.getUtilisateurs().stream().map(UtilisateurDto::convertEntityToDto).collect(Collectors.toList()): null)*/
                .build();
    }

    public static Roles convertDtoToEntity (RolesDto rolesDto) {

        if(rolesDto == null)
            return null;

        Roles roles = new Roles();
        roles.setId(rolesDto.getId());
        roles.setRoleName(rolesDto.getRoleName());
        /*roles.setUtilisateurs(rolesDto.utilisateurs != null ?
                rolesDto.utilisateurs.stream().map(UtilisateurDto::convertDtoToEntity).collect(Collectors.toList()): null);*/
        return roles;

    }
}
