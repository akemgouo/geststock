package com.arnaud.geststock;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootTest
@ComponentScan(basePackages = {"com.arnaud.geststock.repository"})
@EnableJpaAuditing
class GeststockApplicationTests {

    @Test
    void contextLoads() {
    }

}
