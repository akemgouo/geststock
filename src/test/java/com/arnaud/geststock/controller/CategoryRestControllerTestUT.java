package com.arnaud.geststock.controller;

import com.arnaud.geststock.dto.CategoryDto;
import com.arnaud.geststock.model.Category;
import com.arnaud.geststock.services.ArticleService;
import com.arnaud.geststock.services.CategoryService;
import com.arnaud.geststock.utils.ApplicationRequestFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({CategoryRestController.class})
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class CategoryRestControllerTestUT  {

    @Autowired
    private MockMvc mvc;

    @Mock
    private CategoryService categoryService;

    @Mock
    private ArticleService articleService;

    @MockBean
    private CategoryRestController categoryApi;

    @MockBean
    private ApplicationRequestFilter applicationRequestFilter;

    private JacksonTester<Category> jsonCategory;

    private CategoryDto categoryDto;

    private String token ;

    @BeforeEach
    public void init(){
        /*JacksonTester.initFields(this, new ObjectMapper());
        mvc = MockMvcBuilders.standaloneSetup(categoryApi)
                .setControllerAdvice(new RestExceptionHandler())
                .build();*/

        token = "";

        categoryDto = CategoryDto.builder()
                .id(1)
                .code("Cat 01")
                .designation("Category 01")
                .IdEntreprise(1)
                .build();
    }

    @SneakyThrows
    @Test
    @WithMockUser(username = "akemgouo", password = "P@ssw0rd", roles = "ADMIN")
    void should_saveCategoryInController() {
        //rule.strictness(Strictness.LENIENT);
        when(categoryService.save(categoryDto)).thenReturn(categoryDto);
        //when
        ResultActions resultActions = mvc.perform(
                MockMvcRequestBuilders.post("/geststock/v1/categories/create")
                        //.header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(asJsonString(categoryDto))
                        .accept(MediaType.APPLICATION_JSON_VALUE));

        //Then
        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect((ResultMatcher) jsonPath("$.code",is(categoryDto.getCode())));
    }

    @Test
    void findById() {
    }

    @Test
    void findCategoryByCode() {
    }

    @Test
    void findAll() {
    }

    @Test
    void delete() {
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}