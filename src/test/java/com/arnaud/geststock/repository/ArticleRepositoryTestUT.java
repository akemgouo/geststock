package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Article;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;

@DataJpaTest
@AutoConfigureTestDatabase
@Tag("ArticleRepositoryTestUT")
public class ArticleRepositoryTestUT {

    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleRepositoryTestUT(final ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }


    @Test
    @DisplayName("Create a new article")
    public void should_CreateNewArticle(){

        //Given
        Article article = new Article();
        article.setCodeArticle("Item001");
        article.setDesignation("Tomato");
        article.setPrixUnitaire(BigDecimal.valueOf(2542));
    }
}
