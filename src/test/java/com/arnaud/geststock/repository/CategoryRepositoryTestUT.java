package com.arnaud.geststock.repository;

import com.arnaud.geststock.model.Category;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Tag("CategorySimulationTest")
@ActiveProfiles(value = "test")
public class CategoryRepositoryTestUT {

    private final CategoryRepository categoryRepository;

    private Category categoryDto;

    @Autowired
    public CategoryRepositoryTestUT(final CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Test
    @DisplayName("Create a new Category Test")
    public void should_CreateNewCateogory(){
        //Given
        categoryDto = NewCategory();
        //When
        Category savedCategory = categoryRepository.save((categoryDto));

        System.out.println("categoryDto = " + savedCategory);
        //Then
        assertThat(categoryDto.getCode()).isEqualTo(savedCategory.getCode());
        assertThat(categoryDto.getDesignation()).isEqualTo(savedCategory.getDesignation());
        assertThat(categoryDto.getId()).isEqualTo(savedCategory.getId());
        assertThat(categoryDto.getIdEntreprise()).isEqualTo(savedCategory.getIdEntreprise());
    }

    @Test
    @DisplayName("Update a existing Category Test")
    public void should_UpdateNewCategoryCreate(){
        //Given
        Category category = new Category();
        category.setCode("Cat2");
        category.setDesignation("Category 2");
        category.setIdEntreprise(1);

        //When
        Category savedCategory = categoryRepository.save(category);
        //Optional<Category> categoryOptional = categoryRepository.findById(savedCategory.getId());
        Category category1 = savedCategory;
        System.out.println(category);
        category1.setCode("Cat03");
        savedCategory = categoryRepository.save(category1);
        System.out.println(savedCategory);

        //Then
        assertThat(category.getCode()).isEqualTo(savedCategory.getCode());
    }

    @Test
    @DisplayName("Find all Category in Database")
    public void should_FindAllCategories(){
        //Given
        Category category = new Category();
        category.setCode("Cat001");
        category.setDesignation("Category 001");
        category.setIdEntreprise(1);

        //When
        categoryRepository.save(category);
        Iterable<Category> categories = categoryRepository.findAll();

        //Then
        assertThat(categories).hasSizeGreaterThan(0);
    }

    @Test
    @DisplayName("Delete an existing Category")
    public void should_deleteExistantCategory(){
        //Given
        Category category = new Category();
        category.setCode("Cat001");
        category.setDesignation("Category 001");
        category.setIdEntreprise(1);

        //When2
        Category savedCategory = categoryRepository.save(category);
        Optional<Category> categoryOptional = categoryRepository.findById(savedCategory.getId());
        categoryRepository.deleteById(categoryOptional.get().getId());
        Iterable<Category> categories = categoryRepository.findAll();

        //Then
        assertThat(categories).isEmpty();
    }

    private Category NewCategory(){
        Category category = new Category();
        category.setIdEntreprise(1);
        category.setCode("Cat1");
        category.setCreationDate(Instant.now());
        category.setLastModifiedDate(Instant.now());
        category.setDesignation("Category 1");

        return category;
    }
}