package com.arnaud.geststock.services.impl;

import com.arnaud.geststock.dto.CategoryDto;
import com.arnaud.geststock.exception.EntityNotFoundException;
import com.arnaud.geststock.exception.ErrorCodes;
import com.arnaud.geststock.exception.InvalidEntityException;
import com.arnaud.geststock.model.Category;
import com.arnaud.geststock.repository.ArticleRepository;
import com.arnaud.geststock.repository.CategoryRepository;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.quality.Strictness;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
//@RunWith(MockitoJUnitRunner.class)
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
class CategoryServiceImplTestUT {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private ArticleRepository articleRepository;

    @InjectMocks
    private CategoryServiceImpl categoryService;

    private Category category;

    private CategoryDto categoryDto;

    CategoryServiceImplTestUT() {
    }

    @BeforeEach
    public void initCategory(){
        categoryDto = CategoryDto.builder()
                .id(1)
                .code("Cat 01")
                .designation("Category 01")
                .IdEntreprise(1)
                .build();

        category = new Category();
        category.setId(1);
        category.setCode("Cat2");
        category.setDesignation("Category 2");
        category.setIdEntreprise(1);
    }

    @Test
    public void should_SaveCategoryWithSuccess(){
        //Given

        given(categoryRepository.save(CategoryDto.convertDtoToEntity(categoryDto))).willReturn(CategoryDto.convertDtoToEntity(categoryDto));

        //when(categoryRepository.save(ArgumentMatchers.any(Category.class))).thenReturn(CategoryDto.convertDtoToEntity(categoryDto));

        //When
        CategoryDto savedCategory = categoryService.save(categoryDto);

        System.out.println(savedCategory);

        //Then
        assertNotNull(savedCategory);
        assertNotNull(savedCategory.getId());
    }

    //@MockitoSettings(strictness = Strictness.WARN)
    @Test
    public void shoul_UpdateAndExistingCategory(){
        rule.strictness(Strictness.LENIENT);
        //Given
        given(categoryRepository.save(CategoryDto.convertDtoToEntity(categoryDto))).willReturn(CategoryDto.convertDtoToEntity(categoryDto));

        categoryDto.setCode("Cat2");
        categoryDto.setDesignation("Cate 2");

        //When
        CategoryDto updateSavedCategoryDto = categoryService.save(categoryDto);

        //Then
        assertEquals("Cat2",updateSavedCategoryDto.getCode());
        //assertEquals(updateSavedCategoryDto.getDesignation(), "Cate 2");
    }

    @Test
    public void shoul_ThrowInvalidEntityException(){
        //Given
        CategoryDto expectedCategoryDto = CategoryDto.builder().build();

        //When
        InvalidEntityException expectedException = assertThrows(InvalidEntityException.class, () -> categoryService.save(expectedCategoryDto));

        //Then
        assertEquals(ErrorCodes.CATEGORY_NOT_VALID, expectedException.getErrorCodes());
    }

    @Test
    public void shoul_ThrowEntityNotFoundException(){
        EntityNotFoundException expectedEntityNotFoundException = assertThrows(EntityNotFoundException.class, ()-> categoryService.findById(0));
        assertEquals(ErrorCodes.CATEGORY_NOT_FOUND, expectedEntityNotFoundException.getErrorCodes());
    }

    @Test
    public void should_findCategoryId(){
        //Given
        given(categoryRepository.findById(1)).willReturn(Optional.of(CategoryDto.convertDtoToEntity(categoryDto)));

        //When
        Optional<CategoryDto> savedCategory = categoryService.findById(categoryDto.getId());

        Assertions.assertThat(savedCategory).isNotNull();
    }

    @Test
    public void shoul_DeleteCategory(){

        BDDMockito.given(categoryRepository.save(CategoryDto.convertDtoToEntity(categoryDto))).willReturn(CategoryDto.convertDtoToEntity(categoryDto));

        //when(categoryRepository.save(ArgumentMatchers.any(Category.class))).thenReturn(CategoryDto.convertDtoToEntity(categoryDto));

        //When
        CategoryDto savedCategory = categoryService.save(categoryDto);
        willDoNothing().given(categoryRepository).deleteById(savedCategory.getId());

        categoryService.delete(savedCategory.getId());

        verify(categoryRepository, times(1)).deleteById(savedCategory.getId());
    }


}