FROM openjdk:11-jdk-alpine
WORKDIR /opt/app
VOLUME /tmp
ARG JAR_FILE=target/geststock-0.0.1-SNAPSHOT.jar
MAINTAINER Arnaud Kemgouo
COPY ${JAR_FILE} geststock_v1.jar
ENTRYPOINT ["java", "-jar","geststock_v1.jar"]